# Configuration

## Relations

Relations can be customized through each project's `config.yml`.    
The way this is working is: ImmoDb's `config.yml` contains both
`ImmoDb>OpenImmoRelations` and `ImmoDb>ImmoDbRelations`.    
While `OpenImmoRelations` defines all relations set by the OpenImmo standard,
only the items under `ImmoDbRelations` are actually created by default.

If you need other attributes than the `ImmoDb` default, copy the `ImmoDb` settings to your
`mysite/config.yml` and add them under `CustomRelations`. You can now alter sort order
and items as you please. Try to use attributes from the Open Immo standard where possible.    
If this is not possible, coin your own identifier, and it'll be auto-created for you on next
build.

Configuration example:

	ImmoDb:
	  CustomRelations:
		OpenImmoObjektart:
		  grundstueck:
		  FINCA:
		  wohnung:
			- APARTMENT
			- PENTHOUSE
		  haus:
			- VILLA
			- STADTHAUS
			- REIHENHAUS
			- DOPPELHAUSHAELFTE
			- CHALET


**NOTE:** In the configuration only the identifiers are defined. All translations and 
wording changes should happen in the language files (TODO: implementation pending).


