# Radius search

In order to enable/update the radius search you need to run 
the following task from the command line:

	php public/framework/cli-script.php /ImmoDbLocationUpdateTask

For debugging, you can use the map, here:

	/ImmoDbDevController/locations

