# Todos & Considerations

## For release-1

* Translations with https://github.com/unclecheese/TranslatableDataObject
* Sorting and filtering on frontend, based on doap
* Allowing third parties to add estates
* Fixing issues etc. from the PM


## For later releases

* Google Maps
* Make all relations work like `OpenImmoObjektart` - at the moment it seems to be the only relation
we need in our current projects though
* adjust the doap filtering module so it's possible to filter by enabled - or other items
 * We should make a branch called improvements - could be a big pull request in the end


## Doap integration

* simultaneously filtering on two many-many relations gives an error atm (testcategory & objektart)
	=> probably we'll need to write a patch



## Improvements


## Backend considerations

* Use [sliderfield](https://github.com/tractorcow/silverstripe-sliderfield) for rooms etc.?
* Use [linkable](sheadawson/silverstripe-linkable) for links? we don't really have any yet though


## Translations & Multiple languages

* we need some kind of configurations for right titles, left titles etc on standarField - see Geo
* consider ivoba/silverstripe-i18n-fieldtypes    
	>This module provides improved fieldtypes to handle Currency and Date for internationalization. 
	It lets you define your formats via translation files or static setters.
* https://github.com/unisolutions/silverstripe-i18nenum

## Code consolidation (lower priority)

* go through all TODO items in the code


## Ideas


## Performance considerations

* Setting fields again and again (e.g. in `OpenImmoCmsFields::get_cms_fields()`, 
is that a good practice? I don't think so - better check how it's normally done

