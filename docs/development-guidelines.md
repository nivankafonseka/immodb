# Development Guidelines

## Data Model

ImmoDb's data model is based on the [OpenImmo standard](http://www.openimmo.de).
As most real estate usage won't need the entire standard (actually only a fraction of it),
and the standard doesn't contain everything we and our clients deemed necessary for
a software as ImmoDb, we've developed our own "ImmoDb" standard, which builds on top
of the OpenImmo standard.

Hence, the `Immobilie` object in a typical ImmoDb installation uses the following data structure:

* `Immobilie`    
_Individual to every installation, here the customization happens_
	* _extends_ `ImmoDbImmobilie`    
	_Our standard_
		* _extends_ `OpenImmoImmobilie`    
		_Following the OpenImmo standard as strict as possible_
			* _extends_ `ImmobilieBaseDataObject`    
			_The base for `Immobilie`, with helpers and attributes that are not strictly OpenImmo, but nevertheless needed_
				 * _extends_ `DataObjectAsPage`    
				 _A third party module that takes care of url handling_
					* _extends_ `DataObject`    
					_The SilverStripe base data object_


## Naming

* All attributes on `ImmoDbImmobilie` are prepended with `ImmoDb`
* All attributes on `OpenImmoImmobilie` are prepended with `OpenImmo`
* All attributes on `Immobilie` should be prepended width `Custom`    
- as it's custom only to that insttallation


## Customization

* If an attribute that already exists needs to be redefined (e.g. an enum), prepend it with `Altered`
* Attributes of type `MultivalueField` don't need to be redefined, as their options can be defined under `getCMSFields`
	* If an estate should be filterable, it should be either an enum, but preferably a relation



## Tags & Branches

By using tags and branches properly we'll be able to keep on developing on the module,
while not breaking existing live sites, as they'll be set via Composer to only update
from certain tags or branches.

### Tag structure

* `release1.0`
* `release1.1`
* `release1.2`
* `release2.0`



### Branch structure

* `release1`
* `release2-pre` _(if needed)_
* `release2`