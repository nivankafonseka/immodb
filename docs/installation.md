# Installation

## Setting up

For contribution:

	git submodule add git@bitbucket.org:immodb/immodb.git public/immodb

Via composer:


Add the following lines to you `composer.json`:

	"repositories": [
		{
			"type": "vcs",
			"url": "git@bitbucket.org:immodb/immodb.git"
		}
	],

...and under "require":

	"immodb/immodb": "*"



You'll most likely also need to add the additional custom repositories that immodb relies on to your
`composer.json` file. See `composer.json` under `repositories`.


## Necessary code steps

The module expects you to create an `Immobilie` DataObject, extending `ImmoDbImmobilie`.
This DataObject can be used for customization.



