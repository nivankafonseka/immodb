/**
 * Immobilie Form Fields
 */

var ImmobilieForm = function(form) {
	var $this = this;

	
	this.init = function(){
		
		
		this.GoogleMapsEvent();
		this.calcAddress();
		this.checkValidationErrors();
		
		//console.log('Immobilie form initialized');
	}

	this.GoogleMapsEvent = function(){
		var oo = 'OpenImmo';
		form.on('change', '[name='+oo+'Plz],[name='+oo+'Ort],[name='+oo+'Strasse],[name='+oo+'Hausnummer],[name='+oo+'Land]', function() {
			$this.calcAddress();
		});
	}


	this.calcAddress = function(){
		var oo = 'OpenImmo';
		var addr = form.find('[name='+oo+'Strasse]').val() + ' ' + 
		form.find('[name='+oo+'Hausnummer]').val() + ', ' +
		form.find('[name='+oo+'Plz]').val() + ' ' +
		form.find('[name='+oo+'Ort]').val() + ', ' +
		//form.find('[name='+oo+'Land]').val();
		jQuery('[name="ImmoDbBaseLocation"]').val();


		//console.log(addr);
		jQuery('[name=Address]').val(addr);
		
		
	}
	
	this.checkValidationErrors = function() {
		if (jQuery('.message.required').length) {

			var text = 'Bitte korregieren Sie folgendes: <br />';
			
			jQuery.each(jQuery('.message.required'), function() {
				$this = jQuery(this);
				text = text + $this.text() + '<br />';
			});
			
			
			jQuery.noticeAdd({
				stay: true,
				text: text,
				type: 'error'
			});
		}
	}
	




	//Automatic initialization on construction
	$this.init();
	
}