/**
 * Basic infinite scroll for immobilien holder
 */

// (function($) {
// 	$(function () {
//
// 		var holder = $(".lazyloading");
// 		var nextLink = holder.find('.next');
// 		if (nextLink.length) {
//
// 			//this is the over area
// 			var bottombox = $('<div class="bottombox" style="padding:10px;height:1200px;margin-left:0px;margin-right:0px;margin-bottom:-100px;margin-top:-600px;clear:both;" />');
//
// 			//add background color for debugging
// 			//bottombox.css('background','green');
//
// 			bottombox.insertAfter(holder);
//
// 			var nextURL = nextLink.attr('href');
// 			nextLink.remove();
//
// 			var loadInProgress = false;
//
// 			//set return false for bottombox debugging
// 			//return false;
//
// 			$('.bottombox').parent().on({
// 				mouseenter: function(){
//
//
// 					if (!loadInProgress) {
// 						loadInProgress = true;
// 						var loading = $('<div class="loading"><center><i class="fa fa-spinner fa-spin" style="color: #4fafc2; font-size: 60px; margin-top: 60px;">&#160;</i></center></div>');
// 						holder.append(loading);
// 						$.get(nextURL, function(data) {
// 							var items = $(data);
// 							holder.append(items);
//
//
// 							//updating images and heights
// 							Site.lazyLoadListingImages();
// 							Site.eq();
//
// 							loading.remove();
// 							loadInProgress = false;
//
// 							Site.lazyLoadListingImages();
// 							Site.eq();
//
// 							var nextLink = holder.find('.next');
// 							if (nextLink.length) {
// 								nextURL = nextLink.attr('href');
// 								nextLink.remove();
// 							} else {
// 								$('.bottombox').remove();
// 							}
// 						});
// 					}
// 				}
// 			}, ".bottombox");
//
//
//
//
//
// 		}
//
// 	});
// })(jQuery);
