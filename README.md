# Immo DB SilverStripe module

Copyright 2014 Jochen Gülden & title.dk/Anselm Christophersen.    
See License file for further details.


## Installation

See `docs/installation.md`


## Languages & Translations

At the moment the backend is developed to serve German speaking clients only, 
thus the backend needs to be set to **German** to see translations properly.
