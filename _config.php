<?php
//Making immobilie translatable
OpenImmoImmobilie::add_extension('ImmoDbTranslatableDataObject');
Image::add_extension("TranslatableDataObject('Title')");

//Cache settings
//read more about caching here:
//http://doc.silverstripe.org/framework/en/topics/caching

//Cache is set to 24h as we should make cache keys sensible to changes
//wherever it's applied - {@see ImmoDbLocation->getImmobilienWithDistances}

SS_Cache::set_cache_lifetime('any', 60*60*24);
// SS_Cache::set_cache_lifetime('ImmoItem', -1, 100);


//SS_Cache::set_cache_lifetime('any', -1, 100);
