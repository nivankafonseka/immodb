<?php
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\MapTypeId;
use Ivory\GoogleMap\Helper\MapHelper;

class ImmoDbDevController extends Controller {
	function init(){
		parent::init();
		
		//make sure that only admins can access this
		$member = Member::currentUser();
		if (!$member) {
			Security::permissionFailure();
		}
	}

	private static $allowed_actions = array(
		'locations',
		'ivorymap'
	);

	
	
	public function AllLocations() {
		$ls = ImmoDbLocation::get()
			->exclude('ParentID', 0);
		return $ls;
	}
	
	public function ivorymap () {

		$map = new Map();

		$map->setPrefixJavascriptVariable('map_');
		$map->setHtmlContainerId('map_canvas');

		$map->setAsync(false);
		$map->setAutoZoom(false);

		$map->setCenter(0, 0, true);
		$map->setMapOption('zoom', 3);

		$map->setBound(-2.1, -3.9, 2.6, 1.4, true, true);

		$map->setMapOption('mapTypeId', MapTypeId::ROADMAP);
		$map->setMapOption('mapTypeId', 'roadmap');

		$map->setMapOption('disableDefaultUI', true);
		$map->setMapOption('disableDoubleClickZoom', true);
		$map->setMapOptions(array(
			'disableDefaultUI'       => true,
			'disableDoubleClickZoom' => true,
		));

		$map->setStylesheetOption('width', '300px');
		$map->setStylesheetOption('height', '300px');
		$map->setStylesheetOptions(array(
			'width'  => '300px',
			'height' => '300px',
		));

		$map->setLanguage('en');
		
		
		//Debug::dump($map);

		$mapHelper = new MapHelper();
		
		return '<!DOCTYPE html>
		<html>
			<head></head>
			<body>
			' . $mapHelper->render($map) . '
			</body>
		</html>
		';

	}

}