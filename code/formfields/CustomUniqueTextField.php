<?php
class CustomUniqueTextField extends AjaxUniqueTextField {

	protected $objectId;

	public function __construct($name, $title, $restrictedField, $restrictedTable, $value = "", $maxLength = null,
			$validationURL = null, $restrictedRegex = null, $objectId = null){

		$this->objectId = $objectId;

		parent::__construct($name, $title, $restrictedField, $restrictedTable, $value, $maxLength,
			$validationURL, $restrictedRegex);
	}

	public function setObjectId($id) {
		$this->objectId = $id;
		return $this;
	}
	
	

	public function validate( $validator ) {
		
		//if no object id has been submitted, we let the parent handle it
		if (!$this->objectId || ($this->objectId == 0)) {
			return parent::validate($validator);
		}
		
		$result = DB::query($sql = sprintf(
			"SELECT COUNT(*) FROM \"%s\" WHERE \"%s\" = '%s' AND \"ID\" <> %s",
			$this->restrictedTable,
			$this->restrictedField,
			Convert::raw2sql($this->value),
			$this->objectId
		))->value();

		//Debug::dump($sql);


		if( $result && ( $result > 0 ) ) {
			$validator->validationError($this->name,
				_t('Form.VALIDATIONNOTUNIQUE', "The value entered is not unique"));
			return false;
		}

		return true;
	}
	
}