<?php
class ImmoDbVermarktungsartOptionsetField extends OptionsetField {

	function __construct($name) {
		parent::__construct(
			$name,
			ImmoDbTranslationHelper::fieldlabel(
				'Immobilie',
				'ImmoDbVermarktungsart')
		);

		$i = singleton('Immobilie');
		$values = $i->dbObject('ImmoDbVermarktungsart')->enumValues();

		$options = array();
		foreach ($values as $v) {
			$options[$v] = ImmoDbTranslationHelper::fieldlabel(
				'Immobilie',
				'ImmoDbVermarktungsart',
				$v
			);
		}

		$this->setSource($options);
	}
	
}