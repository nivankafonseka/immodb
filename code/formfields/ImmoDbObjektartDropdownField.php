<?php
class ImmoDbObjektartDropdownField extends SimpleTreeDropdownField {

	function __construct($name) {
		parent::__construct(
			$name,
			ImmoDbTranslationHelper::fieldlabel(
				'Immobilie',
				'OpenImmoObjektart'
			),
			'OpenImmoObjektart',
			'ID',
			'TitleTranslated'
		);
		
		$this->setFilter('Enabled=1');
		
		$this->setEmptyString('None');
	}
	
}