<?php
class ImmoDbRadiusSearchHelper {


	/**
	 * returns all available locations
	 * Definition:
	 * Any location that is within 5km of an Immobilie
	 * 
	 * 
	 * Notes about caching below,
	 * it's not needed anymore as we're using a cli task for this intead, but I'm keeping it for reference:
	 * 
	 * This dropdown is cached as calculating the locations is intensinve
	 * ATM the standard invalidation is used
	 *
	 * About caching
	 * http://willrossi.tumblr.com/post/1187670605/caching-memory-intensive-tasks-in-silverstripe
	 * http://doc.silverstripe.org/framework/en/topics/caching
	 * 
	 * 
	 */
	public static function available_locations($radius = 5, $purgeCache = false) {

		//ignoring the radius option for now
		//could be added in later by also calculating it through the task
		
		return ImmoDbLocation::get()
			->filter('ImmobilienInRadius5:GreaterThan', 0)
			->sort('Title');
		
		
		////TODO if called with purge cache, we should purge the cache
		//if ($purgeCache) {
		//	
		//}
		//
		//
		//$cachekey = "ImmoDbAvailableLocations$radius";
		//$cache = SS_Cache::factory($cachekey);
		//if ($result = $cache->load($cachekey)) {
		//	$result = unserialize($result);
		//} else {
		//	$ls = ImmoDbLocation::get()
		//		->filter('enabled',1)
		//		->sort('Title');
		//
		//	$al = new ArrayList();
		//	foreach ($ls as $l) {
		//		$is = $l->getImmobilienWithDistances($radius);
		//		//echo $is->count() . "\n";
		//
		//		if ($is->count()) {
		//			$al->push($l);
		//			
		//		}
		//	}
		//	$result = $al;
		//	$cache->save(serialize($result));
		//}
		//return $result;
	}

	/**
	 * Alternative way of calculating available locations
	 * This is done by looping through all ImmoDbLocations and checking for each
	 * if any immobilien have a location define in OpenImmoOrt that fits
	 */
	public static function available_locations_alt() {
		return ImmoDbLocation::get()
			->exclude('ImmobilienTiedByName', 0)
			->sort('Title');
	}
	


	/**
	 * A command line tester to check up on all locations and the calculated distances
	 * Loops through all locations, and lists distances to all Immobilien for each of them
	 */
	public static function cli_list_locations_with_distances($radius = null) {
		$ls = ImmoDbLocation::get()
			->exclude('ParentID', 0);
		//->byID(77);
		//->limit(15);
		foreach ($ls as $l) {
			echo $l->Title . "\n";
			$is = $l->getImmobilienWithDistances($radius);
			foreach ($is as $i) {
				echo $l->Title . " => " . $i->OpenImmoOrt . " - " . $i->Title . ": " . $i->Distance .  "\n";
			}
			echo "\n\n";
		}
	}

	/**
	 * A command line helper for listing all locations and their metadata
	 */
	public static function cli_list_locations() {

		$ls = ImmoDbLocation::get();
		foreach ($ls as $l) {
			echo "$l->Title, $l->PostalCode ($l->Lat, $l->Lon) \n";
		}
		
	}
}