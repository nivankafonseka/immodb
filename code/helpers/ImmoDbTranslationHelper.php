<?php
class ImmoDbTranslationHelper {

	/**
	 * Helper for building field labels that can be translated
	 * or amended through yml lang files
	 * Also this method defines some "pretty" defaults
	 * @param string $className
	 * @param string $fieldName
	 * @param string $optionName
	 */
	static function fieldlabel($className, $fieldName, $optionName = null) {

		$str = "$className.db_$fieldName";

		//return this for translation debugging
		//return $str;
		
		$default = "";
		if ($optionName) {
			$str = $str . "_$optionName";
			$default = self::label_formatted($optionName);
		} else {
			$default = self::label_formatted($fieldName);
		}

		return _t($str, $default);
	}


	/**
	 * Passing an array of field labels to this method
	 * returns this array with translated labels
	 */
	static function fieldlabels($className, $fieldName, $arr) {
		$tArr = array();

		//allowing to enable only certain enum options by default
		$enabledOptions = null;
		if (isset($className::config()->enabled_enum_options[$fieldName])) {
			$enabledOptions = $className::config()->enabled_enum_options[$fieldName];
			//Debug::dump($enabledOptions);
		}


		foreach ($arr as $item) {
			$doPush = true;
			if ($enabledOptions) {
				if (!in_array($item, $enabledOptions)) {
					$doPush = false;
				}
			}
			if ($doPush) {
				$tArr[$item] = self::fieldlabel($className, $fieldName, $item);
			}
		}
		return $tArr;
	}


	static function label_formatted($str) {


		//removing "custom" from labels - as this is our default for custom fields
		//we don't want it as part of the label though
		$str = str_replace('Custom', '', $str);
		//removing "ImmoDb", and "OpenImmo", which are the 2 main prefixes for the respective standards
		$str = str_replace('OpenImmo', '', $str);
		$str = str_replace('ImmoDbGrundstueck', '', $str);
		$str = str_replace('ImmoDb', '', $str);

		//default formatting

		//Special cases
		if ($str == 'KEINE_ANGABE') {
			$str = 'Keine Angabe';
		}



		//separate camel case
		//http://stackoverflow.com/questions/6254093/how-to-parse-camel-case-to-human-readable-string
		$str = preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]/', ' $0', $str);

		//taking care of ALL_CAPS strings
		if (strtoupper($str) == $str) {
			$str = strtolower($str);
		}

		$str = str_replace('_', ' ', $str);
		$str = str_replace('  ', ' ', $str);

		// TODO uncommented cause it causes trouble renaming e.g. Puerto Andratx to Pürto Andratx. 
		// @Anselm: Do we even need this string replacements?

		//$str = str_replace('ue', 'ü', $str);
		$str = str_replace('ae', 'ä', $str);
		$str = str_replace('oe', 'ö', $str);

		$str = str_replace('Ue', 'Ü', $str);
		$str = str_replace('Ae', 'Ä', $str);
		$str = str_replace('Oe', 'Ö', $str);

		$str = ltrim($str);

		$str = self::ucfirst_utf8_1($str);


		return $str;
	}

	// from http://stackoverflow.com/questions/13940541/ucfirst-with-accent-without-mbstring-enable
	static function ucfirst_utf8_1($string) {
		$convert_from = array(
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
			"v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
			"ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
			"з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
			"ь", "э", "ю", "я"
		);
		$convert_to = array(
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
			"V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï",
			"Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
			"З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
			"Ь", "Э", "Ю", "Я"
		);

		return str_replace($convert_from, $convert_to, mb_substr($string, 0, 1)).mb_substr($string, 1);
	}


}