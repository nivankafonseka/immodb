<?php

/**
 * Creation of demo assets for the ImmoDb Module
 * Extends object, so it's easy to configure it at a later stage
 */
class ImmoDbDemos extends Object {

	public static function requireRecords(){
		//***************** DEMO CREATION START *******************
		//Demo Immobilien creation
		//What's created here is conform with our "ImmoDb" standard
		//At a later stage creation of these can be done by configuration
		//For now they're created and updated on each build
		//systems may need different demo attributes

		//Starting by populating default records
		Populate::requireRecords();
		//sync files and assets
		//ATM this is needed for demo image creation
		Filesystem::sync();


		//Then do some coded amending here
		//Adding images (and other attributes)

		$demoImmoArr = array(
			'DEMO1' => array(
				'imgdir' => 'assets/immodb/demos/demo1/',
				'testcats' => array('TESTCAT1','TESTCAT2'),
				'Objektart' => array('FINCA')
			),
			'DEMO2' => array(
				'imgdir' => 'assets/immodb/demos/demo2/',
				'testcats' => array('TESTCAT2','TESTCAT3', 'TESTCAT5'),
				'Objektart' => array('APARTMENT')
			),
			'DEMO2a' => array(
				'imgdir' => 'assets/immodb/demos/demo2/',
				'testcats' => array('TESTCAT2','TESTCAT3', 'TESTCAT5'),
				'Objektart' => array('APARTMENT')
			),
			'DEMO2b' => array(
				'imgdir' => 'assets/immodb/demos/demo2/',
				'testcats' => array('TESTCAT2','TESTCAT3', 'TESTCAT5'),
				'Objektart' => array('APARTMENT')
			),
			'DEMO3' => array(
				'imgdir' => 'assets/immodb/demos/demo3/',
				'testcats' => array('TESTCAT3', 'TESTCAT4'),
				'Objektart' => array('haus', 'VILLA', 'STADTHAUS', 'DOPPELHAUSHAELFTE', 'CHALET')
			),
			'DEMO3a' => array(
				'imgdir' => 'assets/immodb/demos/demo3/',
				'testcats' => array('TESTCAT3', 'TESTCAT4'),
				'Objektart' => array('haus', 'VILLA', 'grundstueck')
			),
			'DEMO4' => array(
				'imgdir' => 'assets/immodb/demos/demo4/',
				'testcats' => array('TESTCAT3', 'TESTCAT4'),
				'Objektart' => array('haus', 'grundstueck')
			),
			'DEMO4a' => array(
				'imgdir' => 'assets/immodb/demos/demo4/',
				'testcats' => array('TESTCAT3', 'TESTCAT4'),
				'Objektart' => array('haus', 'grundstueck')
			),
			//'DEMO4' => 'assets/immodb/demos/demo4/',
			//'DEMO5' => 'assets/immodb/demos/demo5/',
		);
		foreach ($demoImmoArr as $identifier => $attributes) {
			$i = Immobilie::get()
				->filter('DemoIdentifier', $identifier)
				->first();
			if ($i && $i->exists()) {

				//imgages
				$dir = Folder::get()
					->filter('Filename', $attributes['imgdir'])
					->first();
				if ($dir && $dir->exists()) {
					$images = $i->Images();
					foreach ($dir->Children() as $img) {
						$images->add($img);
					}
				}
				//test categories
				$cats = $i->TestCategories();
				$cats->removeAll();
				foreach ($attributes['testcats'] as $catIdentifier) {
					$cat = ImmodbTestCategory::get()
						->filter('Identifier', $catIdentifier)
						->first();
					$cats->add($cat);
				}
				//Objektart
				$objektart = $i->OpenImmoObjektart();
				$objektart->removeAll();
				foreach ($attributes['Objektart'] as $objIdentifier) {
					$obj = OpenImmoObjektart::get()
						->filter('Identifier', 
							ImmoDbRelationHelper::tansliterated_identifier("OpenImmo$objIdentifier")
						)
						->first();
					if ($obj && $obj->exists()) {
						$objektart->add($obj);
					}
				}
				
				//writing immobilie to trigger "on after write"
				$i->write();
				//also triggering on after write manually, as it seems write is not working
				//when nothing has changed
				$i->doonAfterWrite();
			}
		}


		DB::alteration_message('Added demo images to demo immobilien', "updated");


		//***************** DEMO CREATION END *******************
	}

}
