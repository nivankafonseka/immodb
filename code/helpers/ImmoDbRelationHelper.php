<?php
class ImmoDbRelationHelper {

	/**
	 * This methods creates relations based on the settings set in the configuration
	 * Read more about this under docs/configuration.md
	 */
	public static function requireRecords(){
		
		//Open Immo Relations - only for reference - not acutally touched
		$openImmoRelations = ImmoDb::config()->OpenImmoRelations;
		
		//ImmoDb Relations - only created if no custom relations are set
		$immoDbRelations = ImmoDb::config()->ImmoDbRelations;
		
		//Custom relations - set through mysite/config/config.yml on individual projects
		$customRelations = ImmoDb::config()->CustomRelations;

		
		//Using ImmoDB relations by default
		$relations = $immoDbRelations;
		//If custom relations are set, use these instead
		if (isset($customRelations)) {
			$relations = $customRelations;
			//Debug::dump('using custom relations');
		}
		
		//Looping through each relation
		foreach ($relations as $relation => $attributes) {
			
			//adding all open immo identifiers for this relation to an array
			//so we have an array with both children and parents
			//NOTE: At the moment we're only either having the OpenImmo or Custom
			//prepend - if needed this could be extended by also using the ImmoDb prepend
			$openImmoAll = array();
			
			if (isset($openImmoRelations[$relation])) foreach ($openImmoRelations[$relation] as $parent => $children) {
				$openImmoAll[] = $parent;
				if (is_array($children)) foreach ($children as $child) {
					$openImmoAll[] = $child;
				}
			}
			//Debug::dump($openImmoAll);
			
			//We start by disabling all relations
			//Afterwards we'll re-enable the ones that are set in the config
			$relationObjs = $relation::get();
			if ($relationObjs && $relationObjs->exists()) foreach ($relationObjs as $relationObj) {
				$relationObj->Enabled = false;
				$relationObj->write();
			}
			
			
			//Debug::dump($relation);
			//Debug::dump($attributes);
			//echo "Now creating $relation relations";
			
			//Looping through each relations parent and child relations
			//for now we're supporting only those 2 levels
			$i = 1;
			foreach ($attributes as $parent => $children) {
				//Debug::dump($parent);
				$prepend = null;
				if (in_array($parent, $openImmoAll)) {
					$prepend = 'OpenImmo';
				} else {
					$prepend = 'Custom';
				}
				$parentIdentifier = $prepend . $parent;
				self::add_or_update_relation($relation, $parentIdentifier, $i);

				
				$ii = 1;
				if (is_array($children)) foreach ($children as $child => $childChildren) {
					//Debug::dump($child);
					//Debug::dump($childChildren);
					$childName = $childChildren;
					$search = null;
					if (is_array($childChildren)) {
						//Debug::dump($childChildren);
						$childName = $child;
						
						//Here we could loop through additional childs
						//- this is not needed at the moment
						//foreach ($childChildren as $childChildrenChild => $childChildrenChildAttrs) {
						//	Debug::dump($childChildrenChild);
						//	Debug::dump($childChildrenChildAttrs);
						//}
						
						//the special child "search" defines all searchable strings
						//TODO Ideas: 
						// 1. the system could automatically pick up translations
						// 2. In the future we could have "user defined search terms", a separate field
						//    where administrators can add additional search terms via the backend
						if (isset($childChildren['search'])) {
							$search = $childChildren['search'];
							//Debug::dump($search);
						}
					}
					//Debug::dump($childName);
					$prepend = null;
					if (in_array($childName, $openImmoAll)) {
						$prepend = 'OpenImmo';
					} else {
						$prepend = 'Custom';
					}
					$childIdentifier = $prepend . $childName;
					self::add_or_update_relation($relation, $childIdentifier, $ii, $parentIdentifier, $search);
					$ii++;
				}
				$i++;
			}
		}
		
		

		//Debug::dump($openImmoRelations);
		//Debug::dump($immoDbRelations);
		//Debug::dump($customRelations);

	}

	/**
	 * Adding/updating Relations
	 * TODO this could be improved by also taking
	 * the parent into account when filtering - doesn't seem as needed atm
	 * 
	 * 
	 * @param $relationName
	 * @param $identifier
	 * @param $sortOrder
	 * @param null $parentIdentifier
	 */
	public static function add_or_update_relation($relationName, $identifier, $sortOrder, $parentIdentifier = null, $search = null) {
		
		// Debug::dump($identifier);
// 		Debug::dump(Convert::raw2url());

		$transliteratedIdentifier = self::tansliterated_identifier($identifier);
		
		
		//We're getting the specific relation (classname), with it's specific identifier
		$obj = $relationName::get()
			->filter('Identifier', $transliteratedIdentifier)
			->first();
		if ($obj && $obj->exists()) {
		} else {
			$obj = new $relationName;
			$obj->Identifier = $transliteratedIdentifier;
		}
		
		//Setting a nice label
		$obj->Title = ImmoDbTranslationHelper::label_formatted($identifier);
		$obj->Enabled = true;
		$obj->SortOrder = $sortOrder;
		if ($search) {
			$obj->Search = $search;
		}
		
		//Parent
		if ($parentIdentifier) {
			$transliteratedParentIdentifier = self::tansliterated_identifier($parentIdentifier);
			$parent = $relationName::get()
				->filter('Identifier', $transliteratedParentIdentifier)
				->first();
			if ($parent && $parent->exists()) {
				$obj->ParentID = $parent->ID;
			}
		} else {
			//the default for the parentID should always be 0
			$obj->ParentID = 0;
		}
		
		
		$obj->write();
	}
	
	public static function tansliterated_identifier($identifier) {
		$transliteratedIdentifier = str_replace('Custom', 'custom-', $identifier);
		$transliteratedIdentifier = str_replace('OpenImmo', 'openimmo-', $transliteratedIdentifier);
		$transliteratedIdentifier = Convert::raw2url($transliteratedIdentifier);
		return $transliteratedIdentifier;
	}
}