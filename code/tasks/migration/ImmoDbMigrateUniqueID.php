<?php

/**
 * Used for migrating UniqueID changes from 15th August 2014
 * Only used for testing/developing
 * Run like this:
 * php public/framework/cli-script.php /ImmoDbMigrateUniqueID
 *
 */
class ImmoDbMigrateUniqueID extends CliController {

	/**
	 * Process
	 */
	function process() {
		echo 'Now migrating...';
		
		
		//All that needs to be done is to loop through all immobilien, and to save
		$is = Immobilie::get();
		foreach ($is as $i) {
			$i->ImmoDbUniqueIDRight = $i->Owner()->AgencyID;
			echo $i->Owner()->AgencyID."\n";
			$i->write();
		}
		
		echo 'done';

	}

}