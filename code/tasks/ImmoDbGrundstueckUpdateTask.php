<?php
/**
 * Available Location Update Task
 * Calculating available locations is resource intensive, as for each location
 * you have to loop through each estate, and calculate it's distance to the
 * location.
 * Hence it's better to have it in a task that is run nightly.
 * 
 * 
 * Run like this:
 * php public/framework/cli-script.php /ImmoDbGrundstueckUpdateTask
 *
 */
class ImmoDbGrundstueckUpdateTask extends CliController {

	/**
	 * Process
	 */
	function process() {
		
		
		$Immos = Immobilie::get();
		
		$i = 0;
		
		

		
		foreach ($Immos as $Immo) {
			
			
			if ( $Immo->OpenImmoObjektart()->exclude('Identifier', 'openimmo-grundstueck')->first() && $Immo->OpenImmoGrundstuecksflaeche =="0" ) {
				
				echo $Immo->ImmoDbGrundstueckGrundstuecksflaeche."\n";
				// echo $Immo->ImmoDbUniqueID."\n";

				$Immo->OpenImmoGrundstuecksflaeche = $Immo->ImmoDbGrundstueckGrundstuecksflaeche;
				$Immo->ImmoDbGrundstueckGrundstuecksflaeche = "";

			}
			
			$Immo->write();
			
		}

	}

}