<?php
/**
 * Available Location Update Task
 * Calculating available locations is resource intensive, as for each location
 * you have to loop through each estate, and calculate it's distance to the
 * location.
 * Hence it's better to have it in a task that is run nightly.
 * 
 * 
 * Run like this:
 * php public/framework/cli-script.php /ImmoDbCreatedAtMigrationTask
 *
 */
class ImmoDbCreatedAtMigrationTask extends CliController {

	/**
	 * Process
	 */
	function process() {
		
		
		$MigImmos = MigData::get();
		
		$i = 0;
		
		foreach ($MigImmos as $MigImmo) {
			
			$Immo = Immobilie::get()->filter(array(
				'd_id' => $MigImmo->d_id
			))->First();
			

			if ($Immo->ID && $MigImmo->d_datum_in) {
				echo $Immo->ID.' - '.$Immo->Created."\n";
				$Immo->Created = $MigImmo->d_datum_in;
				$Immo->write();
			}
		}

	}

}