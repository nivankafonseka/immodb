<?php

/**
 * Available Location Update Task
 * Calculating available locations is resource intensive, as for each location
 * you have to loop through each estate, and calculate it's distance to the
 * location.
 * Hence it's better to have it in a task that is run nightly.
 * 
 * 
 * Run like this:
 * php public/framework/cli-script.php /ImmoDbImageMigrationTask
 *
 */
class ImmoDbImageMigrationTask extends CliController {

	/**
	 * Process
	 */
	function process() {
				
				
		$MigImmos = MigData::get()->sort('d_id');
		//$this->Content = '<h2>Total: '.$MigImmos->Count().'</h2> Immobilien';

		$i = 0;
		
		foreach ($MigImmos as $MigImmo) {
			
			$z=0;
			
			$Immo = Immobilie::get()->filter(array(
				'd_id' => $MigImmo->d_id
			))->filter('ImmoDbStatus','Online')->First();
			
			echo "\n\nImmo ID | d_id = ". $Immo->ID . " | ".$Immo->d_id . "\n";
			
			if ( $Immo->d_id ) {
				$filenames = $this->getImagesByID($Immo->d_id);
			}
			
			print_r($filenames);
			
			if ($filenames) {
				
				foreach ($filenames as $filename) {
					
					//$dir = '/var/git-repos/mallorca-immobilien-guide/public/assets/immodb/immobilie/'.$id.'/images/';
					
					
					if ($filename && file_exists('/Users/jochenguelden/Sites/mallorca-immobilien-guide/public/assets/immodb/immobilie/'.$Immo->ID.'/images/'.$filename)) {
						
					echo "Filename: assets/immodb/immobilie/".$Immo->ID."/images/".$filename."\n";
						
						
					$file = File::get()->filter('Filename', 'assets/immodb/immobilie/'.$Immo->ID.'/images/'.$filename)->first();
					
					
					if ($file && $file->extension == "jpg") {
						
						
						
						DB::query ("
							UPDATE `ImmobilieBaseDataObject_Images`
							SET `SortOrder` =  ".$z."
							WHERE `ImageID` = ".$file->ID."
							
						");
						

						//Writing Image Titles
						$moo = "";
						$objektarten = $Immo->OpenImmoObjektart();
						foreach ( $objektarten as $objektart ) {
							$moo .=  $objektart->Title .' ';
						}
	
						// DB::query ("
						// 	UPDATE `File`
						// 	SET `Title` =  '".$moo." ".$Immo->Title."'
						// 	WHERE `ID` = ".$file->ID."
						//
						// ");
						
						
						 // $file->Name("".$moo."-".$Immo->Title."jpg");
						 // $file->write();
						$z++;
						
					}
						
				}
			}
		}
				
		}
		
		echo "done\n";

	}
	
	
	// all images for a single d_id
	// id = Immobilie ID
	public function getImagesByID($d_id) {
		$I = MigData::get()->filter(array(
			'd_id' => $d_id
		))->First();
		
		if ($I) {
			$filenames = unserialize($I->d_img_order);
			return $filenames;
		} else {
			return;
		}
			
	}

}