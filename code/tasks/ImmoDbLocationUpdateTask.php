<?php

/**
 * Location Update Task
 * Run like this:
 * php public/framework/cli-script.php /ImmoDbLocationUpdateTask
 *
 */
class ImmoDbLocationUpdateTask extends CliController {

	/**
	 * Process
	 */
	function process() {

		$ls = ImmoDbLocation::get()->exclude('ParentID', 0);
			//->limit(1);
		foreach ($ls as $l) {
			if ((!$l->Lat && !$l->Lon) || $l->ForceUpdateGeoData) {
				echo $l->getFullLocationString() .  "\n";
				$l->updateGeoData();
				//break;
			}
		}


	}

}