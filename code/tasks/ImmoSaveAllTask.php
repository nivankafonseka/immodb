<?php
/**
 * Available Location Update Task
 * Calculating available locations is resource intensive, as for each location
 * you have to loop through each estate, and calculate it's distance to the
 * location.
 * Hence it's better to have it in a task that is run nightly.
 * 
 * 
 * Run like this:
 * php public/framework/cli-script.php /ImmoSaveAllTask
 *
 */
class ImmoSaveAllTask extends CliController {

	/**
	 * Process
	 */
	function process() {
		
		
		$Immos = Immobilie::get();
		
		foreach ($Immos as $Immo) {
			$Immo->write();
			$Images = $Immo->ImmoImages();
			foreach ($Images as $I) {
				$I->write();
			}
			echo $Immo->ID; 
		}

	}

}