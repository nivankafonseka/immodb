<?php

/**
 * Available Location Update Task
 * Calculating available locations is resource intensive, as for each location
 * you have to loop through each estate, and calculate it's distance to the
 * location.
 * Hence it's better to have it in a task that is run nightly.
 * 
 * 
 * Run like this:
 * php public/framework/cli-script.php /ImmoDbCategoryMigrationTask
 *
 */
class ImmoDbCategoryMigrationTask extends CliController {

	/**
	 * Process
	 */
	function process() {
		
		
		$MigImmos = MigData::get()->sort('ID');
		//$this->Content = '<h2>Total: '.$MigImmos->Count().'</h2> Immobilien';

		$i = 0;
		
		foreach ($MigImmos as $MigImmo) {
			
			$Immo = Immobilie::get()->filter(array(
				'd_id' => $MigImmo->d_id
			))->First();
			
			if ($Immo) {	
				/*
					$_var["de"]["typs"][1] = "Haus";
					$_var["de"]["typs"][2] = "Villa";
					$_var["de"]["typs"][3] = "Anwesen";
					$_var["de"]["typs"][4] = "Finca";
					$_var["de"]["typs"][5] = "Stadthaus";
					$_var["de"]["typs"][6] = "Dorfhaus";
					$_var["de"]["typs"][7] = "Doppelhaushälfte";
					$_var["de"]["typs"][8] = "Reihenhaus";
					$_var["de"]["typs"][9] = "Apartment";
					$_var["de"]["typs"][10] = "Penthouse";
					$_var["de"]["typs"][11] = "Sanierungsobjekt";
					$_var["de"]["typs"][12] = "Grundstück";
					$_var["de"]["typs"][13] = "Gewerbe";
					$_var["de"]["typs"][14] = "Wohnung";
					$_var["de"]["typs"][15] = "Chalet";
					$_var["de"]["typs"][20] = "Wohnung";
				*/
				#Typ

				# The Values that occur in MigData translated to or indentifiers/naming.
				$typ_array = array(
					"1" => "haus",
					"2" => "VILLA",
					"3" => "ANWESEN",
					"4" => "FINCA",
					"5" => "STADTHAUS",
					"6" => "Dorfhaus",
					"7" => "DOPPELHAUSHAELFTE",
					"8" => "REIHENHAUS",
					"9" => "APARTMENT",
					"10" => "PENTHOUSE",
					// "11" => "Sanierungsobjekt",
					"12" => "grundstueck",
					"13" => "Gewerbeimmobilie",
					"14" => "wohnung",
					"15" => "CHALET",
					"20" => "wohnung"
			
				);
		
				// The data we import: string to array
				$MigDataTyp = explode(',', $MigImmo->d_typ );
				// Matching 
				$result = array_intersect_key($typ_array, array_flip($MigDataTyp)); 
				$array = array_values($result);
				$objektart = $Immo->OpenImmoObjektart();
				$objektart->removeAll();
				$Immo->write();
				
				
				// var_dump($array[1]);
				$i=0;
				
				foreach ( $array as $objIdentifier) {
					

					$obj = OpenImmoObjektart::get()
						->filter('Identifier', ImmoDbRelationHelper::tansliterated_identifier("OpenImmo$objIdentifier")
					)->first();

					if ($obj && $obj->exists() && $i == 0 )  {
						
							echo "#Ref: ".$Immo->ImmoDbUniqueID." --> d_id: ".$Immo->d_id . " - " .$objIdentifier." - ".$obj->ID."\n";
							$Immo->ImmoDbKategorieID = $obj->ID;
						
						
						// $objektart->add($obj->ID);
						
						$i++;
					}

					
				}


				if ( array_key_exists("11", $typ_array)) {
					$Immo->ImmoDbSanierungsobjekt = true;
				}

				if ( array_key_exists("12", $typ_array)) {
					$Immo->ImmoDbGrundstueck = true;
				}
				
				
				$Immo->write();
				$Immo->doonAfterWrite();
				
		} // END IF IMMO		
			

	} // END FOREACH


	} // END PROCESS

} // END CLASS