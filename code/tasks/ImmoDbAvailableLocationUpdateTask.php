<?php

/**
 * Available Location Update Task
 * Calculating available locations is resource intensive, as for each location
 * you have to loop through each estate, and calculate it's distance to the
 * location.
 * Hence it's better to have it in a task that is run nightly.
 * 
 * 
 * Run like this:
 * php public/framework/cli-script.php /ImmoDbAvailableLocationUpdateTask
 *
 */
class ImmoDbAvailableLocationUpdateTask extends CliController {

	/**
	 * Process
	 */
	function process() {

		$ls = ImmoDbLocation::get()
			->filter('enabled',1)
			->sort('Title');

		$limit = null;
		
		//lmit only for development
		//$limit = 20;

		if ($limit) {
			$ls = $ls->limit($limit);
		}
		
		foreach ($ls as $l) {
			
			//1. calculating immobilien with distances
			
			$is = $l->getImmobilienWithDistances(1);
			echo $is->count() . "\n";
			echo $l->getFullLocationString() .  "\n";

			$l->ImmobilienInRadius5 = $is->count();
			$l->write();
			
			
			//2. calculating immobilien tied by name
			$l->calcImmobilienTiedbyName();
		}
		
		echo "done\n";

	}

}