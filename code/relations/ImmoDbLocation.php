<?php
class ImmoDbLocation extends ImmoDbBaseRelation implements Mappable {
	
	static $db = array (
		//These are auto-populated by Google, and are not administerable ATM
		'Lat' => 'Varchar',
		'Lon' => 'Varchar',
		'PostalCode' => 'Varchar',
		//To not overstrain the Google API,
		//by default every location is only queried once
		//It can be forced by setting this
		'ForceUpdateGeoData' => 'Boolean',
		'ImmobilienInRadius5' => 'Int',
		'ImmobilienTiedByName' => 'Int'
	);
	
	static $belongs_many_many = array(
		'Immobilien' => 'ImmoDbImmobilie',
		'Members' => 'Member', //Members can be restricted to only be able to post to specific locations
	);


	/**
	 * Full string to identify a relation
	 */
	public function getFullLocationString() {
		$baseLoc = ImmoDb::config()->BaseLocation;
		//removing double white spaces
		$title = preg_replace('/\s+/', ' ',$this->Title);
		
		return $title . ", " . $baseLoc;
	}


	public function LocationString() {
		$baseLoc = ImmoDb::config()->ImmoDbLocation;
		//removing double white spaces
		$title = preg_replace('/\s+/', ' ',$this->Title);
		
		return $title . ", " . $baseLoc;
	}



	/**
	 * Gets all Immobilien and their distance to this location
	 * By adding a radius, only locations within a specific radius is shown
	 */
	public function getImmobilienWithDistances($radius = null){

		$id = $this->ID;
		
		//The cache key is specific to location and radius,
		//and is invalidated everytime an immobilie is added or edited
		$cachekey = "ImmoDbLocation{$id}ImmobilienWithDistance{$radius}_" .
			strtotime(Immobilie::get()->max('LastEdited'));
		
		
		//echo $cachekey . '<br />';
		//return;
		
		$cache = SS_Cache::factory($cachekey);

		if ($result = $cache->load($cachekey)) {
			$result = unserialize($result);
		} else {
			$result = $this->calcImmobilienWithDistances($radius);
			$cache->save(serialize($result));
		}
		return $result;

	}
	
	
	private function calcImmobilienWithDistances($radius = null) {
		$lat = floatval($this->Lat);
		$lat = str_replace(',', '.', $lat);
		$lng = floatval($this->Lon);
		$lng = str_replace(',', '.', $lng);


		//$sql = 'SELECT OpenImmoGeokoordinatenBreitengrad AS lat, OpenImmoGeokoordinatenLaengengrad AS lng,
		//	( 6371 * acos( cos( radians('.$lat.') ) * cos( radians(OpenImmoGeokoordinatenLaengengrad ) ) * cos( radians(OpenImmoGeokoordinatenBreitengrad ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians(OpenImmoGeokoordinatenLaengengrad ) ) ) ) AS distance
		//	FROM OpenImmoImmobilie
		//	ORDER BY distance ASC';
		//
		////echo $sql;


		$sqlQuery = new SQLQuery();
		$sqlQuery->setFrom('OpenImmoImmobilie');

		$sqlQuery->selectField('ID', 'ID');
		$sqlQuery->selectField('OpenImmoObjekttitel', 'title');
		$sqlQuery->selectField('OpenImmoOrt', 'loc');


		$sqlQuery->selectField('OpenImmoGeokoordinatenBreitengrad', 'lat');
		$sqlQuery->selectField('OpenImmoGeokoordinatenLaengengrad', 'lng');
		$sqlQuery->selectField('( 6371 * acos( cos( radians('.$lat.') ) * cos( radians(OpenImmoGeokoordinatenBreitengrad) ) * cos( radians(OpenImmoGeokoordinatenLaengengrad ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians(OpenImmoGeokoordinatenBreitengrad ) ) ) )', 'distance');
		//$sqlQuery->selectField('YEAR("Birthday")', 'Birthyear');
		//$sqlQuery->addLeftJoin('Team','"Player"."TeamID" = "Team"."ID"');
		//$sqlQuery->addWhere('YEAR("Birthday") = 1982');
		// $sqlQuery->setOrderBy(...);
		// $sqlQuery->setGroupBy(...);
		// $sqlQuery->setHaving(...);
		// $sqlQuery->setLimit(...);
		// $sqlQuery->setDistinct(true);

		if ($radius) {
			$sqlQuery->setHaving('distance < '. $radius);
		}


		// Get the raw SQL (optional)
		//$rawSQL = $sqlQuery->sql();

		// Execute and return a Query object
		$result = $sqlQuery->execute();

		// Iterate over results
		//foreach($result as $row) {
		//	echo $this->Title . " => " . $row['loc'] . '(' .substr($row['title'], 0, 15) . "): " . $row['distance'] . "\n";
		//}

		//Adding the distance into a "real" Immobilie object
		$is = Immobilie::get();
		$al = new ArrayList();
		foreach ($is as $i) {
			foreach($result as $row) {
				if ($row['ID'] == $i->ID) {
					//echo $row['distance'] . "\n";
					$i->Distance = $row['distance'];
					$al->push($i);
				}
			}
		}
		return $al;
	}

	/**
	 * Calculates all immobilien that have an OpenImmoOrt that fit
	 * this location's title or search
	 */
	public function calcImmobilienTiedbyName() {
		
		$al = new ArrayList();
		
		$id = $this->ID;
		//$is = Immobilie::get()->limit(200);
		//$is = Immobilie::get()->filter('ImmoDbStatus','Online');
		$is = Immobilie::get();

		foreach ($is as $i) {
			$cl = $i->getLocationFromOrtString();
			if ($cl && $cl->exists()) {
				if ($cl->ID == $id) {
					$al->push($i);
				}
			}
		}
		
		$this->ImmobilienTiedByName = $al->count();
		$this->write();
		return $al;
	}
	
	

	/**
	 * Update the location's geodata through Google maps
	 */
	public function updateGeoData() {
		$address = $this->getFullLocationString();
		if ( $json = @file_get_contents( "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=".urlencode( $address ) ) ) {
			//echo $json. "\n\n";
			$response = Convert::json2array( $json );
			
			//var_dump($response);
			
			$location = $response['results'][0]['geometry']['location'];
			//var_dump($location);
			

			$addrComponents = $response['results'][0]['address_components'];
			//var_dump($addrComponents);

			$postcalCode = null;
			
			if ($addrComponents) {
					
				foreach($addrComponents as $c) {
					//var_dump($c);
					if (isset($c['types'][0])) {
						if ($c['types'][0] == 'postal_code') {
							$postcalCode = $c['long_name'];
							var_dump($c);
						}
					}
				}
					
			}

			$this->Lat = $location['lat'];
			$this->Lon = $location['lng'];
			$this->PostalCode = $postcalCode;
			$this->write();
			
		}
	}
	
	
	

/* Mappable interface requirements */

	public function getLatitude() {
		return $this->Lat;
	}

	public function getLongitude() {
		return $this->Lon;
	}

	public function getMapContent() {
		//below is not currently implemented
		//return GoogleMapUtil::sanitize($this->renderWith('MapBubbleMember'));
	}

	public function getMapPin() {
		//return $this->Type."_pin.png";
	}

}