<?php

/**
 * TODO This will soon become obsolete as we'll
 * get rid of the doap filter module
 */
class ImmoDbTestCategory extends DataObjectAsPageCategory {

	public static $db = array(
		'Identifier' => 'Varchar(255)'
	);

	//Listing Page Class
	private static $listing_page_class = 'ImmobilienHolder';

	//Class Naming (optional but reccomended)
	private static $singular_name = 'Test Category';
	private static $plural_name = 'Test Categories';

	//Category Relation
	private static $belongs_many_many = array(
		'Immobilien' => 'ImmobilieBaseDataObject'
	);
}