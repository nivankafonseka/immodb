<?php
/**
 * Base class for ImmoDB relations
 *
 */
class ImmoDbBaseRelation extends DataObject {

	static $db = array (
		'Title' => 'Varchar(255)',
		//use our naming conventions: OpenImmoZimmer, OpenImmoZIMMER
		//ImmoDbZimmer, CustomZimmer
		//these 3 are allowed (and needed): OpenImmo, ImmoDb, Custom 
		'Identifier' => 'Varchar(255)',
		'Enabled' => 'Boolean',
		"SortOrder" => 'Int',
		'Search' => 'Text' //Full text search
	);

	private static $extensions = array(
		'Hierarchy'
	);

	private static $default_sort = "SortOrder";

	
	public function getTitleTranslated() {
		$identifier = $this->Identifier;
		$translation = ImmoDbTranslationHelper::fieldlabel(
			$this->ClassName,
			$identifier
		);
		
		$translationString = "{$this->ClassName}.db_$identifier";
		//you can use this for debugging translations
		//return $translationString;
		
		$title = null;
		if (strtolower(Convert::raw2url($translation)) == $identifier) {
			//Fallback to db title, if no translation has been defined
			$title = $this->Title;
		} else {
			$title = $translation;
		}
		return $title;

	}
	
	public function Children(){
		$className = $this->ClassName;
		$children = $className::get()
			->filter('ParentID', $this->ID);
		return $children;
		//return $this->AllChildren();
	}

	
	
	//TODO these are currently not used remains 
	//TODO it would be useful though to have methods to automatically filter out disabled items

	
//	public static function get_enabled_fields() {
//		$callerClass = get_called_class();
//
//		$fields = $callerClass::get();
//
//		//enabled fields are configureable through "enabled_identifiers"
//		$enabledIdentifiers = $callerClass::config()->enabled_identifiers;
//		if (is_array($enabledIdentifiers)) {
//			$fields = $fields->filter('Identifier', $enabledIdentifiers);
//		}
//		return $fields;
//	}
//
//	public static function get_enabled_fieldmap() {
//		return self::get_enabled_fields()->map('ID', 'Title');
//	}

}