<?php
class TestHierarchyCategory extends DataObject {
	public static $db = array(
		'Title' => 'Varchar(255)'
	);
	
	private static $extensions = array(
		"Hierarchy",
	);
	
	private static $belongs_many_many = array(
		'Immobilien' => 'ImmobilieBaseDataObject'
	);
}