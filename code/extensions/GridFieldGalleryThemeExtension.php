<?php
/**
 * SS3 GridFiled component appying a theme to the GridField
 * rendering it into a gallery view
 *
 * @author colymba
 * @package GridFieldGalleryTheme
 */
class GridFieldGalleryThemeExtension extends GridFieldGalleryTheme {

    // Changing Thumbnail Size
    function getColumnContent($gridField, $record, $columnName)
    {
      $previewObj = $record->{$this->thumbnailField}();
      $imgFile = GRIDFIELD_GALLERY_THEME_PATH . '/img/icons/missing.png';
      
      if ( $previewObj->ID )
      {
        if ( $previewObj instanceof Image )
        {
          $croppedImage = $previewObj->CroppedImage(200,166);
          if ($croppedImage)
          {
            $url = $croppedImage->URL;
            if ($url) $imgFile = $url;
          }
        }
        else if ( $previewObj instanceof File )
        {
          if ( is_dir( BASE_PATH.'/'.$previewObj->Filename ) ) $imgFile = GRIDFIELD_GALLERY_THEME_PATH . '/img/icons/folder.png';
          else $imgFile = $this->getFileTypeIcon( $previewObj );
        }
      }
      
      return '<img src="'.$imgFile.'" />';
    }
    

}
