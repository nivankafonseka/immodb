<?php

/**
 * Special "TranslatableDataObject",
 * adjusted for ImmoDb
 */
class ImmoDbTranslatableDataObject extends TranslatableDataObject {

	/**
	 * Get a tabset with a tab for every language containing the translatable fields.
	 * Example usage:
	 * <code>
	 *     public function getCMSFields(){
	 *         $fields = new FieldList();
	 *         $fields->add($this->getTranslatableTabSet());
	 *         return $fields;
	 *     }
	 * </code>
	 * @param string $title the title of the tabset to return. Defaults to "Root"
	 * @return TabSet
	 */
	public function getTranslatableTabSet($title = 'Root', $showNativeFields = true){
		$set = new TabSet($title);

		// get target locales
		$locales = self::get_target_locales();

		// get translated fields
		//$fieldNames = array_keys(self::$localizedFields[$this->owner->class]);
		$fieldNames = array_keys(self::$localizedFields['OpenImmoImmobilie']);

		$ambiguity = array();
		foreach($locales as $locale){
			$langCode = i18n::get_lang_from_locale($locale);
			foreach($locales as $l){
				if($l != $locale && i18n::get_lang_from_locale($l) == $langCode){
					$parts = explode('_', $l);
					$localePart = end($parts);
					$ambiguity[$l] = $localePart;
				}
			}
		}

		foreach($locales as $locale){
			$langName = ucfirst(html_entity_decode(
				i18n::get_language_name(i18n::get_lang_from_locale($locale), true),
				ENT_NOQUOTES, 'UTF-8'));

			if(isset($ambiguity[$locale])){
				$langName .= ' (' . $ambiguity[$locale] . ')';
			}
			$tab = new Tab($locale, $langName);

			foreach ($fieldNames as $fieldName) {
				$tab->push($this->getLocalizedFormField($fieldName, $locale));
			}

			$set->push($tab);
		}
		
		//removing "OpenImmoObjekttitel" - it's already synced with "Title", and we don't want it twice
		$set->removeByName('OpenImmoObjekttitel');
		
		return $set;
	}

	/**
	 * Get a form field for the given field name
	 * @param string $fieldName
	 * @param string $locale
	 * @return FormField
	 */
	public function getLocalizedFormField($fieldName, $locale){
		$baseName = $this->getBasename($fieldName);
		
		
		$localizedFieldName = self::localized_field($fieldName, $locale);

		$dbFields = array();
		Config::inst()->get($this->owner->class, 'db', Config::EXCLUDE_EXTRA_SOURCES, $dbFields);

		$type = isset($dbFields[$baseName]) ? $dbFields[$baseName] : '';
		$typeClean = (($p = strpos($type, '(')) !== false) ? substr($type, 0, $p) : $type;
		$field = null;

		$baseName = ImmoDbTranslationHelper::fieldlabel('Immobilie', $fieldName);

		$iHolder = $this->owner->getListingPage();

		//Debug::dump($typeClean);
		
		switch ($typeClean) {
			case 'Varchar':
				//Debug::dump($fieldName);
				
				if ($fieldName == 'URLSegment') {
					
					if ($this->owner->ID) {
						$field = new SiteTreeURLSegmentField($localizedFieldName, 'Link');
						//$prefix = Director::absoluteBaseURL() . 'immobilien/show/';
						$prefix = $prefix = $iHolder->getTranslation($locale)->AbsoluteLink('show').'/';
						$field->setURLPrefix($prefix);
						
						//this is not working for some reason - but also not so important
						//$field->setURLSuffix('/');
						$helpText = _t('SiteTreeURLSegmentField.HelpChars', ' Special characters are automatically converted or removed.');
						$field->setHelpText($helpText);
						return $field;
					} else {
						//we don't want any field here
						return new HiddenField($localizedFieldName, 'Link');
					}
				
				}

				if ($fieldName == 'MetaDescription') {
					return new TextareaField($localizedFieldName, $baseName);
				}
				
			case 'HTMLVarchar':
				$field = new TextField($localizedFieldName, $baseName);
				break;
			case 'Text':
				$field = new TextareaField($localizedFieldName, $baseName);
				break;
			case 'HTMLText':
			default:
				$field = new HtmlEditorField($localizedFieldName, $baseName);
				break;
		}
		return $field;
	}

	/**
	 * Custom getter, as the original a protected
	 * don't know why...
	 */
	public static function get_target_locales_public() {
		return self::get_target_locales();
	}
}