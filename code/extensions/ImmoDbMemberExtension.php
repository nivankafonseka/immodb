<?php
class ImmoDbMemberExtension extends DataExtension {

	//not needed
	//private static $has_many = array(
	//	'OwnedImmobilien' => 'ImmobilieBaseDataObject'
	//);

	private static $db = array(
		'AgencyID' => 'Varchar(4)', //agency ID, definable by admins
		'Company' => 'Varchar'
	);
	
	
	private static $many_many = array(
		'AdministerableLocations' => 'ImmoDbLocation'
	);

	//agency id as index throws some errors...
	//hence we do unique checking on the frontend - through AjaxUniqueTextField
	//static $indexes = array(
	//	//'AgencyID' => 'unique'
	//);
	
	private static $summary_fields = array(
		'FirstName' => 'First Name',
		'Surname' => 'Last Name',
		'Company' => 'Company',
		'Email' => 'Email',
	);
	

	public function updateCMSFields(FieldList $fields) {
		
		$fields->insertAfter(
			TextField::create('Company', 'Firma'),
			'Surname'
		);
		
		$fields->removeByName('AdministerableLocations');
		$fields->addFieldsToTab('Root.ImmoDb', array(
				
			//NumericField::create('AgencyID', 'Agency ID')
			//	->setMaxLength(4)->addExtraClass('small'),
			
			//AjaxUniqueTextField::create('AgencyID', 'Agency ID', 'AgencyID', 'Member')
			//	->setMaxLength(4),

			CustomUniqueTextField::create('AgencyID', 'Agency ID', 'AgencyID', 'Member')
				->setMaxLength(4)
				->setObjectId($this->owner->ID),
			
			//this can both act as a many-many and a has-many relations
			$locationHeader = HeaderField::create('locationHeader', 'Erlaubte Gebiete'),
			$locationDesc = LiteralField::create('locationDesc', '
				Hier können Sie erlaubte Gebiete für diesen Benutzer angeben. <br />
				Ausgewählte Hauptgebiete vererben sich auf Untergebiete. <br />
				Dh. wenn Sie ein Haupt-Gebiet ausgewählt haben, hat der Benutzer Zugang zu allen
				Untergebieten dieses Gebietes.
			'),
			$locationField = new TreeMultiselectField(
				'AdministerableLocations',
				'',
				//'ImmoDbLocation',
				'ImmoDbBaseRelation',
				'ID',
				'Title'
			)

			
		));

		
		$callback = create_function('$obj', 'return $obj->class == "ImmoDbLocation" AND $obj->Enabled == 1;');
		$locationField ->setFilterFunction ( $callback );
		$locationField ->setChildrenMethod('Children');
		$locationField->setShowSearch(false);
		//$locationField->setSearchFunction(array($obj, 'locationSearchCallback'));
	}

	public function onAfterWrite() {
		parent::onAfterWrite();

		//first idea was to automatically add all child locations
		//I think it's cleaner to calculate those in the dropdown though - so that's what I went for
		//$locations = $this->owner->AdministerableLocations();
		//foreach ($locations as $loc) {
		//	if ($loc->ParentID == 0) {
		//		foreach ($loc->Children() as $child) {
		//			//$this->owner->AdministerableLocations()->add($child);
		//		}
		//	}
		//}
	}	
	
	
}