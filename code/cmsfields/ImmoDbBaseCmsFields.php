<?php
/**
 * Base class for Immodb cms field definitions
 * For now this is only used for the Immobilie cms fields as they're
 * awfully long
 *
 * @package immodb
 * @subpackage cmsfields
 */
class ImmoDbBaseCmsFields {

	/**
	 * Get and remove a field name
	 * Convenience function to get a field by it's name, and remove it from
	 * the form fields so it can be manually inserted afterwards
	 *
	 * @param FieldList $fields
	 * @param string $fieldName
	 * @return FormField
	 */
	public static function get_and_remove(FieldList $fields, $fieldName) {
		$field = $fields->dataFieldByName($fieldName);
		$fields->removeByName($fieldName);
		return $field;
	}

}