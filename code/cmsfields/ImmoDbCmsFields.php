<?php

/**
 * ImmoDbImmobilieCmsFields
 *
 * @package immodb
 * @subpackage cmsfields
 */
class ImmoDbCmsFields extends ImmoDbBaseCmsFields {

	/**
	 * All ImmoDb Fields
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function get_cms_fields($fields, $obj) {

		//Basics
		$fields = OpenImmoCmsFields::open_immo_basics($fields);

		//Main
		$fields = self::immodb_main($fields, $obj);

		//Geo
		$fields = self::immodb_geo($fields, $obj);

		//Immobilie
		$fields = self::immodb_immobilie($fields, $obj);

		//Ausstattung
		$fields = self::immodb_ausstattung($fields, $obj);

		//Extras
		$fields = self::immodb_extras($fields, $obj);

		//Texte
		$fields = self::immodb_texte($fields, $obj);

		//Bilder
		$fields = OpenImmoCmsFields::open_immo_bilder($fields, $obj);

		//Infrastruktur
		$fields = self::immodb_infrastruktur($fields, $obj);

		//Kontaktperson(en)
		$fields = OpenImmoCmsFields::open_immo_kontaktperson($fields, $obj);
		//Admin
		$fields = OpenImmoCmsFields::open_immo_admin($fields, $obj);



		$obj->extend('updateCMSFields', $fields);

		return $fields;
	}



	/**
	 * Main
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function immodb_main($fields, $obj) {

		$fields->addFieldsToTab('Root.Main', array(
			FieldGroup::create(
				LiteralField::create('UniqueIdDesc',
				
					'
						<label class="left UniqueIdDesc">Ref.- Nr.</label>
					'),
				
				Textfield::create('ImmoDbUniqueIDLeft', '')
					->setMaxLength(8),
				LiteralField::create('ImmoDbUniqueIDRight',
					'
<div class="field text nolabel" id="ImmoDbUniqueIDRight">
	<div class="middleColumn">
		<input type="text" placeholder="- ' . $obj->getPartnerAgencyID() . '" size="4" class="text nolabel" disabled>
	</div>
</div>
					')
					

					
				//Textfield::create('ImmoDbUniqueIDRight', '')
				//	->setMaxLength(4)
				//	//->performDisabledTransformation()
				//	->setValue('auto')
				//	->performReadonlyTransformation()
			)->addExtraClass('UniqueId field'),
		));
		
		$fields = OpenImmoCmsFields::open_immo_main($fields, $obj);

		$fields->removeByName('OpenImmoNutzungsart');
		$fields->removeByName('OpenImmoVermarktungsart');

		//$objektart = self::get_and_remove($fields, 'OpenImmoObjektart');


		//Preise

		$ab = $obj->standardField('ImmoDbAbschlag');
		$kp = $obj->standardField('OpenImmoKaufpreis');
		$kp->displayIf("ImmoDbVermarktungsart")->isEqualTo("KAUF");
		$nk = $obj->standardField('OpenImmoNettokaltmiete');
		$nk->displayIf("ImmoDbVermarktungsart")->isEqualTo("MIETE_PACHT");
		$ab->displayIf("ImmoDbVermarktungsart")->isEqualTo("MIETE_PACHT");
		
		$fwp = $obj->standardField('ImmoDbFerienvermietungWochenpreis');
		$fwp->displayIf("ImmoDbVermarktungsart")->isEqualTo("FERIENVERMIETUNG");

		//TODO the toggle field needs a fixed height
		
		//It seems fieldgroup is more the one to use instead of CompositeField
		//http://api.silverstripe.org/3.1/class-FieldGroup.html
		
		$grundstueckFields = new FieldGroup('Grundstück',
			array(
				HeaderField::create('GrundstueckHeading', 'Grundstücksangaben', 3),
//				LiteralField::create('GrunstueckTODO',
//				'
//<h3>Überlegungen, Anselm</h3>
//Hier sollen Grundstück-Daten angegeben werden.<br />
//Ich stelle mir vor, dass wir "Grundstück" aus den Kategorien rausnehmen,
//und stattdessen dieses Toggle-Field haben. <br />
//Hier werden dann alle Grundstücks-Daten angegeben. <br />
//Weil alle Immobilien auch Grundstücks-Angaben haben können, sollte keine
//Daten ausgeblendet werden, wenn Grundstücks-daten angegeben werden.
//				'),
				//$obj->standardField('ImmoDbGrundstueck'),
				$obj->standardField('ImmoDbGrundstueckTyp'),
				$obj->standardField('ImmoDbGrundstueckGrundstuecksflaeche'),
				$obj->standardField('ImmoDbGrundstueckBebauungMoeglich'),
				$obj->standardField('ImmoDbGrundstueckStatusQuoBaugenehmigung'),
				
				$obj->standardField('ImmoDbGrundstueckBebaubareFlaeche')
					->setRightTitle('ca. m²'),
				$obj->standardMultivalueCheckBoxField('ImmoDbGrundstueckEinfriedung', array(
					'ja',
					'nein',
					'teilweise', 
					'Natursteinmauer',
					'Zaun'
				)),
				$obj->standardMultivalueCheckBoxField('ImmoDbGrundstueckStrom', array(
					'ja',
					'nein',
					'beantragt', 
					'moeglich',
				)),
				$obj->standardMultivalueCheckBoxField('ImmoDbGrundstueckWasser', array(
					'ja',
					'nein',
					'moeglich',
					'eigenerBrunnen',
					'Gemeinschaftsbrunnen',
					'Stadtwasser'
				)),
				$obj->standardMultivalueCheckBoxField('ImmoDbGrundstueckAltbestandTyp', array(
					'Ruine',
					'Casita',
					'Wohnhaus'
				)),
				$obj->standardField('ImmoDbGrundstueckAltbestandCaM2')
					->setTitle('')
					->setRightTitle('ca. m²'),
				$obj->standardField('ImmoDbGrundstueckZufahrt')
			)
		);
		$grundstueckFields->addExtraClass('grundstueckFields');

		$grundstueckFieldsWrapper = DisplayLogicWrapper::create($grundstueckFields);
		
		//this is not working (I guess it's because all Grundstück fields are hidden by default)
		//$poolGroesse->displayIf("ImmoDbGrundstueckPool")->isChecked();

		$fields->addFieldsToTab('Root.Main', array(
			$obj->standardField('ImmoDbStatus')
				->setValue('Deaktiviert'),
			$obj->standardField('ImmoDbVermarktungsart')
				->setValue('KAUF'),
			$kp, //kaufpreis
			$nk, //miete
			$ab, // abschlag/traspaso
			$fwp,
			$obj->standardField('ImmoDbSichtbarkeitPreis')
				->setValue('PreisAnzeigen'),
			
			//Herr Fischer didn't want this option, so it's taken out
			//see https://projects.title.dk/projects/mallorca-immobilien-guide/tasks/74
			
			//$grundstueck = $obj->standardField('ImmoDbGrundstueck')
			//	->setTitle($obj->tFieldLabel('ImmoDbGrundstueck')),
			
			$grundstueckFieldsWrapper,
			//$objektart,

			//Flächen & Zimmer
			HeaderField::create('MainFlaechen','Flächen'),
			$ooGrundstueckflaeche = $obj->standardField('OpenImmoGrundstuecksflaeche')
				->setRightTitle('ca. m²'),
			$obj->standardField('OpenImmoGesamtflaeche') //we're using it as "Bebaute Fläche"
				->setRightTitle('ca. m²'),
			$obj->standardField('OpenImmoWohnflaeche')
				->setRightTitle('ca. m²'),
			//$obj->standardField('Nutzflaeche'),
			HeaderField::create('MainZimmer','Zimmer'),
			$obj->standardField('OpenImmoAnzahlSchlafzimmer'),
			$obj->standardField('OpenImmoAnzahlBadezimmer'),
			$obj->standardField('ImmoDbAnzahlBadezimmerEnSuite')
			// $obj->standardField('OpenImmoAnzahlSepWc'),
		));

		//$ooGrundstueckflaeche->hideIf("ImmoDbGrundstueck")->isChecked();

		
		
		
		//Grundstück display logic has been much simplified
		//See https://projects.title.dk/projects/mallorca-immobilien-guide/tasks/74
		//I'm leaving the code here for now for reference
		
		
		//Grundstück display logic:
		//If kategorie is "Grundstück", the Grundstück options should be shown by default
		//and the option for "weitere Grundstücksangaben" should not be shown
		//Furthermore, grundstück fläche should not be shown then - as we don't want the user to
		//add grundstücksfläche twice. For consistency the grundstück fläche is saved to the OpenImmo
		//grundstücksfläche before save for this case

		$grundstueckCat = OpenImmoObjektart::get()
			->filter('Identifier', 'openimmo-grundstueck')
			->first();
		if ($grundstueckCat && $grundstueckCat->exists()) {
			$grundstueckCatID = $grundstueckCat->ID;
			
			//displaying additional grundstück fields, if:
			$grundstueckFieldsWrapper
				////if "weitere Angaben zum Grundstück" is checked, or
				//->displayIf("ImmoDbGrundstueck")->isChecked()
				////if Grundstück has been chosen in the dropdown
				//->orIf("ImmoDbKategorieID")->isEqualTo($grundstueckCatID)
				->displayIf("ImmoDbKategorieID")->isEqualTo($grundstueckCatID);
			
			//hide "weitere Angaben zum Grundstück" if grundstück is chosen, as it's already shown
			//$grundstueck->hideIf("ImmoDbKategorieID")->isEqualTo($grundstueckCatID);
			
			//also hide grundstücksfläche if Grundstück is chosen in the dropddown
			//as we don't want that twice (and syncing of these is taken care of behind the scenes)
			$ooGrundstueckflaeche->hideIf("ImmoDbKategorieID")->isEqualTo($grundstueckCatID);
		}
		
		


		//$cat = OpenImmoObjektart::get()
		//	->filter('Identifier', 'openimmo-grundstueck')
		//	->first();
		//if ($cat && $cat->exists()) {
		
		//this is how we can hide these options
		//acutally we don't want to hide any options,
		//as Grundstück can/should be combinable with houses etc.
		//$neubau->hideIf("ImmoDbGrundstueck")->isChecked();
		//$sanierungsobjekt->hideIf("ImmoDbGrundstueck")->isChecked();
		
		
		//SEO
		//if ($obj->ID) {
		//	$fields->addFieldToTab('Root.Main',
		//		HeaderField::create('MainSeo','SEO')
		//	);
//
		//	//TODO this needs potentially to be customized if we choose
		//	//TODO to go with another url structure than "show"
		//	$urlSegment = self::get_and_remove($fields, 'URLSegment');
		//	$fields->addFieldToTab('Root.Main', $urlSegment);
//
		//	////Metadata (added in the DOAP module)
		//	//$fields->addFieldsToTab('Root.Main',
		//	//	array(
		//	//		new TextField("MetaTitle", $obj->fieldLabel('MetaTitle')),
		//	//		new TextareaField("MetaDescription", $obj->fieldLabel('MetaDescription'))
		//	//	)
		//	//);
		//}

		//Sonstiges

		$fields->addFieldsToTab('Root.Main', array(
			HeaderField::create('MainSonstiges','Sonstiges'),
			$neubau = $obj->standardField('ImmoDbNeubauErstbezug'),
			$sanierungsobjekt = $obj->standardField('ImmoDbSanierungsobjekt'),
		));
		
		
		
		if ($obj->ID) {
			$fields->addFieldToTab("Root.Main", LiteralField::create('OwnerDesc', '
					<div class="field text" id="OwnerDesc">
						<label class="left">Eigentümer</label>
						<div class="middleColumn">
							<p>' . $obj->Owner()->getName() . '</p>
						</div>
					</div>
					'
				)
			);
		}
		return $fields;
	}



	/**
	 * Geo
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function immodb_geo($fields, $obj) {

		$fields = OpenImmoCmsFields::open_immo_geo($fields, $obj);


		//removing unneeded geo fields
		$fields->removeByName('GeoMoreHeading');
		$fields->removeByName('OpenImmoFlur');
		$fields->removeByName('OpenImmoFlurstueck');
		$fields->removeByName('OpenImmoGemarkung');
		$fields->removeByName('OpenImmoLageImBau');
		$fields->removeByName('OpenImmoWohnungsnr');
		$fields->removeByName('OpenImmoRegionalerZusatz');
		$fields->removeByName('OpenImmoLageGebiet');
		
		//Customizations for Mallorca
		//This will need to be more configurable once the system
		//is used outside of Mallorca
		$fields->removeByName('BundeslandGemeindecode');
		$fields->removeByName('OpenImmoLand');
		$fields->addFieldToTab('Root.Geo',
			HiddenField::create('ImmoDbBaseLocation')
				->setValue(ImmoDb::config()->BaseLocation)
		);
		
		


		//$fields->addFieldToTab('Root.Geo',
		//	//this can both act as a many-many and a has-many relations
		//	$locationField = new TreeMultiselectField(
		//		'ImmoDbLocation',
		//		$obj->tFieldLabel('ImmoDbLocation'),
		//		//'ImmoDbLocation',
		//		'ImmoDbBaseRelation',
		//		'ID',
		//		'Title'
		//	));
		//
		////TODO this callback is what needs to be edited when admins are restricted to areas in the future
		//$callback = create_function('$obj', 'return $obj->class == "ImmoDbLocation" AND $obj->Enabled == 1;');
		//$locationField ->setFilterFunction ( $callback );
		//$locationField ->setChildrenMethod('Children');
		//$locationField->setSearchFunction(array($obj, 'locationSearchCallback'));


		//Location relations have been taken out:
		
		//$locSource = function(){
		//	$admin = false;
		//	$member = null;
		//	$parents = ImmoDbLocation::get()
		//		->filter('ParentID', 0)
		//		->filter('Enabled', 1);
		//	
		//	if (Permission::check("immodb-admins")) {
		//		$admin = true;
		//	} else {
		//		$member = Member::currentUser();
		//		$parents = $member->AdministerableLocations()
		//			->filter('ParentID', 0)
		//			->filter('Enabled', 1);
		//	}
		//	
		//
		//	$arr = array();
		//	
		//	//looping through all parents' childs
		//	foreach ($parents as $parent) {
		//		//$arr[$parent->ID] = $parent->Title;
		//		foreach ($parent->Children() as $child) {
		//			$arr[$child->ID] = $parent->Title . ' > ' . $child->Title;
		//		}
		//	}
		//	
		//	//additionally add sub locations if these have been set for non-admins
		//	if (!$admin) {
		//		$subLocations = $member->AdministerableLocations()
		//			->exclude('ParentID', 0)
		//			->filter('Enabled', 1);
		//		
		//		foreach ($subLocations as $loc) {
		//			$arr[$loc->ID] = $loc->Parent()->Title . ' > ' . $loc->Title;
		//		}
		//	}
		//	
		//	return $arr;
		//};
		//$locationField = ListBoxField::create('ImmoDbLocation',
		//	$obj->tFieldLabel('ImmoDbLocation'),
		//	$locSource())
		//	->setMultiple(true);
		//$fields->addFieldToTab('Root.Geo', $locationField);


		//Standort
		$fields->addFieldToTab('Root.Geo',
			$obj->standardMultivalueCheckBoxField('ImmoDbStandort', array(
				'Ortslage',
				'Ortsrand', 
				'Ortskern', 
				'Alleinlage', 
				'Laendlich', 
				'Gemeinschaftsanlage', 
				'Urbanisation', 
				'Altstadt'
			))
		);
		//Ausrichtung Terrasse/Balkon
		$fields->addFieldToTab('Root.Geo',
			$obj->standardMultivalueCheckBoxField('OpenImmoAusrichtungBalkonTerrasse', array(
				'NORD',
				'OST',
				'SUED',
				'WEST',
				'NORDOST',
				'NORDWEST',
				'SUEDOST',
				'SUEDWEST',
			))
		);
		
		//Topografie
		$fields->addFieldToTab("Root.Geo",
			$obj->standardField('ImmoDbTopografie')
		);


		//Blick
		$fields->addFieldToTab('Root.Geo',
			$obj->standardMultivalueCheckBoxField('OpenImmoAusblick', array(
				//Standard
				//'FERNE',
				//'SEE',
				//'BERGE',
				//'MEER',

				//Custom
				'Panoramablick',
				'MEER',
				'BERGE',
				'Landschaftblick',
				'Yachthafen',
				'Golfplatz',
				'Altstadt'
			))
		);
		
		//Erste Meereslinie
		$fields->addFieldsToTab('Root.Geo', array(
			$obj->standardField('ImmoDbErsteMeereslinie'),
			$obj->standardField('ImmoDbDirekterMeerzugang')
		)
		);

		
		return $fields;
	}


	/**
	 * Immobilie
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function immodb_immobilie($fields, $obj, $tabName = 'Immobilie') {

		$fields->addFieldsToTab("Root.$tabName", array(
			$baujahr = $obj->standardField('OpenImmoBaujahr'),
			
			$obj->standardMultivalueCheckBoxField('ImmoDbStrom', array(
				'Oeffentlich',
				'Solar',
				'Generator',
				'Wind',
				'OeffentlicherAnschlussMoeglich'
			)),

			$obj->standardMultivalueCheckBoxField('ImmoDbWasser', array(
				'Stadtwasser',
				'EigenerBrunnen',
				'Gemeinschaftsbrunnen',
				'Zisterne30000Liter',
				'EigenerBrunnenMoeglich'
			)),
			$obj->standardField('ImmoDbGemeinschaftkostenPaCa'),
			$obj->standardField('ImmoDbRenoviert'),
			$renoviertJahr = $obj->standardField('ImmoDbRenoviertJahr'),
			$obj->standardField('ImmoDbSaniert'),
			$saniertJahr = $obj->standardField('ImmoDbSaniertJahr'),
			$obj->standardField('ImmoDbTerrasseOffenCaM2'),
			$obj->standardField('ImmoDbTerrasseUeberdachtCaM2'),
			$obj->standardField('ImmoDbBalkonCaM2'),
			$obj->standardField('OpenImmoAnzahlEtagen'),
			$etage = $obj->standardField('OpenImmoEtage'),
			
			
			$obj->standardMultivalueCheckBoxField('OpenImmoStellplatzart', array(
				//Standard
				//'GARAGE',
				//'TIEFGARAGE',
				//'CARPORT',
				//'FREIPLATZ',
				//'PARKHAUS',
				//'DUPLEX',

				//Custom
				'Garage',
				'Doppelgarage',
				'Tiefgarage' ,
				'Freiflaeche',
				'Carport'
			)),

		));
		$baujahr->setRightTitle('ca.');
		$renoviertJahr->hideIf('ImmoDbRenoviert')->isEqualTo('KEINE_ANGABE');
		$saniertJahr->hideIf('ImmoDbSaniert')->isEqualTo('KEINE_ANGABE');
		$etage->setRightTitle('Bsp.: 0 = EG, 1 = 1. OG. …');

		return $fields;
	}


	/**
	 * Ausstattung
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function immodb_ausstattung($fields, $obj, $tabName = 'Ausstattung') {


		//There must be a nicer way of building groups … with multi columns etc …

		$KlimaFieldGroup = CompositeField::create(
			array(
				LabelField::create('Title', '<strong>Klima</strong>'),
				$obj->standardField('OpenImmoKlimatisiert'),
				$klima_vorbereitet = $obj->standardField('ImmoDbKlimaVorbereitet'),
				$klima_typ = $obj->standardField('ImmoDbKlimaTyp')
			)
		);


		$klima_vorbereitet->hideIf('OpenImmoKlimatisiert')->isChecked();
		$klima_typ->displayIf('OpenImmoKlimatisiert')->isChecked();

		
		$SwimmingPoolGroup = FieldGroup::create('Pool',
			array(

				// CompositeField::create( FieldGroup::create( LabelField::create('Title', '<strong>SwimmingPool</strong>') )
				// ),
				// CompositeField::create( FieldGroup::create( $obj->standardField('OpenImmoSwimmingpool')) ),
				CompositeField::create( FieldGroup::create( $pool_aussen = $obj->standardField('ImmoDbPoolAussen')) ),
				CompositeField::create( FieldGroup::create( $pool_indoor = $obj->standardField('ImmoDbPoolIndoor')) ),

				FieldGroup::create(
					// TODO
					// $obj->standardField('ImmoDbGrundstueckPool'),
					$poolGroesse = $obj->standardField('ImmoDbGrundstueckPoolGroesse')->setTitle('Pool Größe')
				)->addExtraClass('pool'),
			)
		);
		
		$SwimmingPoolGroup->addExtraClass('poolFields');
		
		// $pool_aussen->displayIf('OpenImmoSwimmingpool')->isChecked();
		// $pool_indoor->displayIf('OpenImmoSwimmingpool')->isChecked();
		// $poolGroesse->displayIf('OpenImmoSwimmingpool')->isChecked();



		$fields->addFieldsToTab("Root.$tabName", array(
				$obj->standardMultivalueCheckBoxField('OpenImmoHeizungsart', array(
					//Standard
					//'OFEN',
					//'ETAGE',
					//'ZENTRAL',
					//'FERN',
					//'FUSSBODEN',

					//Custom
					'ZENTRAL',
					'FUSSBODEN',
					'OFEN',
					'DezentraleHeizung'
				)),
				$obj->standardField('ImmoDbHeizungVorbereitet'),
				$obj->standardMultivalueCheckBoxField('OpenImmoBefeuerung', array(
					//Standard
					//'OEL',
					//'GAS',
					//'ELEKTRO',
					//'ALTERNATIV',
					//'SOLAR',
					//'ERDWAERME',
					//'LUFTWP',
					//'FERN',
					//'BLOCK',
					//'WASSER-ELEKTRO',
					//'PELLET',

					//Custom
					'OEL',
					'GAS',
					'ELEKTRO',
					'PELLET',
					'AlternativeEnergien',
					'Wärmepumpe'
				)),


				$KlimaFieldGroup,
				$SwimmingPoolGroup,

					$obj->standardMultivalueCheckBoxField('OpenImmoSicherheitstechnik', array(
						//Standard
						'ALARMANLAGE',
						//'KAMERA',
						//'POLIZEIRUF',
						//Custom
						'Videoueberwachung',
						'Wachdienst'
					)),


				$obj->standardField('ImmoDbDoppelVerglasung'),
				$obj->standardField('ImmoDbEinfachVerglasung'),
				$obj->standardField('ImmoDbAutoGartenbewaesserung'),
				$obj->standardField('ImmoDbVollausgestatteteKueche'),
				$obj->standardField('OpenImmoKamin'),

				$obj->standardMultivalueCheckBoxField('ImmoDbTv', array(
					'Kabel', 
					'SAT',
					'tdt'
				)),
				//$obj->standardField('OpenImmoKabelSatTv'),
				//$obj->standardField('OpenImmoDvVerkabelung'),
				//$obj->standardField('OpenImmoDvbt'),
				//$obj->standardField('OpenImmoKabelkanaele'),
		
				$obj->standardField('ImmoDbTelefon'),
				$obj->standardField('ImmoDbInternet'),
				$obj->standardField('ImmoDbEnergiezertifikat'),
			)
		);
		
		return $fields;
	}

	/**
	 * Extras
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function immodb_extras($fields, $obj, $tabName = 'Extras') {

		$fields->addFieldsToTab("Root.$tabName", array(
			$gaestehaus = $obj->standardField('ImmoDbGaestehaus'),
			$gaestehaus_ca_m2 = $obj->standardField('ImmoDbGaestehausCaM2'),
			$gaestehaus_bz = $obj->standardField('ImmoDbGaestehausZahlBz'),
			$gaestehaus_sz = $obj->standardField('ImmoDbGaestehausZahlSz'),
			$gaestehaus_kueche = $obj->standardField('ImmoDbGaestehausKueche'),

			$gaesteapartment = $obj->standardField('ImmoDbSeperatesGaesteapartment'),
			$gaesteapartment_ca_m2 = $obj->standardField('ImmoDbSeperatesGaesteapartmentCaM2'),
			$gaesteapartment_bz = $obj->standardField('ImmoDbSeperatesGaesteapartmentZahlBz'),
			$gaesteapartment_sz = $obj->standardField('ImmoDbSeperatesGaesteapartmentZahlSz'),
			$gaesteapartment_kueche = $obj->standardField('ImmoDbSeperatesGaesteapartmentKueche'),

			//$obj->standardMultivalueCheckBoxField('OpenImmoUnterkellert', array(
			//	//Standard
			//	'JA',
			//	'NEIN',
			//	'TEIL',
			//)),
			$obj->standardField('ImmoDbUnterkellert'),
			$kellerFlaeche = $obj->standardField('OpenImmoKellerflaeche'),
			//$obj->standardMultivalueCheckBoxField('OpenImmoFahrstuhl', array(
			//	//Standard
			//	'PERSONEN',
			//	'LASTEN',
			//)),
			$obj->standardField('ImmoDbFahrstuhl'),
			$obj->standardField('ImmoDbSommerkueche'),
			$obj->standardField('ImmoDbBBQ'),
			$obj->standardField('ImmoDbBodega'),
			$obj->standardField('ImmoDbFitnessraum'),
			$obj->standardField('ImmoDbDampfbad'),
			$obj->standardField('OpenImmoSauna'),
			$obj->standardField('OpenImmoWaschTrockenraum'),
			$obj->standardField('OpenImmoGaestewc'),
			$obj->standardField('ImmoDbTennisplatz'),
			$obj->standardField('OpenImmoRollstuhlgerecht'),
		));

		$gaestehaus_ca_m2->displayIf('ImmoDbGaestehaus')->isChecked();
		$gaestehaus_bz->displayIf('ImmoDbGaestehaus')->isChecked();
		$gaestehaus_sz->displayIf('ImmoDbGaestehaus')->isChecked();
		$gaestehaus_kueche->displayIf('ImmoDbGaestehaus')->isChecked();

		$gaesteapartment_ca_m2->displayIf('ImmoDbSeperatesGaesteapartment')->isChecked();
		$gaesteapartment_bz->displayIf('ImmoDbSeperatesGaesteapartment')->isChecked();
		$gaesteapartment_sz->displayIf('ImmoDbSeperatesGaesteapartment')->isChecked();
		$gaesteapartment_kueche->displayIf('ImmoDbSeperatesGaesteapartment')->isChecked();

		$kellerFlaeche->displayIf('ImmoDbUnterkellert')->isChecked();
		
		return $fields;
	}

	/**
	 * Texte
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function immodb_texte($fields, $obj) {
		
			$fields = OpenImmoCmsFields::open_immo_freitexte($fields, $obj, "Texte");
			
			$TitleOriginal = $obj->Title;

			$fields->dataFieldByName('OpenImmoObjekttitel__es_ES')->setDescription($TitleOriginal);
			$fields->dataFieldByName('OpenImmoObjekttitel__en_US')->setDescription($TitleOriginal);


			$MetaTitleOriginal = $obj->MetaTitle;

			$fields->dataFieldByName('MetaTitle__es_ES')->setDescription($MetaTitleOriginal);
			$fields->dataFieldByName('MetaTitle__en_US')->setDescription($MetaTitleOriginal);

			$MetaDescriptionOriginal = $obj->MetaDescription;

			$fields->dataFieldByName('MetaDescription__es_ES')->setDescription($MetaDescriptionOriginal);
			$fields->dataFieldByName('MetaDescription__en_US')->setDescription($MetaDescriptionOriginal);
			
			$OpenImmoDreizeiler_Original = $obj->OpenImmoDreizeiler;


			$fields->dataFieldByName('OpenImmoDreizeiler__es_ES')->setDescription($OpenImmoDreizeiler_Original);
			$fields->dataFieldByName('OpenImmoDreizeiler__en_US')->setDescription($OpenImmoDreizeiler_Original);



			$OpenImmoLage_Original = $obj->OpenImmoLage;

			$fields->dataFieldByName('OpenImmoLage__es_ES')->setDescription($OpenImmoLage_Original);
			$fields->dataFieldByName('OpenImmoLage__en_US')->setDescription($OpenImmoLage_Original);


			
			$OpenImmoAusstattBeschr_Original = $obj->OpenImmoAusstattBeschr;

			$fields->dataFieldByName('OpenImmoAusstattBeschr__es_ES')->setDescription($OpenImmoAusstattBeschr_Original);
			$fields->dataFieldByName('OpenImmoAusstattBeschr__en_US')->setDescription($OpenImmoAusstattBeschr_Original);

			
			$OpenImmoObjektbeschreibung_Original = $obj->OpenImmoObjektbeschreibung;

			$fields->dataFieldByName('OpenImmoObjektbeschreibung__es_ES')->setDescription($OpenImmoObjektbeschreibung_Original);
			$fields->dataFieldByName('OpenImmoObjektbeschreibung__en_US')->setDescription($OpenImmoObjektbeschreibung_Original);

			$OpenImmoSonstigeAngaben_Original = $obj->OpenImmoSonstigeAngaben;

			$fields->dataFieldByName('OpenImmoSonstigeAngaben__es_ES')->setDescription($OpenImmoSonstigeAngaben_Original);
			$fields->dataFieldByName('OpenImmoSonstigeAngaben__en_US')->setDescription($OpenImmoSonstigeAngaben_Original);


			//  $fields->addFieldsToTab("Root.Texte", array(
    		//	$obj->standardField('Objekttitel'),
	    	//	$obj->standardField('OpenImmoDreizeiler'),
		    //	$obj->standardField('OpenImmoLage'),
		    //	$obj->standardField('OpenImmoObjektbeschreibung'),
    		//	$obj->standardField('OpenImmoAusstattBeschr'),
	    	//	$obj->standardField('OpenImmoSonstigeAngaben'),
		    //));
		
		return $fields;
	}
	
	
	/**
	 * Infrastruktur
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function immodb_infrastruktur($fields, $obj, $tabName = 'Infrastruktur') {
		//Tabname could also be "Anbindung in KM"
		
		
		$fields->addFieldToTab("Root.$tabName", new HeaderField('InfrastrukturTODO','TODO: Spec not finalized'));
		
		$fields = OpenImmoCmsFields::open_immo_infrastruktur($fields, $obj, $tabName);
		
		$fields->addFieldToTab("Root.$tabName",
			KeyValueField::create('CustomDistanzen',
				$obj->tFieldLabel('CustomDistanzen'),
				$obj->tFieldLabels('CustomDistanzen', array(
					'FLUGHAFEN', //Standard
					'GOLFPLATZ',
					'YACHTHAFEN',
					'INTERN_SCHULEN',
					'STRAND', //Standard - though from DistanzenSport
					'RESTAURANT'
				))
			)
		);

		return $fields;
	}
	

}