<?php

/**
 * Helper/wrapper for the immobilie cms fields
 *
 *
 * @package immodb
 * @subpackage cmsfields
 */
class OpenImmoCmsFields extends ImmoDbCmsFields {


	/**
	 * All OpenImmo Fields
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function get_cms_fields($fields, $obj) {
		//Debug::dump($obj->ClassName);
		//Debug::dump($obj->db());
		//$fields = FieldList::create(TabSet::create("Root"));


		//Basics
		$fields = self::open_immo_basics($fields);

		//remove unused fields
		//$fields->removeByName('AssetsFolder'); //this should happen automatically
		//$fields->removeByName('Images');


		//Main
		$fields = self::open_immo_main($fields, $obj);


		//Bilder
		$fields = self::open_immo_bilder($fields, $obj);


		//Geo
		$fields = self::open_immo_geo($fields, $obj);


		//Preise
		$fields = self::open_immo_preise($fields, $obj);


		//Flächen
		$fields = self::open_immo_flaechen($fields, $obj);


		//Bieterverfahren
		$fields = self::open_immo_bieterverfahren($fields, $obj);


		//Versteigerung
		$fields = self::open_immo_versteigerung($fields, $obj);


		//Ausstattung
		$fields = self::open_immo_ausstattung($fields, $obj);


		//Infrastruktur
		$fields = self::open_immo_infrastruktur($fields, $obj);


		//Freitexte
		$fields = self::open_immo_freitexte($fields, $obj);


		//Verwaltungsobjekt
		$fields = self::open_immo_verwaltungsobjekt($fields, $obj);


		//Verwaltungstech
		$fields = self::open_immo_verwaltungstech($fields, $obj);

		//Kontaktperson(en)
		$fields = self::open_immo_kontaktperson($fields, $obj);
		
		//Admin
		$fields = self::open_immo_admin($fields, $obj);


		return $fields;
	}


	/**
	 * Basics
	 * @param $fields
	 * @return FieldList
	 */
	public static function open_immo_basics($fields) {
		//JS Hook - to let the JS identify the form properly
		$fields->addFieldToTab("Root.Main",
			HiddenField::create('ImmobilieJsHook')
		);
		return $fields;
	}

	/**
	 * Main
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_main($fields, $obj) {
		
		//We don't want "objekttitel" - instead we're using "Title"
		//objekttitel is saved on before write though, to conform with the open immo standard
		//$fields->addFieldsToTab('Root.Main', array(
		//	$obj->standardField('OpenImmoObjekttitel')
		//));

		//Objektart
		//$fields->addFieldToTab('Root.Main',
		//	new CheckboxSetField(
		//		'OpenImmoObjektart',
		//		$obj->tFieldLabel('OpenImmoObjektart'),
		//		OpenImmoObjektart::get()->map('ID', 'Title')
		//	));


		//$fields->addFieldToTab('Root.Main',
		//	new TreeMultiselectField(
		//		'TestHierarchyCategories',
		//		'TestHierarchyCategory',
		//		'TestHierarchyCategory'
		//	));
		
			
		//$fields->addFieldToTab('Root.Main',
		//	//this can both act as a many-many and a has-many relations
		//	$objektartField = new TreeMultiselectField(
		//	//$objektartField = new TreeDropdownField(
		//		'OpenImmoObjektart',
		//		$obj->tFieldLabel('OpenImmoObjektart'),
		//		'OpenImmoObjektart',
		//		'ID',
		//		'Title'
		//	));
		//
		//$callback = create_function('$obj', 'return $obj->class == "OpenImmoObjektart" AND $obj->Enabled == 1;');
		//$objektartField->setFilterFunction ( $callback );
		//$objektartField->setChildrenMethod('Children');
		////search seems overkill for objektart
		//$objektartField->setShowSearch(false);

		
		//Kategorie - single dropdown
		$fields->addFieldToTab('Root.Main',
			$kategorie = new ImmoDbObjektartDropdownField('ImmoDbKategorieID')
		);
        // nicer styling
        $kategorie->addExtraClass('dropdown');
		
		//Nutzungsart
		$fields->addFieldToTab('Root.Main',
			new CheckboxSetField(
				'OpenImmoNutzungsart',
				$obj->tFieldLabel('OpenImmoNutzungsart'),
				OpenImmoNutzungsart::get()->map('ID', 'Title')
			));
		//Vermarktungsart
		$fields->addFieldToTab('Root.Main',
			new CheckboxSetField(
				'OpenImmoVermarktungsart',
				$obj->tFieldLabel('OpenImmoVermarktungsart'),
				OpenImmoVermarktungsart::get()->map('ID', 'Title')
				//OpenImmoVermarktungsart::get_enabled_fieldmap()
			));



//		$fields->addFieldToTab("Root.Main",
//			$objektart = DropdownField::create('OpenImmoObjektart',
//				$obj->tFieldLabel('OpenImmoObjektart'),
//				$obj->tFieldLabels('OpenImmoObjektart', $obj->dbObject('OpenImmoObjektart')->enumValues())
//			)
//		);
//		$objektart->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//
//
//		//there is only one "zimmer" type - hence we don't show it
//		//		$fields->addFieldToTab("Root.Main",
//		//			$objektartZimmertyp = DropdownField::create('ObjektartZimmertyp',
//		//				$obj->tFieldLabel('ObjektartZimmertyp'),
//		//				$obj->tFieldLabels('ObjektartZimmertyp', $obj->dbObject('ObjektartZimmertyp')->enumValues())
//		//			)
//		//		);
//		//		$objektartZimmertyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		//		$objektartZimmertyp->displayIf("Objektart")->isEqualTo("zimmer");
//
//		//ObjektartWohnungtyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartWohnungtyp = DropdownField::create('ObjektartWohnungtyp',
//				$obj->tFieldLabel('ObjektartWohnungtyp'),
//				$obj->tFieldLabels('ObjektartWohnungtyp', $obj->dbObject('ObjektartWohnungtyp')->enumValues())
//			)
//		);
//		$objektartWohnungtyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartWohnungtyp->displayIf("Objektart")->isEqualTo("wohnung");
//
//		//ObjektartHaustyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartHaustyp= DropdownField::create('ObjektartHaustyp',
//				$obj->tFieldLabel('ObjektartHaustyp'),
//				$obj->tFieldLabels('ObjektartHaustyp', $obj->dbObject('ObjektartHaustyp')->enumValues())
//			)
//		);
//		$objektartHaustyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartHaustyp->displayIf("Objektart")->isEqualTo("haus");
//
//		//ObjektartGrundstTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartGrundstTyp = DropdownField::create('ObjektartGrundstTyp',
//				$obj->tFieldLabel('ObjektartGrundstTyp'),
//				$obj->tFieldLabels('ObjektartGrundstTyp', $obj->dbObject('ObjektartGrundstTyp')->enumValues())
//			)
//		);
//		$objektartGrundstTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartGrundstTyp->displayIf("Objektart")->isEqualTo("grundstueck");
//
//		//ObjektartBueroTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartBueroTyp= DropdownField::create('ObjektartBueroTyp',
//				$obj->tFieldLabel('ObjektartBueroTyp'),
//				$obj->tFieldLabels('ObjektartBueroTyp', $obj->dbObject('ObjektartBueroTyp')->enumValues())
//			)
//		);
//		$objektartBueroTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartBueroTyp->displayIf("Objektart")->isEqualTo("buero_praxen");
//
//		//ObjektartHandelTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartHandelTyp = DropdownField::create('ObjektartHandelTyp',
//				$obj->tFieldLabel('ObjektartHandelTyp'),
//				$obj->tFieldLabels('ObjektartHandelTyp', $obj->dbObject('ObjektartHandelTyp')->enumValues())
//			)
//		);
//		$objektartHandelTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartHandelTyp->displayIf("Objektart")->isEqualTo("einzelhandel");
//
//		//ObjektartGastgewTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartGastgewTyp= DropdownField::create('ObjektartGastgewTyp',
//				$obj->tFieldLabel('ObjektartGastgewTyp'),
//				$obj->tFieldLabels('ObjektartGastgewTyp', $obj->dbObject('ObjektartGastgewTyp')->enumValues())
//			)
//		);
//		$objektartGastgewTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartGastgewTyp->displayIf("Objektart")->isEqualTo("gastgewerbe");
//
//		//ObjektartHallenTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartHallenTyp = DropdownField::create('ObjektartHallenTyp',
//				$obj->tFieldLabel('ObjektartHallenTyp'),
//				$obj->tFieldLabels('ObjektartHallenTyp', $obj->dbObject('ObjektartHallenTyp')->enumValues())
//			)
//		);
//		$objektartHallenTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartHallenTyp->displayIf("Objektart")->isEqualTo("hallen_lager_prod");
//
//		//ObjektartLandTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartLandTyp= DropdownField::create('ObjektartLandTyp',
//				$obj->tFieldLabel('ObjektartLandTyp'),
//				$obj->tFieldLabels('ObjektartLandTyp', $obj->dbObject('ObjektartLandTyp')->enumValues())
//			)
//		);
//		$objektartLandTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartLandTyp->displayIf("Objektart")->isEqualTo("land_und_forstwirtschaft");
//
//		//ObjektartParkenTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartParkenTyp = DropdownField::create('ObjektartParkenTyp',
//				$obj->tFieldLabel('ObjektartParkenTyp'),
//				$obj->tFieldLabels('ObjektartParkenTyp', $obj->dbObject('ObjektartParkenTyp')->enumValues())
//			)
//		);
//		$objektartParkenTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartParkenTyp->displayIf("Objektart")->isEqualTo("parken");
//
//		//ObjektartSonstigeTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartSonstigeTyp= DropdownField::create('ObjektartSonstigeTyp',
//				$obj->tFieldLabel('ObjektartSonstigeTyp'),
//				$obj->tFieldLabels('ObjektartSonstigeTyp', $obj->dbObject('ObjektartSonstigeTyp')->enumValues())
//			)
//		);
//		$objektartSonstigeTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartSonstigeTyp->displayIf("Objektart")->isEqualTo("sonstige");
//
//		//ObjektartFreizeitTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartFreizeitTyp = DropdownField::create('ObjektartFreizeitTyp',
//				$obj->tFieldLabel('ObjektartFreizeitTyp'),
//				$obj->tFieldLabels('ObjektartFreizeitTyp', $obj->dbObject('ObjektartFreizeitTyp')->enumValues())
//			)
//		);
//		$objektartFreizeitTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartFreizeitTyp->displayIf("Objektart")->isEqualTo("freizeitimmobilie_gewerblich");
//
//		//ObjektartZinsTyp
//		$fields->addFieldToTab("Root.Main",
//			$objektartZinsTyp= DropdownField::create('ObjektartZinsTyp',
//				$obj->tFieldLabel('ObjektartZinsTyp'),
//				$obj->tFieldLabels('ObjektartZinsTyp', $obj->dbObject('ObjektartZinsTyp')->enumValues())
//			)
//		);
//		$objektartZinsTyp->setEmptyString(_t('ImmoDb.PLEASE_SELECT'));
//		$objektartZinsTyp->displayIf("Objektart")->isEqualTo("zinshaus_renditeobjekt");
//
//		$fields->addFieldToTab("Root.Main",
//			TextField::create('ObjektartZusatz', _t('Immobilie.OBJEKTARTZUSATZ', 'Objektart Zusatz'))
//				->setRightTitle('Ergänzungen wie z.B: finca zum Ferienhaus')
//		);


		return $fields;
	}

	/**
	 * Bilder
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */


	public static function open_immo_bilder($fields, $obj) { 
		
		
		//adding upload field - if item has already been saved
		if ($obj->ID && $obj->AssetsFolderID != 0) {
			
			$folder = $obj->getCalcAssetsFolderDirectory();
			$config = GridFieldConfig_RelationEditor::create(50);
			
			
			$config->addComponent($uf = new GridFieldBulkUpload('Image'));
			$config->addComponent($bm = new GridFieldBulkManager());
			
			// Unlink is confusing to the user. So I removed it.
			$bm->removeBulkAction('unLink');
			
			if (Permission::check("immodb-admins")) {
			} else {
				$bm->removeBulkAction('bulkEdit');
			}
			
			$config->addComponent($sr = new GridFieldSortableRows('SortOrder'));
			$sr->setAppendToTop(true);
			
			$config->addComponent($gt = new GridFieldGalleryThemeExtension('Image'));
			
		
			$config->removeComponentsByType($config->getComponentByType('GridFieldAddNewButton'));
			$config->removeComponentsByType($config->getComponentByType('GridFieldSortableHeader'));
			$config->removeComponentsByType($config->getComponentByType('GridFieldFilterHeader'));
			$config->removeComponentsByType($config->getComponentByType('GridFieldAddExistingAutocompleter'));

			// Delete Action is actually "unlink". Unlink is confusing to the user. So I removed it.
			$config->removeComponentsByType($config->getComponentByType('GridFieldDeleteAction'));
			
			
			if (Permission::check("immodb-admins")) {
			} else {
				
				// Only Admins can edit
				$config->removeComponentsByType($config->getComponentByType('GridFieldEditButton'));
				
			}
		
			
			$uf->setUfSetup('setFolderName', $folder);
			$uf->setUfConfig('sequentialUploads', true);

			

			$GridField = new GridField('ImmoImages', 'ImmoImage', $obj->ImmoImages()->sort('SortOrder'), $config);

			$fields->addFieldToTab(
				"Root.Bilder",
				$GridField
			);

		
			//$CustomValidator = new CustomUploadValidator();
			//$uf->bulkUploadField($GridField)->setValidator('CustomUploadValidator');
			// $uf->setUfValidatorSetup('isImageLargeEnough');
			
			
		} else {
			$fields->addFieldToTab('Root.Bilder',
				LiteralField::create('ImagesPlaceholder','Sie können Bilder nach dem ersten speichern hinzufügen.')
			);
		}
		return $fields;
	}
	
	

	/**
	 * Geo
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_geo($fields, $obj) {

		$fields->addFieldToTab("Root.Geo",
			HeaderField::create('AddressHeading','Adresse und Geokoordinaten.')
		);
		

		$fields->addFieldToTab("Root.Geo",
			FieldGroup::create(
				$obj->standardField('OpenImmoStrasse'),
				$obj->standardField('OpenImmoHausnummer')
			)->addExtraClass('Immobilie-Strasse-Hausnummer field')
		);
		$fields->addFieldToTab("Root.Geo",
			FieldGroup::create(
				$obj->standardField('OpenImmoPlz'),
				$obj->standardField('OpenImmoOrt')
			)->addExtraClass('Immobilie-Plz-Ort field')
		);
		$fields->addFieldToTab("Root.Geo",
			FieldGroup::create(
				$obj->standardField('OpenImmoBundesland')
					->setRightTitle('Bundesland, keine Festlegung der Kürzel, da evtl. auch ausländische Bundesländer betroffen sein können'),
				$obj->standardField('OpenImmoGemeindecode')
					->setRightTitle('Gemeindecode (GKZ), frei formulierbar')
			)
				->addExtraClass('Immobilie-Bundesland-Gemeindecode field')
				->setName('BundeslandGemeindecode')
		
		);

		$fields->addFieldToTab("Root.Geo", $obj->standardField('OpenImmoLand'));


		// $fields->addFieldToTab("Root.Geo",
		// 	HeaderField::create('LatLonHeading','Karte')
		// );
		//
		$fields->addFieldToTab("Root.Geo", new CompositeField(
			LiteralField::create('LatLonDesc',"
					<p class='message info warning'>Der Längen- und Breitengrad werden aus der von Ihnen angegebenen Adresse berechnet.<br />Bitte geben Sie erst die Adresse an und klicken Sie dann auf <strong>Ort suchen</strong>. Sie können die Position danach manuell verändern.<br /><strong>Statt einer vollständigen Adresse können Sie auch lediglich den Ortnamen eingeben.</strong></p>

				"))
		);

		$fields->addFieldToTab("Root.Geo",
			TextField::create('Address')
				->addExtraClass('hide')
		//->performReadonlyTransformation()
		);

		$fields->addFieldToTab("Root.Geo", new LatLongField(array(
				new TextField('OpenImmoGeokoordinatenBreitengrad','Breitengrad'),
				new TextField('OpenImmoGeokoordinatenLaengengrad','Längengrad')
			),
			array('Address')
		));

		$fields->addFieldToTab("Root.Geo",
			HeaderField::create('GeoMoreHeading','Weitere Angaben')
		);

		$fields->addFieldToTab("Root.Geo",
			$obj->standardField('OpenImmoFlur')
				->setRightTitle('Flurangabe')
		);
		$fields->addFieldToTab("Root.Geo",
			$obj->standardField('OpenImmoFlurstueck')
				->setRightTitle('Flurstückangabe')
		);
		$fields->addFieldToTab("Root.Geo",
			$obj->standardField('OpenImmoGemarkung')
				->setRightTitle('Gemarkungskennzeichen')
		);


		$fields->addFieldToTab("Root.Immobilie",
			FieldGroup::create(
				
				// NumericField::create('OpenImmoEtage', $obj->tFieldLabel('OpenImmoEtage'))
// 					->setDescription('Lage im Objekt(Etage), negative Zahlen für Kellergeschosse. Bsp. 0 = EG, 1 = 1. OG. usw …'),
				DropdownField::create(
				 	'OpenImmoEtage',
				 	$obj->tFieldLabel('OpenImmoEtage'),
				 	array(
						'0' => 'EG',
						'1' => '1 OG',
						'2'=> '2 OG',
						'3'=> '3 OG'
				 	)
				),
				
				
				NumericField::create('OpenImmoAnzahlEtagen', $obj->tFieldLabel('OpenImmoAnzahlEtagen'))
					->setRightTitle('Gesamtanzahl Etagen')
			)->addExtraClass('Immobilie-Etage-AnzahlEtagen field')
						

							 
							 
		);

		$fields->addFieldToTab("Root.Geo",
			$obj->standardMultivalueCheckBoxField('OpenImmoLageImBau', array(
				'LINKS',
				'RECHTS',
				'VORNE',
				'HINTEN',
			))
		);

		$fields->addFieldToTab("Root.Geo",
			$obj->standardField('OpenImmoWohnungsnr')
				->setRightTitle('Nummer der Wohnung bei Wohnanlagen')
		);

		$fields->addFieldToTab("Root.Geo",
			$obj->standardField('OpenImmoLageGebiet')
		);


		$fields->addFieldToTab("Root.Geo",
			$obj->standardField('OpenImmoRegionalerZusatz')
				->setRightTitle('Freier Text, z.B Landschaft/Kulturlandschaft: Plattensee; Stadtgebiet: Pöseldorf')
		);
		return $fields;
	}

	/**
	 * Preise
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_preise($fields, $obj) {
		$fields->addFieldsToTab("Root.Preise", array(
			$obj->standardField('OpenImmoKaufpreis'),
			$obj->standardField('OpenImmoNettokaltmiete'),
			$obj->standardField('OpenImmoKaltmiete'),
			$obj->standardField('OpenImmoWarmmiete'),
			$obj->standardField('OpenImmoNebenkosten'),
			$obj->standardField('OpenImmoHeizkostenEnthalten'),
			$obj->standardField('OpenImmoZzgMehrwertsteuer'),
			$obj->standardField('OpenImmoMietzuschlaege'),
			$obj->standardField('OpenImmoPacht'),
			$obj->standardField('OpenImmoErbpacht'),
			$obj->standardField('OpenImmoHausgeld'),
			$obj->standardField('OpenImmoMwstSatz'),
			$obj->standardField('OpenImmoMwstGesamnt'),
			$obj->standardField('OpenImmoWaehrung'),
			$obj->standardField('OpenImmoFreitextPreis'),
			$obj->standardField('OpenImmoXFache'),
			$obj->standardField('OpenImmoNettorendite'),
			$obj->standardField('OpenImmoNettorenditeSoll'),
			$obj->standardField('OpenImmoNettorenditeIst'),
			$obj->standardField('OpenImmoMieteinnahmenIst'),
			$obj->standardField('OpenImmoMieteinnahmenSoll'),
			$obj->standardField('OpenImmoErschliessungskosten'),
			$obj->standardField('OpenImmoKaution'),
			$obj->standardField('OpenImmoKautionText'),
			$obj->standardField('OpenImmogeschaeftsguthaben'),
			$obj->standardField('OpenImmoStpCarport'),
			$obj->standardField('OpenImmoStpDuplex'),
			$obj->standardField('OpenImmoStpFreiplatz'),
			$obj->standardField('OpenImmoStpGarage'),
			$obj->standardField('OpenImmoStpParkhaus'),
			$obj->standardField('OpenImmoStpFiefgarage'),
			$obj->standardField('OpenImmoStpSonstige'),
		));
		return $fields;
	}

	/**
	 * Flächen
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_flaechen($fields, $obj) {

		$fields->addFieldsToTab("Root.Flächen", array(
			ToggleCompositeField::create('FlaechenPrivatHolder', 'Privat',
				array(
					$obj->standardField('OpenImmoWohnflaeche'),
					$obj->standardField('OpenImmoNutzflaeche'),
					$obj->standardField('OpenImmoGesamtflaeche'),
				)
			)->setHeadingLevel(4),
			ToggleCompositeField::create('FlaechenGeschaeftlichHolder', 'Geschäftlich',
				array(
					$obj->standardField('OpenImmoLadenflaeche'),
					$obj->standardField('OpenImmoLagerflaeche'),
					$obj->standardField('OpenImmoVerkaufsflaeche'),
					$obj->standardField('OpenImmoFreiflaeche'),
					$obj->standardField('OpenImmoBueroflaeche'),
					$obj->standardField('OpenImmoBueroteilflaeche'),
					$obj->standardField('OpenImmoFensterfront'),
					$obj->standardField('OpenImmoVerwaltungsflaeche'),
					$obj->standardField('OpenImmoGastroflaeche'),
					$obj->standardField('OpenImmoGrundstuecksflaeche'),
				)
			)->setHeadingLevel(4),
			ToggleCompositeField::create('FlaechenZimmerHolder', 'Zimmer',
				array(
					$obj->standardField('OpenImmoAnzahlZimmer'),
					$obj->standardField('OpenImmoAnzahlSchlafzimmer'),
					$obj->standardField('OpenImmoAnzahlBadezimmer'),
					$obj->standardField('OpenImmoAnzahlSepWc'),
					$obj->standardField('OpenImmoAnzahlBalkone'),
					$obj->standardField('OpenImmoAnzahlTerassen'),
					$obj->standardField('OpenImmoBalkonTerasseFlaeche'),
					$obj->standardField('OpenImmoAnzahlWohnSchlafZimmer'),
				)
			)->setHeadingLevel(4),
			ToggleCompositeField::create('FlaechenMiscHolder', 'Misc',
				array(
					$obj->standardField('OpenImmoGartenflaeche'),
					$obj->standardField('OpenImmoKellerflaeche'),
					$obj->standardField('OpenImmoFensterfrontQm'),
					$obj->standardField('OpenImmoGrundstuecksfront'),
					$obj->standardField('OpenImmoDachbodenflaeche'),
					$obj->standardField('OpenImmoTeilbarAb'),
					$obj->standardField('OpenImmoBeheizbareFlaeche'),
					$obj->standardField('OpenImmoAnzahlStellplaetze'),
					$obj->standardField('OpenImmoPlaetzeGastraum'),
					$obj->standardField('OpenImmoAnzahlBetten'),
					$obj->standardField('OpenImmoAnzahlTagungsraeume'),
					$obj->standardField('OpenImmoVermiebareFlaeche'),
					$obj->standardField('OpenImmoAnzahlWohneinheiten'),
					$obj->standardField('OpenImmoAnzahlGewerbeeinheiten'),
					$obj->standardField('OpenImmoEinliegerwohnung'),
				)
			)->setHeadingLevel(4),
		));

		return $fields;
	}

	/**
	 * Bieterverfahren
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_bieterverfahren($fields, $obj) {
		$fields->addFieldsToTab("Root.Bieterverfahren", array(
			$obj->standardField('OpenImmoBeginnAngebotsphase'),
			$obj->standardField('OpenImmoBesichtigungstermin'),
			$obj->standardField('OpenImmoBesichtigungstermin2'),
			$obj->standardField('OpenImmoHoechstgebotZeigen'),
			$obj->standardField('OpenImmoMindestpreis'),
		));
		return $fields;
	}

	/**
	 * Versteigerung
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_versteigerung($fields, $obj) {

		$fields->addFieldsToTab("Root.Versteigerung", array(
			$obj->standardField('OpenImmoZwangsversteigerung'),
			$obj->standardField('OpenImmoAktenzeichen'),
			$obj->standardField('OpenImmoZvtermin'),
			$obj->standardField('OpenImmoZusatztermin'),
			$obj->standardField('OpenImmoAmtsgericht'),
		));
		return $fields;
	}

	/**
	 * Ausstattung
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_ausstattung($fields, $obj) {

		$fields->addFieldsToTab("Root.Ausstattung", array(
			$obj->standardField('OpenImmoAusstattKategorie'),
			$obj->standardField('OpenImmoWgGeeignet'),
			$obj->standardField('OpenImmoRaeumeVeraenderbar'),
			$obj->standardMultivalueCheckBoxField('OpenImmoBad', array(
				'DUSCHE',
				'WANNE',
				'FENSTER',
				'BIDET',
				'PISSOIR',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoKueche', array(
				'EBK',
				'OFFEN',
				'PANTRY',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoBoden', array(
				'FLIESEN',
				'STEIN',
				'TEPPICH',
				'PARKETT',
				'FERTIGPARKETT',
				'LAMINAT',
				'DIELEN',
				'KUNSTSTOFF',
				'ESTRICH',
				'DOPPELBODEN',
				'LINOLEUM',
				'MARMOR',
				'TERRAKOTTA',
				'GRANIT',
			)),
			$obj->standardField('OpenImmoKamin'),
			$obj->standardMultivalueCheckBoxField('OpenImmoHeizungsart', array(
				'OFEN',
				'ETAGE',
				'ZENTRAL',
				'FERN',
				'FUSSBODEN',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoBefeuerung', array(
				'OEL',
				'GAS',
				'ELEKTRO',
				'ALTERNATIV',
				'SOLAR',
				'ERDWAERME',
				'LUFTWP',
				'FERN',
				'BLOCK',
				'WASSER-ELEKTRO',
				'PELLET',
			)),
			$obj->standardField('OpenImmoKlimatisiert'),
			$obj->standardMultivalueCheckBoxField('OpenImmoFahrstuhl', array(
				'PERSONEN',
				'LASTEN',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoStellplatzart', array(
				'GARAGE',
				'TIEFGARAGE',
				'CARPORT',
				'FREIPLATZ',
				'PARKHAUS',
				'DUPLEX',
			)),
			$obj->standardField('OpenImmoGartennutzung'),
			$obj->standardMultivalueCheckBoxField('OpenImmoAusrichtungBalkonTerrasse', array(
				'NORD',
				'OST',
				'SUED',
				'WEST',
				'NORDOST',
				'NORDWEST',
				'SUEDOST',
				'SUEDWEST',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoMoebliert', array(
				'VOLL',
				'TEIL',
			)),
			$obj->standardField('OpenImmoRollstuhlgerecht'),
			$obj->standardField('OpenImmoKabelSatTv'),
			$obj->standardField('OpenImmoDvbt'),
			$obj->standardField('OpenImmoBarrierefrei'),
			$obj->standardField('OpenImmoSauna'),
			$obj->standardField('OpenImmoSwimmingpool'),
			$obj->standardField('OpenImmoWaschTrockenraum'),
			$obj->standardField('OpenImmoWintergarten'),
			$obj->standardField('OpenImmoDvVerkabelung'),
			$obj->standardField('OpenImmoKran'),
			$obj->standardField('OpenImmoHebebuehne'),
			$obj->standardField('OpenImmoGastterrasse'),
			$obj->standardField('OpenImmoStromanschlusswert'),
			$obj->standardField('OpenImmoKantineCafeteria'),
			$obj->standardField('OpenImmoTeekueche'),
			$obj->standardField('OpenImmoHallenhoehe'),
			$obj->standardMultivalueCheckBoxField('OpenImmoAngeschlGastronomie', array(
				'HOTELRESTAURANT',
				'BAR',
			)),
			$obj->standardField('OpenImmoBrauereibindung'),
			$obj->standardField('OpenImmoSporteinrichtungen'),
			$obj->standardField('OpenImmoWellnessbereich'),
			$obj->standardMultivalueCheckBoxField('OpenImmoServiceleistungen', array(
				'BETREUTES_WOHNEN',
				'CATERING',
				'REINIGUNG',
				'EINKAUF',
				'WACHDIENST',
			)),
			$obj->standardField('OpenImmoTelefonFerienimmobilie'),
			$obj->standardMultivalueCheckBoxField('OpenImmoSicherheitstechnik', array(
				'ALARMANLAGE',
				'KAMERA',
				'POLIZEIRUF',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoUnterkellert', array(
				'JA',
				'NEIN',
				'TEIL',
			)),
			$obj->standardField('OpenImmoAbstellraum'),
			$obj->standardField('OpenImmoFahrradraum'),
			$obj->standardField('OpenImmoRolladen'),
			$obj->standardMultivalueCheckBoxField('OpenImmoDachform', array(
				'KRUEPPELWALMDACH',
				'MANSARDDACH',
				'PULTDACH',
				'SATTELDACH',
				'WALMDACH',
				'FLACHDACH',
				'PYRAMIDENDACH',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoBauweise', array(
				'MASSIV',
				'FERTIGTEILE',
				'HOLZ',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoAusbaustufe', array(
				'BAUSATZHAUS',
				'AUSBAUHAUS',
				'SCHLUESSELFERTIGMITKELLER',
				'SCHLUESSELFERTIGOHNEBODENPLATTE',
				'SCHLUESSELFERTIGMITBODENPLATTE',
			)),
			$obj->standardMultivalueCheckBoxField('OpenImmoEnergietyp', array(
				'PASSIVHAUS',
				'NIEDRIGENERGIE',
				'NEUBAUSTANDARD',
				'KFW40',
				'KFW60',
			)),
			$obj->standardField('OpenImmoBibliothek'),
			$obj->standardField('OpenImmoDachboden'),
			$obj->standardField('OpenImmoGaestewc'),
			$obj->standardField('OpenImmoKabelkanaele'),
			$obj->standardField('OpenImmoSeniorengerecht'),
		));
		return $fields;
	}

	/**
	 * Infrastruktur
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_infrastruktur($fields, $obj, $tabName = 'Infrastruktur') {

		$fields->addFieldsToTab("Root.$tabName", array(
			$obj->standardField('OpenImmoZulieferung'),
			$obj->standardMultivalueCheckBoxField('OpenImmoAusblick', array(
				'FERNE',
				'SEE',
				'BERGE',
				'MEER',
			)),
			//this is a special field that allows to add several distances via dropdowns
			KeyValueField::create('OpenImmoDistanzen',
				$obj->tFieldLabel('OpenImmoDistanzen'),
				$obj->tFieldLabels('OpenImmoDistanzen', array(
					'FLUGHAFEN',
					'FERNBAHNHOF',
					'AUTOBAHN',
					'US_BAHN',
					'BUS',
					'KINDERGAERTEN',
					'GRUNDSCHULE',
					'HAUPTSCHULE',
					'REALSCHULE',
					'GESAMTSCHULE',
					'GYMNASIUM',
					'ZENTRUM',
					'EINKAUFSMOEGLICHKEITEN',
					'GASTSTAETTEN',
				))
			),
			//this is a special field that allows to add several distances via dropdowns
			KeyValueField::create('OpenImmoDistanzenSport',
				$obj->tFieldLabel('OpenImmoDistanzenSport'),
				$obj->tFieldLabels('OpenImmoDistanzenSport', array(
					'STRAND',
					'SEE',
					'MEER',
					'SKIGEBIET',
					'SPORTANLAGEN',
					'WANDERGEBIETE',
					'NAHERHOLUNG',
				))
			)
		));

		return $fields;
	}

	/**
	 * Freitexte
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_freitexte($fields, $obj, $tabName = 'Freitexte') {

		//$fields->addFieldsToTab("Root.Freitexte", array(
		//	//$obj->standardField('Objekttitel'),
		//	$obj->standardField('OpenImmoDreizeiler'),
		//	$obj->standardField('OpenImmoLage'),
		//	$obj->standardField('OpenImmoAusstattBeschr'),
		//	$obj->standardField('OpenImmoObjektbeschreibung'),
		//	$obj->standardField('OpenImmoSonstigeAngaben'),
		//));


		$tabSet = $obj->getTranslatableTabSet($title = 'Root', $showNativeFields = true);
		
		
		$fields->addFieldToTab("Root.$tabName",
			$tabSet
		);
		
		return $fields;
	}

	
	/**
	 * Verwaltungsobjekt
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_verwaltungsobjekt($fields, $obj) {
		$fields->addFieldsToTab("Root.Verwaltungsobjekt", array(
			$obj->standardField('OpenImmoObjektadresseFreigeben'),
			$obj->standardField('OpenImmoVerfuegbarAb'),
			$obj->standardField('OpenImmoAbdatum'),
			$obj->standardField('OpenImmoBisdatum'),
			$obj->standardField('OpenImmoMinMietdauer'),
			$obj->standardField('OpenImmoMaxMietdauer'),
			$obj->standardField('OpenImmoVersteigerungstermin'),
			$obj->standardField('OpenImmoWbsSozialwohnung'),
			$obj->standardField('OpenImmoVermietet'),
			$obj->standardField('OpenImmoGruppennummer'),
			$obj->standardField('OpenImmoZugang'),
			$obj->standardField('OpenImmoLaufzeit'),
			$obj->standardField('OpenImmoMaxPersonen'),
			$obj->standardField('OpenImmoNichtraucher'),
			$obj->standardField('OpenImmoHaustiere'),
			$obj->standardField('OpenImmoGeschlecht'),
			$obj->standardField('OpenImmoDenkmalgeschuetzt'),
			$obj->standardField('OpenImmoAlsFerien'),
			$obj->standardField('OpenImmoGewerbliche_nutzung'),
			$obj->standardField('OpenImmoBranchen'),
			$obj->standardField('OpenImmoHochhaus'),
		));
		return $fields;
	}

	/**
	 * Verwaltungstech
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_verwaltungstech($fields, $obj) {

		$fields->addFieldsToTab("Root.Verwaltungstech", array(
			$obj->standardField('OpenImmoObjektnrIntern'),
			$obj->standardField('OpenImmoObjektnrExtern'),
			$obj->standardMultivalueCheckBoxField('OpenImmoBauweise', array(
				'CHANGE',
				'DELETE',
			)),
			$obj->standardField('OpenImmoAktivVon'),
			$obj->standardField('OpenImmoAktivBis'),
			$obj->standardField('OpenImmoOpenimmoObid'),
			$obj->standardField('OpenImmoKennungUrsprung'),
			$obj->standardField('OpenImmoStandVom'),
			$obj->standardField('OpenImmoWeitergabeGenerell'),
			$obj->standardField('OpenImmoWeitergabePositiv'),
			$obj->standardField('OpenImmoWeitergabeNegativ'),
			$obj->standardField('OpenImmoGruppenKennung'),
			$obj->standardField('OpenImmoMaster'),
		));
		return $fields;
	}

	/**
	 * Kontaktperson(en)
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_kontaktperson($fields, $obj) {
		
		$k = new GridField(
			'OpenImmoKontaktperson', 
			'Kontaktperson(en)', 
			$obj->OpenImmoKontaktperson(),
			new GridFieldConfig_RelationEditor()
		);
		
		$fields->addFieldToTab("Root.Kontaktperson", 
			$k
		);
		
		return $fields;
	}
	

	/**
	 * Admin
	 * @param FieldList $fields
	 * @param OpenImmoImmobilie $obj
	 * @return FieldList
	 */
	public static function open_immo_admin($fields, $obj) {
		if (Permission::check("immodb-admins")) {
			$fields->addFieldsToTab("Root.Admin", array(
					LiteralField::create('AdminDesc', '<em>Dieser Bereich ist nur für Administratoren zugänglich</em>'),
					DropdownField::create('OwnerID', $obj->tFieldLabel('OwnerID'))
						->setSource(Member::get()->map())
						->setEmptyString(_t('ImmoDb.PLEASE_SELECT'))
				)
			);
		}
		return $fields;
	}


}