<?php

class MegaMenuCategory extends DataObject {

	private static $db = array(
		"Title" => "Varchar"
	);
	
	private static $has_many = array(
		"Pages" => "Page"
	);
	
	private static $has_one = array(
		'ParentPage' => 'Page'
	);
	
	public function getCMSFields() {
		
		$fields = parent::getCMSFields();
		//$fields->removeByName('ParentID');
		return $fields;
	}
	
	private static $default_sort = '"Title"';
	
	private static $singular_name = 'MenuCategory';

	private static $plural_name = 'MenuCategories';
	
	
	public function onAfterWrite() {
		parent::onAfterWrite();
	}

}
