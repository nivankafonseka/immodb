<?php

/**
 * Immobilie Base DataObject
 * This is the base for the Immobilie type, which contains helpers etc.
 * Furthermore, while {@OpenImmoImmobilie} is strictly adhering to the OpenImmo standard,
 * this class contains additional items, that we want present in any kind of Immobilie
 *
 *
 *
 * TODO This could potentially also be used to implement auto generation or URLS for
 * additional languages
 *
 * @package immodb
 * @subpackage model
 */
class ImmobilieBaseDataObject extends DataObjectAsPage implements Wishable {


	static $listing_page_class = 'ImmobilienHolder';

	static $extensions = array(
		"MappableData",
		"AssetsFolderExtension",
		"UploadDirRules_DataObjectExtension"
	);

	/******************************* FIELDS & RELATIONS *******************************/

	static $db = array (
		'Demo' => 'Boolean',	//allowing for defining "demo" Immobilien in sub projects,
		//that can be filtered out when on live mode
		'DemoIdentifier' => 'Varchar(255)',
		'AllowPurchase' => 'Boolean'
	);

	static $has_one = array (
		//'KontaktpersonFoto' => 'Image',
		'Owner' => 'Member'
	);

	static $many_many = array (
		// 'Images' => 'Image',
		'TestCategories' => 'ImmoDbTestCategory', //just for testing the filtering module
		'TestHierarchyCategories' => 'TestHierarchyCategory',
	);
	
	static $has_many = array (
		'ImmoImages' => 'ImmoImage',
		'FeaturedImages' => 'FeaturedImage'
	);
	
	private static $many_many_extraFields = array(
		// 'Images' => array('SortOrder' => 'Int'),
		// 'FeaturedImages' => array('SortOrder' => 'Int')
	);



	private static $defaults = array(
		'AllowPurchase' => true
	);
	
	private static $global_allow_purchase = true;

	private static $create_table_options = array(
	    'MySQLDatabase' => 'ENGINE=InnoDB'
	);
	

	/* BUYABLE TEST */
	/**
	 * Create a new OrderItem to add to an order.
	 *
	 * @param int $quantity
	 * @param boolean $write
	 * @return OrderItem new OrderItem object
	 */
	public function createItem($quantity = 1,$filter = array()) {
		return;
	}

	/**
	 * Checks if the wishable can be purchased.
	 *
	 * @return boolean
	 */
	public function canPurchase($member = null) {
		return true;
	}


	/**
 	* The price the customer gets this wishable for, with any additional additions or subtractions.
 	*/
	public function sellingPrice() {
		return '1';
	}

	/* BUYABLE END */



	/******************************* BEFORE & AFTER WRITE *******************************/

	public function onBeforeWrite() {
		// check on first write action, aka "database row creation"
		// (ID-property is not set)
		if (!$this->ID) {
			$u = Member::currentUser();
			//setting owner on first write
			if ($u) {
				$this->OwnerID = $u->ID;
			}
		}
		parent::onBeforeWrite();
		
		//Link Settings for translated objects
		//The main language url is taken care of in DataObjectAsPage::onBeforeWrite
		//This code is taken from there and amended
		//In general this is a bit of duplicate code, but we can't get around that without changing
		//the doap module, which is a little bit out of the scope of this project at this point (July 2014)

		//$locales = self::get_target_locales();
		$locales = ImmoDbTranslatableDataObject::get_target_locales_public();

		//if ($this->ID) {
		//echo 'now writing...';
	
		foreach($locales as $locale){

			if($locale != Translatable::default_locale()) {
				$defaults = $this->config()->defaults;

				$origLocale = i18n::get_locale();
				i18n::set_locale($locale);
				$defaultTitle = _t('Immobilie.SINGULARNAME');
				i18n::set_locale($origLocale);

				//$defaultUrl = strtolower("$defaultTitle-$this->ID");
				//ID is not always available at this point, so we go for the default title
				$defaultUrl = strtolower("$defaultTitle");
				
				//echo $defaultUrl;
				//echo "what?";
				
				$urlSegmentFieldName = TranslatableDataObject::localized_field('URLSegment',$locale);
				$urlSegment = $this->$urlSegmentFieldName;
				//echo $urlSegment;
				
				$titleFieldName = TranslatableDataObject::localized_field('OpenImmoObjekttitel',$locale);
				$title = $this->$titleFieldName;
				//if (strlen($title) == 0) {
				//	$title = $defaultTitle;
				//}

				$metaDescriptionFieldName = TranslatableDataObject::localized_field('MetaDescription',$locale);
				$metaDescription = $this->$metaDescriptionFieldName;

				//if((!$urlSegment || $urlSegment == $defaults['URLSegment']) && $title != $defaults['Title']) {
				
				//if no url and no title have been set
				if (!$urlSegment && strlen($title) == 0) {
					
					//set default url
					$this->$urlSegmentFieldName = $defaultUrl;
				
				//else if a title has been set and there's no url segment, or only the default one
				} else if ((($urlSegment == $defaultUrl) || !$urlSegment) && strlen($title) > 0) {
					
					//set a url based on the title
					$this->$urlSegmentFieldName = $this->generateLocaleSpecificURLSegment($title, $locale);
					
				//else if the url has been changed - do sanity checks
				} else if($this->isChanged($urlSegmentFieldName)) {
					// Make sure the URLSegment is valid for use in a URL
					$segment = preg_replace('/[^A-Za-z0-9]+/','-',$urlSegment);
					$segment = preg_replace('/-+/','-',$segment);

					// If after sanitising there is no URLSegment, give it a reasonable default
					if(!$segment) {
						$segment = $defaultUrl;
					}


					$this->$urlSegmentFieldName = $segment;
				}

				// Ensure that this object has a non-conflicting URLSegment value.
				$count = 2;

				$xURLSegment = $this->$urlSegmentFieldName;
				
				
				$ID = $this->ID;
				
				//echo $xURLSegment . " " . $ID;

				while($this->LookForExistingTranslatableURLSegment($xURLSegment, $urlSegmentFieldName, $ID)) {
					$xURLSegment = preg_replace('/-[0-9]+$/', null, $xURLSegment) . '-' . $count;
					$count++;
				}
				//echo $xURLSegment;

				$this->$urlSegmentFieldName =$xURLSegment;
				
			}
		}
		//}
		
	}

	/**
	 * Locale specific url segment generation
	 */
	public function generateLocaleSpecificURLSegment($title, $locale) {
		$url = parent::generateURLSegment($title);
		
		if ($locale == 'de_DE') {
			$url = str_replace('-and-', '-und-', $url);
		} elseif ($locale == 'es_ES') {
			$url = str_replace('-and-', '-y-', $url);
		}
		
		
		return $url;
	}


	protected function onAfterWrite() {
		parent::onAfterWrite();

		//Creating the assets dir if it hasn't been created yet
		if ($this->AssetsFolderID == 0) {
			$this->FindOrMakeAssetsFolder(
				$this->getCalcAssetsFolderDirectory(),
				true
			);
		}
	}
	

	/**
	 * Check if there is already a DOAP with this URLSegment
	 */
	public function LookForExistingTranslatableURLSegment($URLSegment, $fieldname, $ID) {
		return Immobilie::get()->filter(
			$fieldname,
			$URLSegment
		)->exclude('ID', $ID)->exists();
	}
	
	

	/**
	 * Require default records
	 */
	public function requireDefaultRecords() {
		parent::requireDefaultRecords();

		//Immodb users have access to properties owned by themselves
		ImmoDbSecurityHelper::default_group(
			'immodb-users',
			'ImmoDB Users',
			null,
			array(
				'CMS_ACCESS_ImmoDbAdmin'
			)
		);
		//Immodb admins have access to all properties
		ImmoDbSecurityHelper::default_group(
			'immodb-admins',
			'ImmoDB Administrators',
			'immodb-users',
			array(
				'CMS_ACCESS_ImmoDbAdmin'
			)
		);

	}

	/******************************* URL HANDLING *******************************/
	
	public function Link($action = null) {
		//Hack for search results
		if($item = DataObjectAsPage::get()->byID($this->ID)) {
			//Build link
			if($listingPage = $item->getListingPage()) {
				$urlSegment = $item->T('URLSegment');
				
				return Controller::join_links($listingPage->Link(), 'show', $urlSegment, $action) . "/";
			}
		}
	}
	
	
	public function AbsoluteLink($action = null) {
		//Hack for search results
		if($item = DataObjectAsPage::get()->byID($this->ID)) {
			//Build link
			if($listingPage = $item->getListingPage()) {
				$urlSegment = $item->T('URLSegment');
				
				return Controller::join_links($listingPage->AbsoluteLink(), 'show', $urlSegment, $action) . "/";
			}
		}
	}
	
	
	

	/******************************* GETTERS & SETTERS *******************************/


	/**
	 * Generate custom metatags to display on the DataObject Item page
	 */
	public function MetaTags($includeTitle = true)
	{
		$tags = "";
		$locale = Translatable::get_current_locale();


		if($locale != Translatable::default_locale()) {
			$metaTitleFieldName = TranslatableDataObject::localized_field('MetaTitle',$locale);
			$metaTitle = $this->$metaTitleFieldName;
		} else {
			$metaTitle = $this->Title;
		}


		if($includeTitle === true || $includeTitle == 'true') {
			$tags .= "<title>" . Convert::raw2xml(($metaTitle) ? $metaTitle : $this->Title) . "</title>\n";
		}

		// $tags .= "<meta name=\"generator\" content=\"SilverStripe - http://silverstripe.org\" />\n";

		$charset = ContentNegotiator::get_encoding();
		$tags .= "<meta http-equiv=\"Content-type\" content=\"text/html; charset=$charset\" />\n";

		if($locale != Translatable::default_locale()) {
			$metaDescriptionFieldName = TranslatableDataObject::localized_field('MetaDescription',$locale);
			$metaDescription  = $this->$metaDescriptionFieldName;
		} else {
			$metaDescription = $this->MetaDescription;
		}

		if ($metaDescription) {
			$tags .= "<meta name=\"description\" content=\"" . Convert::raw2att($metaDescription) . "\" />\n";
		}
		$this->extend('MetaTags', $tags);
		return $tags;
	}




	/**
	 * Sorted Images
	 * @return mixed
	 */
	public function SortedImages(){
		return $this->ImmoImages();
	}

	public function SortedFeaturedImages(){
		return $this->FeaturedImages();
	}


	/**
	 * First Image
	 * @return mixed
	 */
	public function getFirstImage(){
		return $this->SortedImages()->First();
	}

	/**
	 * CMS Thumbnail (if it exists)
	 * @return string
	 */
	public function getCalcThumbnail(){
		$img = $this->getFirstImage();
		if ($img && $img->exists())  {
			return $img->Thumbnail();
		} else {
			return "No thumbnail available";
		}
	}

	// UploadDirRulesInterface requirements
	/**
	 * Calculation of the assets folder directory
	 */
	function getCalcAssetsFolderDirectory(){
		$dir = 'immodb/immobilie/' . $this->ID . '/images/';
		return $dir;
	}
	/**
	 * Message in the "save first" dialog
	 */
	function getMessageSaveFirst(){
		//use default
		return false;
	}
	/**
	 * Message in the "upload directory" label
	 */
	function getMessageUploadDirectory(){
		//use default
		return false;
	}



	/******************************* CRUD SETTINGS *******************************/

	public function canCreate($member = null) {
		return true;
	}

	public function canView($member = null) {
		//this is a little tricky, as from the front end everything should be viewable
		//for now we return true, but at some point we might need to find a solution
		//that allows certain groups to only see certain parts from the backend
		return true;
		//return $this->canEdit($member);
	}

	public function canEdit($member = null) {
		
		
		if (!$member) {
			$member = Member::currentUser();
		}
		if (
			!$this->ID ||
			$member->ID == $this->OwnerID ||
			Permission::check("immodb-admins")
		) {
			return true;
		}
	}

	public function canDelete($member = null) {
		return $this->canEdit($member);
	}


	/******************************* HELPERS *******************************/

	/**
	 * Helper method for standard fields
	 * @param string $fieldName
	 * @return FormField
	 */
	public function standardField($fieldName) {
		$label = $this->tFieldLabel($fieldName);
		$dbFields = $this->db();
		$field = null;
		if (isset ($dbFields[$fieldName])) {
			$type = $dbFields[$fieldName];
			if ($type == 'Decimal(19,4)') {
				$field = new PriceField($fieldName, $label);
			} elseif(substr($type, 0, 4) == 'Enum') {
				$enumVals = $this->dbObject($fieldName)->enumValues();

				$field = OptionsetField::create($fieldName, $label,
					$this->tFieldLabels($fieldName, $enumVals)
				);
				if (in_array('KEINE_ANGABE', $enumVals)) {
					$field->setValue('KEINE_ANGABE');
				}
			} else {
				$obj = $this->dbObject($fieldName);
				if ($obj) {
					$field = $obj->scaffoldFormField($label);
				}
			}
		}

		if (!$field) {
			$field = LiteralField::create($fieldName, 'ERROR: ' . $fieldName . ' could not be found.');
		}

		return $field;
	}

	/**
	 * Helper method for standard multivalue checkboxes
	 * TODO in the future the array should be removed, and this should be configurable
	 * @param string $fieldName
	 * @param array $values
	 * @return MultiValueCheckboxField
	 */
	public function standardMultivalueCheckBoxField($fieldName, $values) {
		$field = MultiValueCheckboxField::create($fieldName,
			$this->tFieldLabel($fieldName),
			$this->tFieldLabels($fieldName, $values)
		);
		return $field;
	}


	/**
	 * Translated field label
	 * @param string $fieldName
	 * @return string
	 */
	public function tFieldLabel($fieldName) {
		$class = 'Immobilie';
		return ImmoDbTranslationHelper::fieldlabel($class, $fieldName);
	}

	/**
	 * Translated field labels
	 * @param string $fieldName
	 * @param array $arr
	 * @return array
	 */
	public function tFieldLabels($fieldName, $arr) {
		$class = 'Immobilie';
		return ImmoDbTranslationHelper::fieldlabels($class, $fieldName, $arr);
	}

	/**
	 * Get the listing page to view this Immobilie on
	 * As {@see ImmobilienFilterPage} extends {@see ImmobilienHolder}
	 * we need to explicitly return {@see ImmobilienHolder}
	 */
	public function getListingPage(){
		$listingClass = $this->stat('listing_page_class');
		//if(Controller::curr() instanceof $listingClass) {
		//	$listingPage = Controller::curr();
		//} else {
			$listingPage = $listingClass::get()
				->filter('ClassName', $listingClass)
				->First();
		//}

		return $listingPage;
	}

	/**
	 * Get a set of content languages (for quick language navigation)
	 * @example
	 * <code>
	 * <!-- in your template -->
	 * <ul class="langNav">
	 * 		<% loop Languages %>
	 * 		<li><a href="$Link" class="$LinkingMode" title="$Title.ATT">$Language</a></li>
	 * 		<% end_loop %>
	 * </ul>
	 * </code>
	 *
	 * @return ArrayList|null
	 */
	public function Languages(){
		
		$locales = ImmoDbTranslatableDataObject::get_target_locales_public();

		$currentLocale = Translatable::get_current_locale();


		$langSet = ArrayList::create();
		foreach($locales as $locale){

			if($locale != $currentLocale) {
				$urlSegmentFieldName = TranslatableDataObject::localized_field('URLSegment',$locale);

				$controller = Controller::curr();
	
				$translation = $controller->hasTranslation($locale) ? $controller->getTranslation($locale) : null;
				$link = '/';
				if ($translation) {
					$baseLink = $translation->Link() . 'show/';
					$link = $baseLink . $this->$urlSegmentFieldName . '/';
				}

				$langSet->push(new ArrayData(array(
					// the locale (eg. en_US)
					'Locale' => $locale,
					// locale conforming to rfc 1766
					'RFC1766' => i18n::convert_rfc1766($locale),
					// the language 2 letter code (eg. EN)
					'Language' => DBField::create_field('Varchar',
							strtoupper(i18n::get_lang_from_locale($locale))),
					// the language as written in its native language
					'Title'	=> DBField::create_field('Varchar', ucfirst(html_entity_decode(
							i18n::get_language_name(i18n::get_lang_from_locale($locale), true),
							ENT_NOQUOTES, 'UTF-8'))),
					// linking mode (useful for css class)
					'LinkingMode' => $currentLocale == $locale ? 'current' : 'link',
					// link to the translation or the home-page if no translation exists for the current page
					'Link' => $link)
				));

			}

		}


		return $langSet;

	}
	
	
	
	public function getCMSFields(){
		//We don't want all that scaffolding
		//$fields = parent::getCMSFields();
		$fields = FieldList::create(TabSet::create("Root"));

		//here we re-create the functionality from the doap module
		//(mostly copied from there)

		//Add the status/view link
//		if($this->ID)
//		{
//			if($this->isVersioned)
//			{
//				$status = $this->getStatus();
//
//				$color = '#E88F31';
//				$links = sprintf(
//					"<a target=\"_blank\" class=\"ss-ui-button\" data-icon=\"preview\" href=\"%s\">%s</a>", $this->Link() . '?stage=Stage', 'Draft'
//				);
//
//				if($status == 'Published')
//				{
//					$color = '#000';
//					$links .= sprintf(
//						"<a target=\"_blank\" class=\"ss-ui-button\" data-icon=\"preview\" href=\"%s\">%s</a>", $this->Link() . '?stage=Live', 'Published'
//					);
//
//					if($this->hasChangesOnStage())
//					{
//						$status .= ' (changed)';
//						$color = '#428620';
//					}
//				}
//
//				$statusPill = '<h3 class="doapTitle" style="background: '.$color.';">'. $status . '</h3>';
//			}
//			else
//			{
//				$links = sprintf(
//					"<a target=\"_blank\" class=\"ss-ui-button\" data-icon=\"preview\" href=\"%s\">%s</a>", $this->Link() . '?stage=Stage', 'View'
//				);
//
//				$statusPill = "";
//			}
//
//			$fields->addFieldToTab('Root.Main', new LiteralField('',
//				'<div class="doapToolbar">
//					' . $statusPill . '
//					<p class="doapViewLinks">
//						' . $links . '
//					</p>
//				</div>'
//			));
//		}
		
		
		//Preview button
		$links = sprintf(
			"<a target=\"_blank\" class=\"ss-ui-button\" data-icon=\"preview\" href=\"%s\">%s</a>", $this->AbsoluteLink() . '?stage=Stage', 'View'
		);
		$statusPill = "";


		$fields->addFieldToTab('Root.Main', new LiteralField('',
			'<div class="doapToolbar">
				' . $statusPill . '
		<p class="doapViewLinks">
			' . $links . '
		</p>
			</div>'
		));



		//Remove Scafolded fields
		$fields->removeFieldFromTab('Root.Main', 'URLSegment');
		$fields->removeFieldFromTab('Root.Main', 'Status');
		$fields->removeFieldFromTab('Root.Main', 'Version');
		$fields->removeFieldFromTab('Root.Main', 'MetaTitle');
		$fields->removeFieldFromTab('Root.Main', 'MetaDescription');
		$fields->removeByName('Versions');

		$fields->addFieldToTab('Root.Main', new TextField('Title'));

//		if($this->ID)
//		{
//			$urlsegment = new SiteTreeURLSegmentField("URLSegment", $this->fieldLabel('URLSegment'));
//
//			if($this->getListingPage()) {
//				$prefix = $this->getListingPage()->AbsoluteLink('show').'/';
//			} else {
//				$prefix = Director::absoluteBaseURL() . 'listing-page/show/';
//			}
//			$urlsegment->setURLPrefix($prefix);
//
//			$helpText = _t('SiteTreeURLSegmentField.HelpChars', ' Special characters are automatically converted or removed.');
//			$urlsegment->setHelpText($helpText);
//			$fields->addFieldToTab('Root.Main', $urlsegment);
//		}

		$fields->addFieldToTab('Root.Main', new HTMLEditorField('Content'));

		$fields->addFieldToTab('Root.Main',new ToggleCompositeField('Metadata', 'Metadata',
			array(
				new TextField("MetaTitle", $this->fieldLabel('MetaTitle')),
				new TextareaField("MetaDescription", $this->fieldLabel('MetaDescription'))
			)
		));

		//$fields->push(new HiddenField('PreviewURL', 'Preview URL', $this->StageLink()));
		//$fields->push(new TextField('CMSEditURL', 'Preview URL', $this->CMSEditLink()));



		//removing some of that doap functionality
		$fields->removeByName('Content');
		$fields->removeByNAme('Metadata');


		return $fields;


	}
	
}