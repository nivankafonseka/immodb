<?php

/**
 * Immobilie (OpenImmo Standard)
 *
 * @package immodb
 * @subpackage model
 */
class OpenImmoImmobilie extends ImmobilieBaseDataObject implements UploadDirRulesInterface {


	/******************************* FIELDS & RELATIONS *******************************/

	/**
	 * By setting this via the individual project's config.yml, you can
	 * select which particular options you want to show for any enum field on this object
	 */
	//TODO what to do with this?
	private static $enabled_enum_options = array();


	static $db = array (
		//'OpenImmoObjektart' => 'Enum("zimmer,wohnung,haus,grundstueck,buero_praxen,einzelhandel,gastgewerbe,hallen_lager_prod,land_und_forstwirtschaft,parken,sonstige,freizeitimmobilie_gewerblich,zinshaus_renditeobjekt")',
		//'OpenImmoObjektartZimmertyp' => 'Enum("ZIMMER")',
		//'OpenImmoObjektartWohnungtyp' => 'Enum("DACHGESCHOSS,MAISONETTE,LOFT-STUDIO-ATELIER,PENTHOUSE,TERRASSEN,ETAGE,ERDGESCHOSS,SOUTERRAIN,APARTMENT,FERIENWOHNUNG,GALERIE,ROHDACHBODEN,ATTIKAWOHNUNG,KEINE_ANGABE")',
		//'OpenImmoObjektartHaustyp' => 'Enum("REIHENHAUS,REIHENEND,REIHENMITTEL,REIHENECK,DOPPELHAUSHAELFTE,EINFAMILIENHAUS,STADTHAUS,BUNGALOW,VILLA,RESTHOF,BAUERNHAUS,LANDHAUS,SCHLOSS,ZWEIFAMILIENHAUS,MEHRFAMILIENHAUS,FERIENHAUS,BERGHUETTE,CHALET,STRANDHAUS,LAUBE-DATSCHE-GARTENHAUS,APARTMENTHAUS,BURG,HERRENHAUS,FINCA,RUSTICO,FERTIGHAUS,KEINE_ANGABE")',
		//'OpenImmoObjektartGrundstTyp' => 'Enum("WOHNEN,GEWERBE,INDUSTRIE,LAND_FORSTWIRSCHAFT,FREIZEIT,GEMISCHT,GEWERBEPARK,SONDERNUTZUNG,SEELIEGENSCHAFT")',
		//'OpenImmoObjektartBueroTyp' => 'Enum("BUEROFLAECHE,BUEROHAUS,BUEROZENTRUM,LOFT_ATELIER,PRAXIS,PRAXISFLAECHE,PRAXISHAUS,AUSSTELLUNGSFLAECHE,COWORKING,SHARED_OFFICE")',
		//'OpenImmoObjektartHandelTyp' => 'Enum("LADENLOKAL,EINZELHANDELSLADEN,VERBRAUCHERMARKT,EINKAUFSZENTRUM,KAUFHAUS,FACTORY_OUTLET,KIOSK,VERKAUFSFLAECHE,AUSSTELLUNGSFLAECHE")',
		//'OpenImmoObjektartGastgewTyp' => 'Enum("GASTRONOMIE,GASTRONOMIE_UND_WOHNUNG,PENSIONEN,HOTELS,WEITERE_BEHERBERGUNGSBETRIEBE,BAR,CAFE,DISCOTHEK,RESTAURANT,RAUCHERLOKAL,EINRAUMLOKAL")',
		//'OpenImmoObjektartHallenTyp' => 'Enum("HALLE,INDUSTRIEHALLE,LAGER,LAGERFLAECHEN,LAGER_MIT_FREIFLAECHE,HOCHREGALLAGER,SPEDITIONSLAGER,PRODUKTION,WERKSTATT,SERVICE,FREIFLAECHEN,KUEHLHAUS")',
		//'OpenImmoObjektartLandTyp' => 'Enum("LANDWIRTSCHAFTLICHE_BETRIEBE,BAUERNHOF,AUSSIEDLERHOF,GARTENBAU,ACKERBAU,WEINBAU,VIEHWIRTSCHAFT,JAGD_UND_FORSTWIRTSCHAFT,TEICH_UND_FISCHWIRTSCHAFT,SCHEUNEN,REITERHOEFE,SONSTIGE_LANDWIRTSCHAFTSIMMOBILIEN,ANWESEN,JAGDREVIER")',
		//'OpenImmoObjektartParkenTyp' => 'Enum("STELLPLATZ,CARPORT,DOPPELGARAGE,DUPLEX,TIEFGARAGE,BOOTSLIEGEPLATZ,EINZELGARAGE,PARKHAUS,TIEFGARAGENSTELLPLATZ,PARKPLATZ_STROM")',
		//'OpenImmoObjektartSonstigeTyp' => 'Enum("PARKHAUS,TANKSTELLE,KRANKENHAUS,SONSTIGE")',
		//'OpenImmoObjektartFreizeitTyp' => 'Enum("SPORTANLAGEN,VERGNUEGUNGSPARKS_UND_CENTER,FREIZEITANLAGE")',
		//'OpenImmoObjektartZinsTyp' => 'Enum("MEHRFAMILIENHAUS,WOHN_UND_GESCHAEFTSHAUS,GESCHAEFTSHAUS,BUEROGEBAEUDE,SB_MAERKTE,EINKAUFSCENTREN,WOHNANLAGEN,VERBRAUCHERMAERKTE,INDUSTRIEANLAGEN,PFLEGEHEIM,SANATORIUM,SENIORENHEIM,ETREUTES-WOHNEN")',
		//'OpenImmoObjektartZusatz' => 'Varchar(255)',
		'OpenImmoPlz' => 'Varchar(255)',
		'OpenImmoOrt' => 'Varchar(255)',
		'OpenImmoGeokoordinatenBreitengrad' => 'Varchar(255)',
		'OpenImmoGeokoordinatenLaengengrad' => 'Varchar(255)',
		'OpenImmoStrasse' => 'Varchar(255)',
		'OpenImmoHausnummer' => 'Varchar(255)',
		'OpenImmoBundesland' => 'Varchar(255)',
		'OpenImmoLand' => 'Varchar(255)',
		'OpenImmoGemeindecode' => 'Varchar(255)',
		'OpenImmoFlur' => 'Varchar(255)',
		'OpenImmoFlurstueck' => 'Varchar(255)',
		'OpenImmoGemarkung' => 'Varchar(255)',
		'OpenImmoEtage' => 'Varchar',
		'OpenImmoAnzahlEtagen' => 'Int',
		'OpenImmoLageImBau' => 'MultiValueField',
		'OpenImmoWohnungsnr' => 'Varchar(255)',
		'OpenImmoLageGebiet' => 'Enum("WOHN,GEWERBE,INDUSTRIE,MISCH,NEUBAU,ORTSLAGE,SIEDLUNG,STADTRAND,STADTTEIL,STADTZENTRUM,NEBENZENTRUM,1A,1B")',
		'OpenImmoRegionalerZusatz' => 'Varchar(255)',


		//Preise
		'OpenImmoKaufpreis' => 'Decimal(19,4)',
		'OpenImmoNettokaltmiete' => 'Decimal(19,4)',
		'OpenImmoKaltmiete' => 'Decimal(19,4)',
		'OpenImmoWarmmiete' => 'Decimal(19,4)',
		'OpenImmoNebenkosten' => 'Decimal(19,4)',
		'OpenImmoHeizkostenEnthalten' => 'Decimal(19,4)',
		'OpenImmoHeizkosten' => 'Decimal(19,4)',
		'OpenImmoZzgMehrwertsteuer' => 'Decimal(19,4)',
		'OpenImmoMietzuschlaege' => 'Decimal(19,4)',
		'OpenImmoPacht' => 'Decimal(19,4)',
		'OpenImmoErbpacht' => 'Decimal(19,4)',
		'OpenImmoHausgeld' => 'Decimal(19,4)',
		'OpenImmoMwstSatz' => 'Int',
		'OpenImmoWaehrung' => 'Varchar(3)',
		'OpenImmoMwstGesamt' => 'Decimal',
		'OpenImmoFreitextPreis' => 'Varchar(255)',
		'OpenImmoXFache' => 'Varchar(255)',
		'OpenImmoNettorendite' => 'Int',
		'OpenImmoNettorenditeSoll' => 'Int',
		'OpenImmoNettorenditeIst' => 'Int',
		'OpenImmoMieteinnahmenIst' => 'Int',
		'OpenImmoMieteinnahmenSoll' => 'Int',
		'OpenImmoErschliessungskosten' => 'Int',
		'OpenImmoKaution' => 'Int',
		'OpenImmoKautionText' => 'Varchar(255)',
		'OpenImmogeschaeftsguthaben' => 'Int',
		'OpenImmoStpCarport' => 'MultiValueField',
		'OpenImmoStpDuplex' => 'MultiValueField',
		'OpenImmoStpFreiplatz' => 'MultiValueField',
		'OpenImmoStpGarage' => 'MultiValueField',
		'OpenImmoStpParkhaus' => 'MultiValueField',
		'OpenImmoStpFiefgarage' => 'MultiValueField',
		'OpenImmoStpSonstige' => 'MultiValueField',

		//Flächen
		'OpenImmoWohnflaeche' => 'Int',
		'OpenImmoNutzflaeche' => 'Int',
		'OpenImmoGesamtflaeche' => 'Int',
		'OpenImmoLadenflaeche' => 'Int',
		'OpenImmoLagerflaeche' => 'Int',
		'OpenImmoVerkaufsflaeche' => 'Int',
		'OpenImmoFreiflaeche' => 'Int',
		'OpenImmoBueroflaeche' => 'Int',
		'OpenImmoBueroteilflaeche' => 'Int',
		'OpenImmoFensterfront' => 'Int',
		'OpenImmoVerwaltungsflaeche' => 'Int',
		'OpenImmoGastroflaeche' => 'Int',
		'OpenImmoGrundstuecksflaeche' => 'Int',
		'OpenImmoAnzahlZimmer' => 'Int',
		'OpenImmoAnzahlSchlafzimmer' => 'Int',
		'OpenImmoAnzahlBadezimmer' => 'Int',
		'OpenImmoAnzahlSepWc' => 'Int',
		'OpenImmoAnzahlBalkone' => 'Int',
		'OpenImmoAnzahlTerassen' => 'Int',
		'OpenImmoBalkonTerasseFlaeche' => 'Int',
		'OpenImmoAnzahlWohnSchlafZimmer' => 'Int',
		'OpenImmoGartenflaeche' => 'Int',
		'OpenImmoKellerflaeche' => 'Int',
		'OpenImmoFensterfrontQm' => 'Int',
		'OpenImmoGrundstuecksfront' => 'Int',
		'OpenImmoDachbodenflaeche' => 'Int',
		'OpenImmoTeilbarAb' => 'Int',
		'OpenImmoBeheizbareFlaeche' => 'Int',
		'OpenImmoAnzahlStellplaetze' => 'Int',
		'OpenImmoPlaetzeGastraum' => 'Int',
		'OpenImmoAnzahlBetten' => 'Int',
		'OpenImmoAnzahlTagungsraeume' => 'Int',
		'OpenImmoVermiebareFlaeche' => 'Int',
		'OpenImmoAnzahlWohneinheiten' => 'Int',
		'OpenImmoAnzahlGewerbeeinheiten' => 'Int',
		'OpenImmoEinliegerwohnung' => 'Int',

		//Bieterverfahren
		'OpenImmoBeginnAngebotsphase' => 'Date',
		'OpenImmoBesichtigungstermin' => 'Date',
		'OpenImmoBesichtigungstermin2' => 'Date',
		'OpenImmoHoechstgebotZeigen' => 'Boolean',
		'OpenImmoMindestpreis' => 'Int',

		//Versteigerung
		'OpenImmoZwangsversteigerung' => 'Boolean',
		'OpenImmoAktenzeichen' => 'Varchar(255)',
		'OpenImmoZvtermin' => 'SS_Datetime',
		'OpenImmoZusatztermin' => 'SS_Datetime',
		'OpenImmoAmtsgericht' => 'Varchar(255)',

		//Ausstattung
		'OpenImmoAusstattKategorie' => 'Enum("STANDARD,GEHOBEN,LUXUS")',
		'OpenImmoWgGeeignet' => 'Boolean',
		'OpenImmoRaeumeVeraenderbar' => 'Boolean',
		'OpenImmoBad' => 'MultiValueField',
		'OpenImmoKueche' => 'MultiValueField',
		'OpenImmoBoden' => 'MultiValueField',
		'OpenImmoKamin' => 'Boolean',
		'OpenImmoHeizungsart' => 'MultiValueField',
		'OpenImmoBefeuerung' => 'MultiValueField',
		'OpenImmoKlimatisiert' => 'Boolean',
		'OpenImmoFahrstuhl' => 'MultiValueField',
		'OpenImmoStellplatzart' => 'MultiValueField',
		'OpenImmoGartennutzung' => 'Boolean',
		'OpenImmoAusrichtBalkonTerrasse' => 'MultiValueField',
		'OpenImmoMoebliert' => 'MultiValueField',
		'OpenImmoRollstuhlgerecht' => 'Boolean',
		'OpenImmoKabelSatTv' => 'Boolean',
		'OpenImmoDvbt' => 'Boolean',
		'OpenImmoBarrierefrei' => 'Boolean',
		'OpenImmoSauna' => 'Boolean',
		'OpenImmoSwimmingpool' => 'Boolean',
		'OpenImmoWaschTrockenraum' => 'Boolean',
		'OpenImmoWintergarten' => 'Boolean',
		'OpenImmoDvVerkabelung' => 'Boolean',
		'OpenImmoRampe' => 'Boolean',
		'OpenImmoHebebuehne' => 'Boolean',
		'OpenImmoKran' => 'Boolean',
		'OpenImmoGastterrasse' => 'Boolean',
		'OpenImmoStromanschlusswert' => 'Varchar(255)',
		'OpenImmoKantineCafeteria' => 'Boolean',
		'OpenImmoTeekueche' => 'Boolean',
		'OpenImmoHallenhoehe' => 'Varchar(255)',
		'OpenImmoAngeschlGastronomie' => 'MultiValueField',
		'OpenImmoBrauereibindung' => 'Boolean',
		'OpenImmoSporteinrichtungen' => 'Boolean',
		'OpenImmoWellnessbereich' => 'Boolean',
		'OpenImmoServiceleistungen' => 'MultiValueField',
		'OpenImmoTelefonFerienimmobilie' => 'Boolean',
		'OpenImmoSicherheitstechnik' => 'MultiValueField',
		'OpenImmoUnterkellert' => 'MultiValueField',
		'OpenImmoAbstellraum' => 'Boolean',
		'OpenImmoFahrradraum' => 'Boolean',
		'OpenImmoRolladen' => 'Boolean',
		'OpenImmoDachform' => 'MultiValueField',
		'OpenImmoBauweise' => 'MultiValueField',
		'OpenImmoAusbaustufe' => 'MultiValueField',
		'OpenImmoEnergietyp' => 'MultiValueField',
		'OpenImmoBibliothek' => 'Boolean',
		'OpenImmoDachboden' => 'Boolean',
		'OpenImmoGaestewc' => 'Boolean',
		'OpenImmoKabelkanaele' => 'Boolean',
		'OpenImmoSeniorengerecht' => 'Boolean',

		//Zustand/Angaben
		'OpenImmoBaujahr' => 'Int',

		//Infrastruktur
		'OpenImmoZulieferung' => 'Boolean',
		'OpenImmoAusblick' => 'MultiValueField',
		'OpenImmoDistanzen' => 'MultiValueField',
		'OpenImmoDistanzenSport' => 'MultiValueField',

		//Freitexte
		'OpenImmoObjekttitel' => 'Varchar(255)',
		'OpenImmoDreizeiler' => 'Text',
		'OpenImmoLage' => 'Text',
		'OpenImmoAusstattBeschr' => 'Text',
		'OpenImmoObjektbeschreibung' => 'Text',
		'OpenImmoSonstigeAngaben' => 'Text',

		//VerwaltungObjekt
		'OpenImmoObjektadresseFreigeben' => 'Boolean',
		'OpenImmoVerfuegbarAb' => 'Varchar(255)',
		'OpenImmoAbdatum' => 'Date',
		'OpenImmoBisdatum' => 'Date',
		'OpenImmoMinMietdauer' => 'Varchar(255)',
		'OpenImmoMaxMietdauer' => 'Varchar(255)',
		'OpenImmoVersteigerungstermin' => 'Date',
		'OpenImmoWbsSozialwohnung' => 'Boolean',
		'OpenImmoVermietet' => 'Boolean',
		'OpenImmoGruppennummer' => 'Int',
		'OpenImmoZugang' => 'Int',
		'OpenImmoLaufzeit' => 'Int',
		'OpenImmoMaxPersonen' => 'Int',
		'OpenImmoNichtraucher' => 'Int',
		'OpenImmoHaustiere' => 'Boolean',
		'OpenImmoGeschlecht' => 'Varchar(255)',
		'OpenImmoDenkmalgeschuetzt' => 'Boolean',
		'OpenImmoAlsFerien' => 'Boolean',
		'OpenImmoGewerbliche_nutzung' => 'Boolean',
		'OpenImmoBranchen' => 'Varchar(255)',
		'OpenImmoHochhaus' => 'Boolean',

		//VerwaltungTechn
		'OpenImmoObjektnrIntern' => 'Int',
		'OpenImmoObjektnrExtern' => 'Int',
		'OpenImmoAktion' => 'MultiValueField',
		'OpenImmoAktivVon' => 'Date',
		'OpenImmoAktivBis' => 'Date',
		'OpenImmoOpenimmoObid' => 'Int',
		'OpenImmoKennungUrsprung' => 'Varchar(255)',
		'OpenImmoStandVom' => 'Date',
		'OpenImmoWeitergabeGenerell' => 'Boolean',
		'OpenImmoWeitergabePositiv' => 'Boolean',
		'OpenImmoWeitergabeNegativ' => 'Boolean',
		'OpenImmoGruppenKennung' => 'Int',
		//'Master' => 'Int',
	);

	static $many_many = array (
		'OpenImmoVermarktungsart' => 'OpenImmoVermarktungsart',
		'OpenImmoNutzungsart' => 'OpenImmoNutzungsart',
		'OpenImmoObjektart' => 'OpenImmoObjektart',
		'OpenImmoKontaktperson' => 'OpenImmoKontaktperson',
	);


	private static $translatable_fields = array(
		'OpenImmoObjekttitel',
		'MetaTitle',
		'MetaDescription',
		'URLSegment',
		'OpenImmoDreizeiler',
		'OpenImmoLage',
		'OpenImmoAusstattBeschr',
		'OpenImmoObjektbeschreibung',
		'OpenImmoSonstigeAngaben',
	);
	
	private static $defaults = array(
		'OpenImmoEtage' => 'NA'
	);
	

	/******************************* BEFORE & AFTER WRITE *******************************/

	public function onBeforeWrite() {
		// check on first write action, aka "database row creation"
		// (ID-property is not set)
		if (!$this->ID) {
		}

		//Auto updating OpenImmoObjekttitel from Title
		$this->OpenImmoObjekttitel = $this->Title;

		parent::onBeforeWrite();
	}


	protected function onAfterWrite() {
		$this->doonAfterWrite();
		parent::onAfterWrite();
	}
	//public acessor for the on after write stuff - e.g. for the demo creation
	public function doonAfterWrite(){
		

	}
	
	

	/**
	 * Require default records
	 */
	public function requireDefaultRecords() {
		parent::requireDefaultRecords();
	}


	/******************************* GETTERS & SETTERS *******************************/

//taken out as DataObjectAsPage actually has a title
//	/**
//	 * Returns the title (needed for admin)
//	 * @return string
//	 */
	// public function getTitle() {
	// 	return $this->ImmoDbUniqueID . ' - '. $this->OpenImmoObjekttitel;
	// }

	/**
	 * List of Vermarktungsarten
	 * @return string
	 */
	public function getVermarktungsartList(){
		$str = "";
		foreach ($this->OpenImmoVermarktungsart() as $obj) {
			$str .= $obj->Title . ", ";
		}
		return rtrim($str, ", ");
	}

	/**
	 * List of Nutzungsarten
	 * @return string
	 */
	public function getNutzungsartList(){
		$str = "";
		foreach ($this->OpenImmoNutzungsart() as $obj) {
			$str .= $obj->Title . ", ";
		}
		return rtrim($str, ", ");
	}


	/**
	 * List of ObjektartList
	 * @return string
	 */
	public function getOpenImmoObjektartList(){
		$str = "";
		$idList = $this->OpenImmoObjektart()->getIDList();
		foreach ($this->OpenImmoObjektart() as $obj) {
			if ($obj->ParentID == 0) {
				$str .= $obj->Title;
				$children = $obj->AllChildrenIncludingDeleted();
				if ($children && $children->exists()) {
					$substr = " (";
					foreach ($children as $child) {
						if (in_array($child->ID, $idList)) {
							$substr .= $child->Title . ", ";
						}
					}
					$substr = rtrim($substr, ", ");
					$substr .= ")";
					$str .= $substr;
				}
				$str .= ", ";
			}
				
		}
		return rtrim($str, ", ");
	}
	
	/**
	 * Translated Objektart
	 * @return string
	 */
	public function getObjektartNice() {
		return 'currently not implemented';
		//if ($this->Objektart) {
		//	return ImmoDbTranslationHelper::fieldlabel($this->ClassName, 'OpenImmoObjektart', $this->OpenImmoObjektart);
		//} else {
		//	return '-';
		//}
	}


	// Mappable interface requirements
	public function getLatitude() {
		return $this->OpenImmoGeokoordinatenBreitengrad;
	}
	public function getLongitude() {
		return $this->OpenImmoGeokoordinatenLaengengrad;
	}
	public function getMapContent() {
		//below is not currently implemented
		//return GoogleMapUtil::sanitize($this->renderWith('MapBubbleMember'));
	}
	public function getMapPin() {
		//return $this->Type."_pin.png";
	}
	
	
	public function getUCOpenImmoOrt() {
		if ($this->OpenImmoOrt) {
			return ucwords($this->OpenImmoOrt);
		}
		return;
	}


}


