<?php
/**
 * Kontaktpersonen für Immobilien
 * 
 */
class OpenImmoKontaktperson extends DataObject {
	
	private static $db = array(
		'Name' => 'Varchar(255)',
		'Vorname' => 'Varchar(255)',
		'Titel' => 'Varchar(255)',
		'Anrede' => 'Varchar(255)',
		'AnredeBrief' => 'Varchar(255)',
		'Firma' => 'Varchar(255)',
		'Zusatzfeld' => 'Varchar(255)',
		'Strasse' => 'Varchar(255)',
		'Hausnummer' => 'Varchar(255)',
		'Plz' => 'Varchar(255)',
		'Ort' => 'Varchar(255)',
		'Postfach' => 'Varchar(255)',
		'PostfPlz' => 'Varchar(255)',
		'PostfOrt' => 'Varchar(255)',
		'Land' => 'Varchar(255)',
		'EmailPrivat' => 'Varchar(255)',
		'EmailFeedback' => 'Varchar(255)',
		'TelPrivat' => 'Varchar(255)',
		'TelSonstige' => 'Varchar(255)',
		'Adressfreigabe' => 'Boolean',
		'Personennummer' => 'Int',
		'Url' => 'Varchar(255)',
		'Freitextfeld' => 'Text',
		'EmailZentrale' => 'Varchar(255)',
		'EmailDirekt' => 'Varchar(255)',
		'TelZentrale' => 'Varchar(255)',
		'TelDurchw' => 'Varchar(255)',
		'TelFax' => 'Varchar(255)',
		'TelHandy' => 'Varchar(255)',
	);
	
	private static $field_labels = array(
		'Vorname' => 'Vorname',
		'Name' => 'Nachname',
		'EmailPrivat' => 'Email Privat',
		'EmailFeedback' => 'Email Feedback',
		'TelPrivat' => 'Telefon Privat'
		
	   );
	private static $summary_fields = array(
		'Vorname',
		'Name',
		'EmailPrivat',
		'EmailFeedback',
		'TelPrivat'
		
	   );
	
	private static $belongs_many_many = array(
		'Immobilien' => 'OpenImmoImmobilie'
	);
	
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->removeByName('Zusatzfeld');
		$fields->removeByName('PostfPlz');
		$fields->removeByName('PostfOrt');
		$fields->removeByName('EmailFeedback');
		$fields->removeByName('TelSonstige');
		$fields->removeByName('Personennummer');
		$fields->removeByName('TelDurchw');
		$fields->removeByName('TelZentrale');
		$fields->removeByName('EmailZentrale');
		$fields->removeByName('EmailZentrale');
		$fields->removeByName('EmailDirekt');
		

		// Testing styling
		// $fields->insertAfter(
		// 	$fg = FieldGroup::create(
		// 		TextField::create('PostfPlz'),
		// 		TextField::create('PostfOrt')
		// 	),
		// 	'TelFax'
		// );
		// $fg->addExtraClass('Immobilie-Strasse-Hausnummer field');
		
		
		
		$fields->insertBefore(
			LiteralField::create('Holder', '<div class="KontaktPersonFields bs">'),
			'Name'
		);
		
		$fields->dataFieldByName('Name')->addExtraClass('col50');
		$fields->dataFieldByName('Vorname')->addExtraClass('col50');
		$fields->dataFieldByName('Titel')->addExtraClass('col50');
		$fields->dataFieldByName('Anrede')->addExtraClass('col50');
		$fields->dataFieldByName('AnredeBrief')->addExtraClass('col50');
		$fields->dataFieldByName('Firma')->addExtraClass('col100');
		$fields->dataFieldByName('Strasse')->addExtraClass('col50');
		$fields->dataFieldByName('Hausnummer')->addExtraClass('col50');
		$fields->dataFieldByName('Plz')->addExtraClass('col50');
		$fields->dataFieldByName('Ort')->addExtraClass('col50');
		$fields->dataFieldByName('Postfach')->addExtraClass('col50');
		$fields->dataFieldByName('Land')->addExtraClass('col50');
		$fields->dataFieldByName('TelPrivat')->addExtraClass('col50');

		$fields->dataFieldByName('EmailPrivat')->addExtraClass('col50');
		$fields->dataFieldByName('EmailPrivat')->setTitle('Email');

		$fields->dataFieldByName('Url')->addExtraClass('col50')->setTitle('Website');

		$fields->insertAfter(
			$fields->dataFieldByName('TelHandy')->addExtraClass('col50')->setTitle('Mobil'),
			'TelPrivat'
		);

		$fields->insertAfter(
			$fields->dataFieldByName('TelFax')->addExtraClass('col50')->setTitle('Fax'),
			'TelHandy'
		);
		
		$fields->insertAfter(
			LiteralField::create('HolderClose', '</div>'),
			'Freitextfield'
		);
		
		
		return $fields;
	}

}