<?php

/**
 * Immobilie ("ImmoDB Standard")
 * This is the Immobilie defined by us through the work
 * with our clients. It's based on the OpenImmo Standard, but extends it
 *
 * @package immodb
 * @subpackage model
 */
class ImmoDbImmobilie extends OpenImmoImmobilie {

	public static $singular_name = 'Immobilie';
	public static $plural_name = 'Immobilien';

	/******************************* FIELDS & RELATIONS *******************************/
	
	static $db = array(

		'ImmoDbUniqueIDLeft' => 'Varchar(11)',
		'ImmoDbUniqueIDRight' => 'Varchar(4)',
		'ImmoDbUniqueID' => 'Varchar(15)', //this is where left and right IDs are joined
		'ImmoDbVermarktungsart' => 'Enum("KAUF,MIETE_PACHT, FERIENVERMIETUNG","KAUF")',
		'ImmoDbStatus' => 'Enum("Online,Deaktiviert","Deaktiviert")',
		'ImmoDbSichtbarkeitPreis' => 'Enum("PreisAnzeigen,PreisAufAnfrage")',
		'ImmoDbNeubauErstbezug' => 'Boolean',
		'ImmoDbSanierungsobjekt' => 'Boolean',
		'ImmoDbAbschlag' => 'Decimal(19,4)',
		'ImmoDbAnzahlBadezimmerEnSuite' => 'Int',
		'ImmoDbFerienvermietungWochenpreis' => 'Decimal(19,4)',

		//Grundstück
		'ImmoDbGrundstueck' => 'Boolean',
		'ImmoDbGrundstueckTyp' => "Enum('Staedtisch,Laendlich,KEINE_ANGABE','KEINE_ANGABE')",
		'ImmoDbGrundstueckBebauungMoeglich' => 'Boolean',
		'ImmoDbGrundstueckStatusQuoBaugenehmigung' => "Enum('LiegtVor,Beantragt,KEINE_ANGABE','KEINE_ANGABE')",
		'ImmoDbGrundstueckBebaubareFlaeche' => 'Int',
		'ImmoDbGrundstueckEinfriedung' => 'MultiValueField',
		'ImmoDbGrundstueckStrom' => "MultiValueField",
		'ImmoDbGrundstueckWasser' => "MultiValueField",
		'ImmoDbGrundstueckAltbestandCaM2' => 'Int',
		'ImmoDbGrundstueckAltbestandTyp' => 'MultiValueField',
		'ImmoDbGrundstueckZufahrt' => "Enum('Unbefestigt,Befestigt,KEINE_ANGABE','KEINE_ANGABE')",
		
		//Grundstück-aids
		//We could consider appending "Aid" to the end of their names...
		'ImmoDbGrundstueckGrundstuecksflaeche' => 'Int',
		'ImmoDbGrundstueckPool' => 'Boolean',
		'ImmoDbGrundstueckPoolGroesse' => 'Varchar(255)',
		
		//Lage
		'ImmoDbStandort' => 'MultiValueField',
		'ImmoDbTopografie' => 'Enum("Flach,Huegelig,leichteHanglage,Hanglage","Flach")',
		//'ImmoDbAusblick' => 'MultiValueField',
		'ImmoDbErsteMeereslinie' => 'Boolean',
		'ImmoDbDirekterMeerzugang' => 'Boolean',

		//Immobilie
		'ImmoDbStrom' => 'MultiValueField',
		'ImmoDbWasser' => 'MultiValueField',
		'ImmoDbGemeinschaftkostenPaCa' => 'Decimal(19,4)',
		'ImmoDbRenoviert' => "Enum('Teilrenoviert,Vollrenoviert,KEINE_ANGABE','KEINE_ANGABE')",
		'ImmoDbRenoviertJahr' => 'Int',
		'ImmoDbSaniert' => "Enum('Teilsaniert,Vollsaniert,KEINE_ANGABE','KEINE_ANGABE')",
		'ImmoDbSaniertJahr' => 'Int',
		//Out of these three, we could potentially calculate "balkon_terrasse_flaeche" for the standard
		'ImmoDbTerrasseOffenCaM2' => 'Int',
		'ImmoDbTerrasseUeberdachtCaM2' => 'Int',
		'ImmoDbBalkonCaM2' => 'Int',

		//Ausstattung
		'ImmoDbHeizungVorbereitet' => 'Boolean',
		'ImmoDbKlimaVorbereitet' => 'Boolean',
		'ImmoDbKlimaTyp' => "Enum('WARM_KALT,KALT,KEINE_ANGABE','KEINE_ANGABE')",
		'ImmoDbPoolAussen' => "Enum('Moeglich,Privatpool,Gemeinschaftspool,KEINE_ANGABE','KEINE_ANGABE')",
		'ImmoDbPoolIndoor' => "Enum('Privatpool,Gemeinschaftspool,KEINE_ANGABE','KEINE_ANGABE')",
		'ImmoDbDoppelVerglasung' => 'Boolean',
		'ImmoDbEinfachVerglasung' => 'Boolean',
		'ImmoDbAutoGartenbewaesserung' => 'Boolean',
		'ImmoDbVollausgestatteteKueche' => 'Boolean',
		'ImmoDbTv' => 'MultiValueField',
		'ImmoDbTelefon' => 'Boolean',
		'ImmoDbInternet' => 'Boolean',
		'ImmoDbEnergiezertifikat' => "Enum('IN_BEARBEITUNG,A,B,C,D,E,F,G,H,KEINE_ANGABE','KEINE_ANGABE')",

		//Extras
		'ImmoDbGaestehaus' => 'Boolean',
		'ImmoDbGaestehausCaM2' => 'Int',
		'ImmoDbGaestehausZahlBz' => 'Int',
		'ImmoDbGaestehausZahlSz' => 'Int',
		'ImmoDbGaestehausKueche' => 'Boolean',
		'ImmoDbSeperatesGaesteapartment' => 'Boolean',
		'ImmoDbSeperatesGaesteapartmentCaM2' => 'Int',
		'ImmoDbSeperatesGaesteapartmentZahlBz' => 'Int',
		'ImmoDbSeperatesGaesteapartmentZahlSz' => 'Int',
		'ImmoDbSeperatesGaesteapartmentKueche' => 'Boolean',
		'ImmoDbSommerkueche' => 'Boolean',
		'ImmoDbUnterkellert' => 'Boolean',
		'ImmoDbFahrstuhl' => 'Boolean',
		'ImmoDbBBQ' => 'Boolean',
		'ImmoDbBodega' => 'Boolean',
		'ImmoDbFitnessraum' => 'Boolean',
		'ImmoDbDampfbad' => 'Boolean',
		'ImmoDbTennisplatz' => 'Boolean',

		//Distanzen
		'ImmoDbDistanzen' => 'MultiValueField',

	);
	
	static $has_one = array(
		//Each property has one OpenImmoObjektart
		//Behind the scenes the many_many relation to OpenImmoObjektart is used though,
		//see {OpenImmoImmobilie}
		'ImmoDbKategorie' => 'OpenImmoObjektart'
	);

	//this is obsolete - will go away at some point
	static $many_many = array (
		'ImmoDbLocation' => 'ImmoDbLocation',
	);


	public function getCMSValidator(){
		return new RequiredFields('OpenImmoOrt');
	}
	
	/******************************* GETTERS *******************************/

	/**
	 * Finds a location based on OpenImmoOrt
	 */
	public function getLocationFromOrtString() {
		$lTitle = $this->OpenImmoOrt;

		if (!$lTitle) return;

		//first attempt, trying to find the location based on the exact title
		$l = ImmoDbLocation::get()
			->filter('Title', $lTitle)
			->first();
		if ($l && $l->exists()) {
			return $l;
		} else {
			//second attempt, trying to find the location based on search strings
			$l = ImmoDbLocation::get()
				->filter('Search:PartialMatch', $lTitle)
				->first();
		}
		return $l;
	}
	
	
	/**
	 * Finds a region based on OpenImmoOrt
	 */
	public function getLocationRegion() {
		$l = $this->getLocationFromOrtString();
		if ($l && $l->exists()) {
			return $l->Parent();
		}
	}
	
	public function getPartnerAgencyID() {
		
		//the agency id is always taken from the owner of the immobilie
		$owner = $this->Owner();
		if ($owner) {
			return $owner->AgencyID;
		}
		
		
		//$id = null;
		//$member = Member::currentUser();
		//$idCalculated = null;
		//
		////id can only be calculated if a member is logged in
		////which will always be the case in admin
		//if ($member) {
		//	$idCalculated = $member->AgencyID;
		//}
		//
		//if ($this->ID) {
		//	//once written, get the agency id directly
		//	$id = $this->ImmoDbUniqueIDRight;
		//	
		//	//if it's nothing, re-calculate it
		//	if (!$id) {
		//		$id = $idCalculated;
		//	}
		//} else {
		//	//before first write
		//	$id = $idCalculated;
		//}
		//return $id;
	}
	
	public function generateImmoDbUniqueID() {
		return $this->ImmoDbUniqueIDLeft . "-" . $this->ImmoDbUniqueIDRight;
	}
	
	
	/**
	 * Translated Vermarktungsart
	 * @return string
	 */
	public function getImmoDbVermarktungsartNice() {
		if ($this->ImmoDbVermarktungsart) {
			return ImmoDbTranslationHelper::fieldlabel(
				$this->ClassName, 
				'ImmoDbVermarktungsart', 
				$this->ImmoDbVermarktungsart);
		} else {
			return '-';
		}
	}

	/**
	 * Getter for the price
	 */
	public function getImmoDbPriceNice(){
		$number = null;
		if ($this->ImmoDbVermarktungsart == 'KAUF') {
			$number = $this->OpenImmoKaufpreis;
		} elseif ($this->ImmoDbVermarktungsart == 'MIETE_PACHT') {
			$number = $this->OpenImmoNettokaltmiete;
		} else {
			$number = $this->ImmoDbFerienvermietungWochenpreis;
		}

		$amount = new Price();
		//Debug::dump($this->Kaufpreis);
		$amount->setAmount($number);
		//TODO this should not be hard coded but a setting
		$amount->setCurrency('EUR');
		$amount->setSymbol('EUR '); //I've added a space here for this to look nicer
		$amount->setPrecision(0);

		return $amount;	

	}

	/**
	 * Getter for ImmoDbGemeinschaftkostenPaCa
	 */
	public function getImmoDbGemeinschaftkostenPaCaNice(){
		$number = null;
		$number = $this->ImmoDbGemeinschaftkostenPaCa;
		$amount = new Price();
		//Debug::dump($this->Kaufpreis);
		$amount->setAmount($number);
		//TODO this should not be hard coded but a setting
		$amount->setCurrency('EUR');
		$amount->setSymbol('EUR '); //I've added a space here for this to look nicer
		$amount->setPrecision(0);
		
		return $amount;	

	}



	/**
	* Returns if the item is published or not
	*
	* @return string
	*/
	public function getCustomStatus(){
		return $this->isPublished()
			? _t('ImmoDbImmobilie.IsPublished','Online')
			: _t('ImmoDbImmobilie.IsUnpublished', 'Deaktiviert');
	}

	/**
	 * Helper function to determine if this object is  published or not
	 *
	 * @return bool
	 */
	public function isPublished(){

		if ($this->ImmoDbStatus == "Online") {
			return true;
		} else {
			return false;
		}
	}



	/**
	* Publishes an item
	*
	* @throws ValidationException
	*/
	public function doPublish()
	{
		if (!$this->canEdit()) {
			throw new ValidationException(_t('ImmoDbImmobilie.PublishPermissionFailure',
				'No permission to publish or unpublish this item'));
		}
		$this->ImmoDbStatus = 'Online';
		$this->write();
	}

	/**
	 * Unpublishes an  item
	 *
	 * @throws ValidationException
	 */
	public function doUnpublish()
	{
		if (!$this->canEdit()) {
			throw new ValidationException(_t('ImmoDbImmobilie.PublishPermissionFailure',
				'No permission to publish or unpublish this item'));
		}
		$this->ImmoDbStatus = 'Deaktiviert';
		$this->write();
	}


	/******************************* CRUD SETTINGS *******************************/

	public function canView($member = null) {
		if (
			//always return true if the following conditions are met:
			!$this->ID || //the id is not set
			$this->ImmoDbStatus == 'Online' //set status is set to online
			) {
			return true;
		} else {
			//else the following needs to be met
			if (!$member) {
				$member = Member::currentUser();
			}
			
			//either a member is logged in, and owns
			if ($member && ($this->OwnerID == $member->ID)) {
				return true;
			}
			
			//or a member is admin
			if ($member && Permission::check("immodb-admins")) {
				return true;
			}

			//either a member is logged in, and is calling the object with "stage"
			if (isset ($_GET['stage']) && $member) {
				return true;
			}
		
		}
		return false;
	}
	

	
	/******************************* BEFORE & AFTER WRITE *******************************/

	public function onBeforeWrite() {
		parent::onBeforeWrite();
		
		//Setting "Vermarktungart" based on what's been set in "ImmoDbVermarktungsart"
		if ($this->OpenImmoVermarktungsart()->exists()) {
			$this->OpenImmoVermarktungsart()->removeAll();
			$this->OpenImmoVermarktungsart()
				->add(
					OpenImmoVermarktungsart::get()
						->filter('Identifier', $this->ImmoDbVermarktungsart)
						->first()
				);
		}
		
		//Special case: Grundstück:
		//If the Kategorie = Grundstück, 
		//OpenImmoGrundstuecksflaeche should be set by ImmoDbGrundstueckGrundstuecksflaeche
		$cat = OpenImmoObjektart::get()
			->filter('Identifier', 'openimmo-grundstueck')
			->first();
		if ($cat && $cat->exists()) {
			if ($this->ImmoDbKategorieID == $cat->ID) {
				$this->OpenImmoGrundstuecksflaeche = $this->ImmoDbGrundstueckGrundstuecksflaeche;
			} 
		}
		
		
		//Setting/syncing partner agency ID
		$this->ImmoDbUniqueIDRight = $this->getPartnerAgencyID();
		//setting the ImmoDbUniqueID based on right and left ids
		$this->ImmoDbUniqueID = $this->generateImmoDbUniqueID();
	}

	protected function onAfterWrite() {
		parent::onAfterWrite();
		
		////Make sure that a parent locations is created for each
		////child location
		//$locations = $this->ImmoDbLocation();
		//foreach ($locations as $loc) {
		//	//Debug::dump($objektArt->Title);
		//	//Debug::dump($objektArt->ParentID);
		//	if ($loc->ParentID > 0) {
		//		//Debug::dump("Adding " . $objektArt->Parent()->Title . " to " . $this->Title);
		//		$this->ImmoDbLocation()->add($loc->Parent());
		//	}
		//}


		//Objektart #1
		//remove all objektart relations, and add only one - ImmoDbKategorieID
		$objektArten = $this->OpenImmoObjektart();
		foreach ($objektArten as $objektArt) {
			$this->OpenImmoObjektart()->remove($objektArt);
		}
		$objektArt = OpenImmoObjektart::get()->byID($this->ImmoDbKategorieID);
		if ($objektArt && $objektArt->exists()) {
			$this->OpenImmoObjektart()->add($objektArt);
		}
		
		
		
		//Objektart #2
		//Make sure that a parent objekt art is created for each
		//child objektart
		$objektArten = $this->OpenImmoObjektart();
		foreach ($objektArten as $objektArt) {
			//Debug::dump($objektArt->Title);
			//Debug::dump($objektArt->ParentID);
			if ($objektArt->ParentID > 0) {
				//Debug::dump("Adding " . $objektArt->Parent()->Title . " to " . $this->Title);
				$this->OpenImmoObjektart()->add($objektArt->Parent());
			}
		}
		
	}


	public function requireDefaultRecords() {
		parent::requireDefaultRecords();

		ImmoDbRelationHelper::requireRecords();
		//Demo Immobilien creation
		//This can also be triggered through own task
		//ImmoDbDemos::requireRecords();
	}


	/******************************* cached templates (testing) *******************************/
	
	public function ImmoItem($ContentLocale) {
		//Debug::dump($ContentLocale);

		$l = str_replace('-','',$ContentLocale);
		$id = $this->ID;
		$cachekey = "Immobilie{$id}ImmoItem_{$l}_" .
			strtotime($this->LastEdited);
		$cache = SS_Cache::factory($cachekey);
		//for testing
		//return $cachekey;

		
		if ($result = $cache->load($cachekey)) {
			$result = unserialize($result);
		} else {
			
			if ( $this->OpenImmoObjektart()->filter('Identifier', 'openimmo-grundstueck')->first() ) {
				$isGrundstueck = true;
			} else {
				$isGrundstueck = false;
			}
			
			$this->ContentLocale = $ContentLocale;
			$this->isGrundstueck = $isGrundstueck;

			$result = $this->renderWith('ImmoItem');
			
			$cache->save(serialize($result));
		}
		return $result;
		
	}
	
	

	/******************************* FIELDS & ADMIN *******************************/


	private static $summary_fields = array(
		'CalcThumbnail' => 'Bild',
		'Title' => 'Titel',
		'ImmoDbStatus' => 'Status',
		'getImmoDbVermarktungsartNice' => 'Vermarktungsart',
		'getOpenImmoObjektartList' => 'Objektart',
		'Owner.Name' => 'Eigentümer',
		'OpenImmoOrt' => 'Ort',
		'CustomStatus' => 'Status',
		'ImmoDbUniqueID' => 'Ref.-Nr.',
		//'Bundesland',
		//'Gemeindecode',
	);

	private static $searchable_fields = array(
		'Title' => array(
			'title' => 'Titel'
		),
		'ImmoDbStatus' => array(
			'title' => 'Status'
		),
		'ImmoDbVermarktungsart' => array(
			'title' => 'Vermarktungsart'
		),
		'OpenImmoObjektart.ID' => array(
			'title' => 'Objektart',
		),
		'Owner.ID' => array(
			'title' => 'Eigentümer'
		),
		'ImmoDbUniqueID' => array(
			'title' => 'Ref.-Nr.'
		)
		
	);


	public function summaryFields() {
		$fields = parent::summaryFields();
		//Debug::dump($fields);

		if (!Permission::check("immodb-admins")) {
			//display owner only for admins
			unset($fields['Owner.Name']);
		}

		//$fields['ObjektartNice'] = _t('Immobilie.db_Objektart');

		$fields['Title'] = _t('Immobilie.db_Title');
		$fields['ImmoDbUniqueID'] = 'Ref.-Nr.';
		$fields['getImmoDbVermarktungsartNice'] = $this->tFieldLabel('ImmoDbVermarktungsart');
		$fields['getOpenImmoObjektartList'] = $this->tFieldLabel('OpenImmoObjektart');
		$fields['ImmoDbStatus'] = $this->tFieldLabel('ImmoDbStatus');
		
		return $fields;
	}

	public function scaffoldSearchFields($params = array()){
		$fields = parent::scaffoldSearchFields();

		$request = Controller::curr()->getRequest();
		$query = $request->requestVar('q');
		
		//Debug::dump($query);
		
		//$objektArt = isset($query['OpenImmoObjektart']) ? $query['OpenImmoObjektart'] : array();

		//$fields->push(CheckboxSetField::create('OpenImmoObjektart',
		//		$this->tFieldLabel('OpenImmoObjektart'),
		//		$this->tFieldLabels('OpenImmoObjektart',
		//		$this->dbObject('OpenImmoObjektart')->enumValues())
		//	)->setValue($objektArt));

		
		//vermarktungsart checkboxes - working as of 31st march - disabled at the moment
		//$vermarktungsArt = isset($query['ImmoDbVermarktungsart']) ? $query['ImmoDbVermarktungsart'] : array();
		//$fields->push(CheckboxSetField::create('ImmoDbVermarktungsart',
		//		$this->tFieldLabel('ImmoDbVermarktungsart'),
		//		$this->tFieldLabels('ImmoDbVermarktungsart',
		//		$this->dbObject('ImmoDbVermarktungsart')->enumValues())
		//	)->setValue($vermarktungsArt));

		//instead - we use dropdown
		$vermarktungsArt = isset($query['ImmoDbVermarktungsart']) ? $query['ImmoDbVermarktungsart'] : array();
		$fields->push(DropdownField::create('ImmoDbVermarktungsart',
				$this->tFieldLabel('ImmoDbVermarktungsart'),
				$this->tFieldLabels('ImmoDbVermarktungsart',
				$this->dbObject('ImmoDbVermarktungsart')->enumValues())
			)->setValue($vermarktungsArt)
			->setEmptyString('Alle'));


		$objektArt = isset($query['OpenImmoObjektart']) ? $query['OpenImmoObjektart'] : array();
		//$fields->push(DropdownField::create('OpenImmoObjektart__ID',
		//	$this->tFieldLabel('OpenImmoObjektart'),
		//	$this->tFieldLabels('OpenImmoObjektart',
		//		OpenImmoObjektart::get()->map('ID', 'Title'))
		//		)->setValue($objektArt)
		//	->setEmptyString('Alle'));

		$fields->push(
		$objektartField = new SimpleTreeDropdownField(
			'OpenImmoObjektart__ID',
			$this->tFieldLabel('OpenImmoObjektart'),
			'OpenImmoObjektart',
			'ID',
			'Title'
		));
		$objektartField->setEmptyString('Alle');
		$objektartField->addExtraClass('dropdown');
		


		if (!Permission::check("immodb-admins")) {
			$fields->removeByName('Owner__ID');
		}

		//make sure the "Vermarktungsart" filter only shows enabled fields
		//$fields->dataFieldByName('OpenImmoVermarktungsart__ID')
		//	->setSource(OpenImmoVermarktungsart::get_enabled_fieldmap());

		//$fields->push(new Literalfield('test', 'test...'));

		return $fields;
	}
	
	public static function frontend_requirements(){
		Requirements::javascript('immodb/javascript/admin/ImmobilieForm.js');
		Requirements::javascript('immodb/javascript/admin.js');
		Requirements::css('immodb/css/admin.css');
	}
	
	
	public function getCMSFields() {
		self::frontend_requirements();

		$fields = parent::getCMSFields();
		$fields = ImmoDbCmsFields::get_cms_fields($fields, $this);
		$this->extend('updateCMSFields', $fields);

		return $fields;
	}


	/**
	 * Location tree search (backend)
	 *
	 * @return callback
	 */
	public function locationSearchCallback($sourceObject, $labelField, $search) {
		return DataObject::get('ImmoDbLocation', "\"Identifier\" LIKE '%$search%' OR \"Title\" LIKE '%$search%'");
	}


}