<?php

/**
 * A page type for filtered Immobilien listing
 * Main filters can be set for this page in the CMS, so this can act like a 
 * "vermietung" or "verkauf" url, but also a landing page for more specific filters
 * Finally this allows for translated urls as well
 */
class ImmobilienFilterPage extends ImmobilienHolder {
	
	public static $db = array(
		//'PriceRange' => 'Int',
		//'AnzahlSchlafzimmer' => 'Int',
		'Vermarktungsart' => 'Varchar(20)',
		'FincasUndLandhaeuser' => 'Boolean',
		'HaeuserUndVillen' => 'Boolean',
		'WohnungenUndApartments' => 'Boolean',
		'Grundstuecke' => 'Boolean',
		'Gewerbeimmobilien' => 'Boolean',
		'NeubauErstbezug' => 'Boolean',
		'Investmentobjekt' => 'Boolean',
		'NeuesteObjekte' => 'Boolean',
		'Unter250' => 'Boolean',
		'Meerblick' => 'Boolean',
		'LocationID' => 'Int',
		'Luxus' => 'Boolean'
	);
	
	
	public static $has_one = array(
		'ObjektArt' => 'OpenImmoObjektart'
	);



	public function getSettingsFields() {
		$fields = parent::getSettingsFields();
		
		
		$fields->addFieldToTab('Root.Settings', new HeaderField('ImmobilienFilterHeading', 'Filter Options'));
		$fields->addFieldToTab('Root.Settings', new ImmoDbObjektartDropdownField('ObjektArtID'));
		$fields->addFieldToTab('Root.Settings', new ImmoDbVermarktungsartOptionsetField('Vermarktungsart'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('FincasUndLandhaeuser'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('HaeuserUndVillen'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('WohnungenUndApartments'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('Grundstuecke'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('Gewerbeimmobilien'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('NeubauErstbezug'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('Investmentobjekt'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('NeuesteObjekte'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('Unter250'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('Meerblick'));
		$fields->addFieldToTab('Root.Settings', new CheckboxField('Luxus'));
		
		$fields->addFieldToTab('Root.Settings', 
	
		$formField = new DropdownField(
			'LocationID',
			'Location',
			ImmoDbLocation::get()
			//ImmoDbRadiusSearchHelper::available_locations()
				//->filter('enabled',1)
				->sort('Title')
				//->map('ID', 'TitleTranslated')
				->map('ID', 'TitleTranslated')
			)

		);
		
		$formField->setEmptyString(_t('SearchBox.PLACE', 'Place'));
		// $value = $this->getLocationFilter();
		//
		// if ($value) {
		// 	$formField->setValue($value);
		//}
		
			
		if ($this->Parent()->ID != 0 ) {
			$field = new DropdownField('MegaMenuCategoryID', 'Mega Menu Kategorie', $this->Parent()->MegaMenuCategories()->map('ID', 'Title'));
			$field->setEmptyString(' -- Bitte wählen -- ');
			$fields->addFieldToTab('Root.Settings', $field);
		}



		
		return $fields;
	}
	
}

class ImmobilienFilterPage_Controller extends ImmobilienHolder_Controller {


	/**
	 * There should only be one Immobilien Holder
	 * per site
	 */
	public function getImmobilienHolder(){
		return ImmobilienHolder::get()->first();
	}


	/**
	 * Objektart Filter
	 * o
	 *
	 * @return int|null
	 */
	public function getObjektartFilter(){
		$o = $this->ObjektArtID;
		if ($o) {
			return $o;
		} else {
			return parent::getObjektartFilter();
		}

		//$o = parent::getObjektartFilter();
		//$o2 = $this->ObjektArtID;
		//
		//if (!$o) {
		//	if ($o2) {
		//		return $o2;
		//	}
		//} else {
		//	if ($o == $o2) {
		//		return $o2;
		//	} else {
		//		//$this->redirect($this->getImmobilienHolder()->Link());
		//		$this->redirect('/immobilien-mallorca/');
		//	}
		//}
	}

	/**
	 * Objektart Dropdown
	 * o
	 *
	 * @return DropdownField
	 */
	public function ObjektartField(){
		$field = parent::ObjektartField();
		if ($this->ObjektArtID) {
			$field = $field->performReadonlyTransformation();
		}
		return $field;
	}
	

	/**
	 * Vermarktungsart Filter
	 * v
	 *
	 * @return string|null
	 */
	public function getVermarktungsartFilter(){
		$v = $this->Vermarktungsart;
		if ($v) {
			return $v;
		} else {
			return parent::getVermarktungsartFilter();
		}
	}
	
	/**
	 * Vermarktungsart RadioButtons
	 * v
	 *
	 * @return OptionsetField
	 */
	public function VermarkungsartField(){
		$field = parent::VermarkungsartField();
		if ($this->Vermarktungsart) {
			$field = $field->performReadonlyTransformation();
		}
		return $field;
	}
	
	public function getFincasUndLandhaeuserFilter(){
		
		$fl = $this->FincasUndLandhaeuser;
		if ($fl) {
			return $fl;
		} else {
			return parent::getFincasUndLandhaeuserFilter();
		}
	}

	public function getHaeuserUndVillenFilter(){
		
		$hv = $this->HaeuserUndVillen;
		
		if ($hv) {
			return $hv;
		} else {
			return parent::getHaeuserUndVillenFilter();
		}
	}
	
	public function getWohnungenUndApartmentsFilter(){
		
		$wa = $this->WohnungenUndApartments;
		
		if ($wa) {
			return $wa;
		} else {
			return parent::getWohnungenUndApartmentsFilter();
		}
	}

	public function getLocationFilter(){
		
		$l = $this->LocationID;
		
		if ($l) {
			return $l;
		} else {
			return parent::getLocationFilter();
		}
	}
	
	public function getGrundstueckeFilter(){
		
		$g = $this->Grundstuecke;
		
		if ($g) {
			return $g;
		} else {
			return parent::getGrundstueckeFilter();
		}
	}

	public function getGewerbeimmobilienFilter(){
		
		$gi = $this->Gewerbeimmobilien;
		
		if ($gi) {
			return $gi;
		} else {
			return parent::getGewerbeimmobilienFilter();
		}
	}
	
	public function getNeubauErstbezugFilter(){
		
		$ne = $this->NeubauErstbezug;
		
		if ($ne) {
			return $ne;
		} else {
			return parent::getNeubauErstbezugFilter();
		}
	}

	public function getInvestmentobjektFilter(){
		
		$io = $this->Investmentobjekt;
		
		if ($io) {
			return $io;
		} else {
			return parent::getInvestmentobjektFilter();
		}
	}

	public function getNeuesteObjektFilter(){
		
		$no = $this->NeuesteObjekte;
		
		if ($no) {
			return $no;
		} else {
			return parent::getNeuesteObjektFilter();
		}
	}
	
	public function getUnter250Filter(){
		
		$u250 = $this->Unter250;
		
		if ($u250) {
			return $u250;
		} else {
			return parent::getUnter250Filter();
		}
	}
	
	public function getMeerblickFilter(){
		
		$mb = $this->Meerblick;
		
		if ($mb) {
			return $mb;
		} else {
			return parent::getMeerblickFilter();
		}
	}

	public function getLuxusFilter(){
		
		$lx = $this->Luxus;
		
		if ($lx) {
			return $lx;
		} else {
			return parent::getLuxusFilter();
		}
	}
	
	public function FilteredItems() {
		return $this->Items();
	}
}