<?php

/**
 * Holder for Immobilien
 * This should be the base page where all Immobilien are available (and filterable)
 * 
 * Custom url filters can be set up with {@see ImmobilienFilterPage}
 *
 */
class ImmobilienHolder extends DataObjectAsPageHolder {


}
class ImmobilienHolder_Controller extends DataObjectAsPageHolder_Controller {


	private static $allowed_actions = array(
		'show',
		'rendermodalform',
		'renderRecommendatioModalform',
		'ModalForm',
		'RecommendationForm',
		'itemsCount' => true
	);



	//Variables for filtering - set in init method
	public $filters = array();
	private $filterPath = null;
	
	//Range definitions
	//Hard conded ATM. If ever the need for customization arises, this should
	//happen over config.yml, an then they might need to be moved to ImmobilienHolder
	//intead of ImmobilienHolder_Controller
	private $schlafzimmerRanges = array(1,2,3,4,5);

    private $badezimmerRanges = array(1,2,3,4,5);

    private $locationRadiusRanges = array(1,5,10,15,30,50,100);
	private $priceRanges = array(
		1 => array(
			'min' => '0',
			'max' => '300.000'
		),
		2 => array(
			'min' => '300.000',
			'max' => '500.000'
		),
		3 => array(
			'min' => '500.000',
			'max' => '800.000'
		),
		4 => array(
			'min' => '800.000',
			'max' => '1.000.000'
		),
		5 => array(
			'min' => '1.000.000',
			'max' => '3.000.000'
		),
		6 => array(
			'min' => '3.000.000',
			'max' => '4.999.000'
		),
		7 => array(
			'min' => '5.000.000',
			'max' => null
		),
	);

	private $priceSortParameters = array('ASC','DESC','SZ_ASC','SZ_DESC', 'CREATED_DESC');


	public function init(){
		parent::init();
		
		
		//Set filters based on the query string
		$url = $_SERVER['REQUEST_URI'];
		$url_parsed = parse_url($url);

		// Manipulating Search Form Action for Search starting at Detail Page
		$params = $this->getURLParams();
		$action = $params['Action'];
		// Manipulating BreadCrumbs
		
		if (Session::get('FilterPageItems')) {
			$SessionParams = Session::get('FilterPageItems')->request->params();
			
			// EXPOSEE!
		
			if ($action !="page"  &&  !isset($params['ID']) && $SessionParams['ID']!="" &&  $params['Action'] !="" ) {
			
				$BackLink = $_SERVER['HTTP_REFERER'];
			
				$this->AddBreadcrumbAfter( 
					BreadcrumbNavigation::CreateBreadcrumb(
						_t('BREADCRUMB.Seite', 'Seite ').$SessionParams['ID'], 
						$BackLink, 
						false
					) 
				);
			
			} else {
				// LISTING
				$Params = $this->request->params();
				$BackLink = $_SERVER['HTTP_REFERER'];
				$SessionParams = Session::get('FilterPageItems')->request->params();
				$url = $_SERVER['HTTP_REFERER'];
				$data = parse_url($url);
				$referer = $data['query'];

				//Wir kommen von einer Suchanfrage
				if ( isset($referer) ) {
					
					$this->AddBreadcrumbAfter(
						BreadcrumbNavigation::CreateBreadcrumb(
							'Suchergebnisse',
							$BackLink,
							true
						)
					);
				}
				
				// Pagination
				if ( $Params['ID'] !="" ) {
					
					$this->AddBreadcrumbAfter( 
						BreadcrumbNavigation::CreateBreadcrumb(
							_t('BREADCRUMB.Seite', 'Seite ').$Params['ID'], 
							$BackLink, 
							true
						) 
					);

				}	
			
			}
		}
		

		


		$Loc = ImmoDbLocation::get()->byID( $_GET["l"] );
		$Location = $Loc->TitleTranslated;
		if ($Loc) {
			
			// Check if Location set.
			if (isset( $_GET["l"]) && $_GET["lr"] > 1 ) {
				// We got a location -> get Realname
				if ($Location) {
					
					$this->Title = _t('ImmobilienHolder.TITLE', 'Immobilien in <span>{Location}</span> und Umgebung', array('Location' => $Location));
				}
			} else {
				$this->Title = _t('ImmobilienHolder.IMMOBILIEN_IN', 'Immobilien in <span>{Location}</span>', array('Location' => $Location));
				
			}
		} else {
			
		}
		
		
		if ( $this->Parent()->ClassName == "MigImmobilienHolder" || $this->Parent()->ClassName == "E5ImmobilienHolder") {
			$this->filterPath = i18n::get_lang_from_locale(Controller::curr()->Locale).'/'.$this->Parent()->URLSegment;
			
		}
		else if ($action || $action == "show") {
			$this->filterPath = i18n::get_lang_from_locale(Controller::curr()->Locale).'/'.Controller::curr()->URLSegment.'/?'.$this->query;
		}
		
		else {
			//$this->filterPath = $url_parsed['path'];
			$Root = MigImmobilienHolder::get()->filter('isRoot', 1)->First();
			$RootURLSegment = $Root->URLSegment;
			$this->filterPath = i18n::get_lang_from_locale(Controller::curr()->Locale).'/'.$RootURLSegment.'/';
		
		}
		
		
		if (isset($url_parsed['query'])) {
			$query = $url_parsed['query'];
			
			if (isset($_GET["re"]) && $_GET["re"] == "reset") {
				$query = "re=reset";
			}
			
			parse_str($query, $this->filters);
			$this->query = $query;

		}
		

	}
	
	
	public function index() {
		
		if ($this->request->isAjax()) {
			return $this->renderWith('ImmobilienHolderList');
		} else {
			return $this;
		}
	}
	
	
	public function URLParsed() {
		$url = $_SERVER['REQUEST_URI'];
		return $url;
	}
		

	public function itemsCount(SS_HTTPRequest $request) {
		$response = new SS_HTTPResponse(Convert::raw2json(array('count' => $this->Items()->count() )));
		$response->addHeader('Content-Type', 'application/json');
		return $response;
	}
	
	public function ModalForm($ItemID = null){
		$form = new EnquiryForm($this, 'ModalForm', $ItemID);
		return $form;
	}

	public function RecommendationForm($ItemID = null){
		$form = new RecommendationForm($this, 'RecommendationForm', $ItemID);
		return $form;
	}
	

	public function renderRecommendatioModalform(){
		$vars = $this->request->getVars();
		$form = $this->RecommendationForm($vars['ItemID']);
		return $form->forTemplate()->RAW();
	}	
	
	public function rendermodalform(){
		$vars = $this->request->getVars();
		$form = $this->ModalForm($vars['ItemID']);
		return $form->forTemplate()->RAW();
	}
	

	
	
	public function show() {
		
		if( $item = $this->getCurrentItem() )
		{
			if ($item->canView())
			{
				
				$url = $_SERVER['HTTP_REFERER'];
				$data = parse_url($url);
				$referer = $data['query'];
				
				//Wir kommen von einer Suchanfrage
				if ( isset($referer) ) {
					// For Breadcrumb
					$this->BackLinkText = _t('ImmobilienHolderShow.BackLinkText', 'Zurück zu den Suchergebnissen');
					$this->isSearchResult = true;
					$BackLink = $_SERVER['HTTP_REFERER'];
					$this->BackLink = $BackLink;
					
					//$this->AddBreadcrumbBefore(BreadcrumbNavigation::CreateBreadcrumb('Suchergebnisse', $BackLink, false) );
					
				}

				
				if ( $item->OpenImmoObjektart()->filter('Identifier', 'openimmo-grundstueck')->first() ) {
					$isGrundstueck = true;
				} else {
					$isGrundstueck = false;
				}
				
				$item->isSelf = true;
				$this->AddBreadcrumbAfter($item);
				
				$data = array(
					'Item' => $item,
					'MetaTags' => $item->MetaTags(),
					'IsDetailView' => true,
					'isGrundstueck' => $isGrundstueck
				);
				
				
				
				if (isset($this->query)) {
					// Needed to build action in search form
					$data['query'] = $this->query;
				}
				
				$this->BackLink = $_SERVER['HTTP_REFERER'];
				
			
				
				
				return $this->customise(new ArrayData($data))->renderWith(array('ImmobilienHolder_show','Page'));
				
				
			}
			else
			{
				return Security::permissionFailure($this);
			}
		}
		else
		{
			return $this->httpError(404);
		}
	}


	/**
	 * Spec Row
	 * TODO for translations it makes more sense that everything
	 * is taken directly from the field, and thus we won't need $Title here
	 * 
	 */
	public function SpecRow($Field, $Title, $DataType = null) {
		
		
		$true_false = "false";
		$item = $this->getCurrentItem();
		$type = $item->obj($Field)->class;
		$ret = "";
		$after = "";
		$before = "";
		
		if ($DataType == 'sqm') {
			$after = 'm<sup>2</sup>';
			$before = _t('ImmobilienHolder.SQM_APPR','appr.') . " ";
		}
		
		if ($DataType == 'ca') {
			$after = '';
			$before = _t('ImmobilienHolder.SQM_APPR','appr.') . " ";
		}
		

			
		if ($Field == 'OpenImmoEtage') {
			if ( $item->obj($Field)->getValue() == 0) {
				// OpenImmoEtage is an Integer we need a TextObject here
				$item->$Field = new Text();
				$after = " ". _t('ImmobilienHolder.EG','EG') . " ";
				
			} else{
				$after = " ". _t('ImmobilienHolder.OG','OG') . " ";
			}

			$before = '';
		}
		
		
		
		if ($item->$Field) {
			
			
			
			//Debug::dump($type);
			
			// NO HEIZUNG!
			if ( $Field == "OpenImmoHeizungsart" && count($item->obj($Field)->value) == 0 ) {
				return "
					<tr>
						<td>". _t('ImmobilienHolder.HEIZUNG','Heizung') . "</td>
						<td class='".$true_false."'>". _t('ImmobilienHolder.NO','Nein') ."</td>
					</tr>
					";
			}
			
			// We deal with a MultiValuefield
			if ( $type == "MultiValueField" && $DataType != 'isSet') {
				
				// make sure its not an empty multivalue field
				//if ( $item->$Field->value ) {
				if ( count($item->obj($Field)->value) > 0 ) {
					$arrRet = ImmoDbTranslationHelper::fieldlabels('Immobilie', $Field, $item->$Field->value);
					$ret = implode($arrRet, ', ');
				} else {
					return false;
					
				}
				
				$true_false = "false";

				
			} else if ($type == "Boolean") {
				$true_false = ($item->$Field == 1) ? 'true' : 'false'; 
			
			} else if ($type == 'Enum') {

				$ret = ImmoDbTranslationHelper::fieldlabel('Immobilie', $Field, $item->$Field->value);
				
				if( $item->obj($Field)->value == "KEINE_ANGABE" ) {
					return false;
				} else {
					$ret = ImmoDbTranslationHelper::fieldlabel('Immobilie', $Field, $item->$Field);
				}
				
			} else if ($type == "MultiValueField" && $DataType == 'isSet') {
				// Show just a hook, even if it is a mult value field. client spec!
				if ( count($item->obj($Field)->value) > 0 ) {
					// At leas one value
					$true_false = 'true';
					$ret="";
				} else {
					return false;
				}
				
			}
			else {
				$ret = $item->$Field;
				$true_false = 'false'; 
			}
			
		
			
			
			if (
				$ret!="KEINE_ANGABE" && 
				$ret!="KEINE_angabe" &&
				$ret!="Keine Angabe" &&
				$ret!="Keine  Angabe"
			) {
				
				//we're overwriting title  for translations
				//TODO: remove title completely
				$Title = ImmoDbTranslationHelper::fieldlabel('Immobilie', $Field);
				
				
				return "
					<tr>
						<td>".$Title."</td>
						<td class='".$true_false."'>".$before.ucwords(strtolower($ret)).$after."</td>
					</tr>
					";
			} else {
				//return $Title.' ist nicht gesetzt<br />';
				return false;
				
			}
				
				
				
				
		} else {
			// return $Title.' ist nicht gesetzt<br />';
			return false;
			

		}
		
	}
    
    


	/******************************* FILTERS *******************************/
	/**
	 * Objektart Filter
	 * o
	 * 
	 * @return int|null
	 */
	public function getObjektartFilter(){
		$filters = $this->filters;
		if (isset($filters['o'])) {
			return (int) $filters['o'];
		}
	}

	/**
	 * Vermarktungsart Filter
	 * v
	 * 
	 * @return string|null
	 */
	public function getVermarktungsartFilter(){
		$filters = $this->filters;
		if (isset($filters['v'])) {
			return $filters['v'];
		}
	}
	
	/**
	 * Price Filter
	 * p
	 * 
	 * @return int|null
	 */
	public function getPriceFilter(){
		$filters = $this->filters;
		if (isset($filters['p'])) {
			return (int) $filters['p'];
		}
	}
	
	
	/**
	 * Location Filter
	 * l
	 * 
	 * @return int|null
	 */
	public function getLocationFilter(){
		$filters = $this->filters;
		if (isset($filters['l'])) {
			return (int) $filters['l'];
		}
	}
	
	
	/**
	 * Location Filter
	 * u
	 * 
	 * @return string|null
	 */
	public function getUniqueIDFilter(){
		$filters = $this->filters;
		if (isset($filters['u'])) {
			return $filters['u'];
		}
	}
	
	
	/**
	 * Location Filter
	 * u
	 * 
	 * @return string|null
	 */
	public function getRefFilter(){
		$filters = $this->filters;
		if (isset($filters['ref'])) {
			return $filters['ref'];
		}
	}
	
	
	
	
	/**
	 * Location Range Filter
	 * l
	 * 
	 * @return int
	 */
	public function getLocationRangeFilter(){
		$filters = $this->filters;
		if (isset($filters['lr'])) {
			return (int) $filters['lr'];
		} else {
			return $this->locationRadiusRanges[0];
		}
	}
	
	/**
	 * Schlafzimmer Filter
	 * s
	 * 
	 * @return int|null
	 */
	public function getSchlafzimmerFilter(){
		$filters = $this->filters;
		if (isset($filters['s'])) {
			return (int) $filters['s'];
		}
	}

	/**
	 * Badezimmer Filter
	 * b
	 * 
	 * @return int|null
	 */
	public function getBadezimmerFilter(){
		$filters = $this->filters;
		if (isset($filters['b'])) {
			return (int) $filters['b'];
		}
	}



	/**
	 * Fincas und Landhäuser Filter
	 * s
	 * 
	 * @return int|null
	 */
	public function getFincasUndLandhaeuserFilter(){
		$filters = $this->filters;
		if (isset($filters['fl'])) {
			return (int) $filters['fl'];
		}
	}
	

	/**
	 * Häuser und Villen Filter
	 * hv
	 * 
	 * @return int|null
	 */
	public function getHaeuserUndVillenFilter(){
		$filters = $this->filters;
		if (isset($filters['hv'])) {
			return (int) $filters['hv'];
		}
	}


	/**
	 * Wohnungen Und Apartments Filter
	 * wa
	 * 
	 * @return int|null
	 */
	public function getWohnungenUndApartmentsFilter(){
		$filters = $this->filters;
		if (isset($filters['wa'])) {
			return (int) $filters['wa'];
		}
	}


	/**
	 * Grundstuecke Filter
	 * g
	 * 
	 * @return int|null
	 */
	public function getGrundstueckeFilter(){
		$filters = $this->filters;
		if (isset($filters['g'])) {
			return (int) $filters['g'];
		}
	}

	/**
	 * Gewerbeimmobilien Filter
	 * gi
	 * 
	 * @return int|null
	 */
	public function getGewerbeimmobilienFilter(){
		$filters = $this->filters;
		if (isset($filters['gi'])) {
			return (int) $filters['gi'];
		}
	}


	/**
	 * NeubauErstbezug Filter
	 * ne
	 * 
	 * @return int|null
	 */
	public function getNeubauErstbezugFilter(){
		$filters = $this->filters;
		if (isset($filters['ne'])) {
			return (int) $filters['ne'];
		}
	}


	/**
	 * Investmentobjekt Filter
	 * ne
	 * 
	 * @return int|null
	 */
	public function getInvestmentobjektFilter(){
		$filters = $this->filters;
		if (isset($filters['io'])) {
			return (int) $filters['io'];
		}
	}
	
	
	/**
	 * NeuesteObjekte Filter
	 * no
	 * 
	 * @return int|null
	 */
	public function getNeuesteObjekteFilter(){
		
		$filters = $this->filters;
		if (isset($filters['no'])) {
			return (int) $filters['no'];
		}
	}
	
	
	/**
	 * NeuesteObjekte Filter
	 * no
	 * 
	 * @return int|null
	 */
	public function getUnter250Filter(){
		$filters = $this->filters;
		
		if (isset($filters['u250'])) {
			return (int) $filters['u250'];
		}
	}
	

	public function getMeerblickFilter(){
		$filters = $this->filters;
		
		if (isset($filters['mb'])) {
			return (int) $filters['mb'];
		}
	}
	
	public function getLuxusFilter(){
		$filters = $this->filters;
		
		if (isset($filters['lx'])) {
			return (int) $filters['lx'];
		}
	}
	

	/**
	 * Reset Filter
	 * re
	 * 
	 * @return string|null
	 */
	public function getResetFilter(){
		$filters = $this->filters;
		if (isset($filters['re'])) {
			return $filters['re'];
		}
	}
	

	public function getSortDirectionFilter(){
		$filters = $this->filters;
		if (isset($filters['sdf'])) {
			return $filters['sdf'];
		}
	}

	public function getSortPropertyFilter(){
		$filters = $this->filters;
		if (isset($filters['spf'])) {
			return $filters['spf'];
		}
	}


	/**
	 * Getter for the search query - if it exists
	 * NOT YET IMPLEMENTED
	 * 
	 * @return string
	 */
	public function SearchQuery(){
		if (isset($_GET["q"])) {
			return trim($_GET["q"]);
		}
	}

	/**
	 * Filters string
	 * for URLs
	 * @return string|null
	 */
	public function FiltersString(){
		$filters = $this->filters;
		if ($filters) {
			return '?' . http_build_query($filters);
		}
	}

	/**
	 * Filter Path
	 */
	public function getFilterPath(){
		return $this->filterPath;
	}


	
	private $increasedRadius = null;
	/**
	 * Template getter for radius auto increment
	*/
	public function IncreasedRadius() {
		//start by calculating the items - we can't find the radius
		//before items have been calculated
		return $this->increasedRadius;
	}

	/******************************* FORM FIELDS *******************************/

	/**
	 * Vermarktungsart RadioButtons
	 * v
	 * 
	 * @return OptionsetField
	 */
	// public function VermarkungsartField(){
	// 	$formField = new MigVermarktungsartDropdownField('v');
	// 
	// 	$value = $this->getVermarktungsartFilter();
	// 	if ($value) {
	// 		$formField->setValue($value);
	// 		
	// 	}
	// 	$formField->addExtraClass('form-control');
	// 	return $formField;
	// }
	
	/**
	 * Objektart Dropdown
	 * o
	 * 
	 * @return DropdownField
	 */
	public function ObjektartField(){
		$formField = new ImmoDbObjektartDropdownField('o');
		
		$value = $this->getObjektartFilter();
		if ($value) {
			$formField->setValue($value);
		}
		
		$formField->setEmptyString(_t('SearchBox.MIG_PROPERTY_TYPE', 'Property type'));
		$formField->addExtraClass('form-control');
		
		return $formField;
	}

	/**
	 * Schlafzimmer Dropdown
	 * s
	 * 
	 * @return DropdownField
	 */
	public function SchlafzimmerField(){
		//OpenImmoAnzahlSchlafzimmer
		
		$ranges = $this->schlafzimmerRanges;
		
		$rangeLabels = array();
		foreach ($ranges as $range) {
			$rangeLabels[$range] = _t(
				'ImmobilienHolder.SchlafzimmerFieldRange' . $range,
				"$range"
			);
		}

		$formField = new DropdownField(
			's',
			'Anzahl Schlafzimmer',
			$rangeLabels
		);
		$formField->setEmptyString(_t('SearchBox.MIG_BEDROOMS', 'Bedrooms'))
			->addExtraClass('form-control');

		$value = $this->getSchlafzimmerFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	/**
	 * Badezimmer Dropdown
	 * b
	 * 
	 * @return DropdownField
	 */
	public function BadezimmerField(){
		
		$ranges = $this->badezimmerRanges;
		
		$rangeLabels = array();
		foreach ($ranges as $range) {
			$rangeLabels[$range] = _t(
				'ImmobilienHolder.BadezimmerFieldRange' . $range,
				"$range"
			);
		}

		$formField = new DropdownField(
			'b',
			'Anzahl Badezimmer',
			$rangeLabels
		);
		$formField->setEmptyString(_t('SearchBox.MIG_BATHROOMS', 'Bathrooms'))
			->addExtraClass('form-control');

		$value = $this->getBadezimmerFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}
	
	
	
	
	/**
	 * Price Dropdown
	 * p
	 * 
	 * @return DropdownField
	 */
	public function PriceRangeField(){

    $ranges = $this->priceRanges;

    foreach ($ranges as $range => $minmax) {
        $rangeLabels[$range] = _t(
            'ImmobilienHolder.PriceRangeFieldRange' . $range,
            $minmax['min'] . " - " . $minmax['max']
        );
    }
        $formField = new DropdownField(
            'p',
            'Preis',
            $rangeLabels
        );

        $formField->setEmptyString(_t('SearchBox.MIG_PRICE', 'Price'))
			->addExtraClass('form-control');

            $rangeLabels = array();

		    $value = $this->getPriceFilter();
		    if ($value) {
			    $formField->setValue($value);
		    }

		return $formField;
	}


	/**
	 * Location Dropdown
	 * l
	 * 
	 * @return DropdownField
	 */
	public function LocationField(){
		//$formField = new SimpleTreeDropdownField(
		//	'l',
		//	'Location',
		//	'ImmoDbLocation',
		//	'ID',
		//	'TitleTranslated'
		//);
		//$formField->setFilter('Enabled=1');
		//return false;


		$formField = new DropdownField(
			'l',
			'Location',
			//ImmoDbLocation::get()
			//ImmoDbRadiusSearchHelper::available_locations()
			ImmoDbRadiusSearchHelper::available_locations_alt()
				//->filter('enabled',1)
				//->sort('Title')
				->map('ID', 'TitleTranslated')
		);

		$formField->setEmptyString(_t('SearchBox.MIG_PLACE', 'Place'));

		$value = $this->getLocationFilter();
		
		
		if ($value) {
			$formField->setValue($value);
		}
		
		return $formField;
	}

	/**
	 * UniqueID Field
	 * u
	 *
	 * @return TextField
	 */
	public function UniqueIDField(){

		$formField = new TextField(
			'u',
			'UniqueID'
		);

		$formField->setAttribute('placeholder',_t('SearchBox.REF_NO_SHORT'));

		$value = $this->getUniqueIDFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}
	
	
	/**
	 * Ref Field
	 * ref
	 *
	 * @return TextField
	 */
	public function RefField(){

		$formField = new TextField(
			'ref',
			'Ref_'
		);

		$formField->setAttribute('placeholder',_t('SearchBox.REF_NO_SHORT'));

		$value = $this->getRefFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}
	
	
	

	/**
	 * Location Range Dropdown
	 * lr
	 *
	 * @return DropdownField
	 */
	public function LocationRangeField(){

		$ranges = $this->locationRadiusRanges;

		$rangeLabels = array();
		foreach ($ranges as $range) {
			$rangeLabels[$range] = _t(
				"$range km",
				"$range"
			);
		}

		$formField = new DropdownField(
			'lr',
			'Umkreis',
			$rangeLabels
		);

		$value = $this->getLocationRangeFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}
	
	
	
	/**
	 * FincasUndLandhaeuserField Checkbox
	 * fl
	 * 
	 * @return CheckboxField
	 */
	public function FincasUndLandhaeuserField(){
		$formField = new CheckboxField('fl');

		$value = $this->getFincasUndLandhaeuserFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	/**
	 * HaeuserUndVillenField Checkbox
	 * hv
	 * 
	 * @return CheckboxField
	 */
	public function HaeuserUndVillenField(){
		$formField = new CheckboxField('hv');

		$value = $this->getHaeuserUndVillenFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	/**
	 * WohnungenUndApartments Checkbox
	 * hv
	 * 
	 * @return CheckboxField
	 */
	public function WohnungenUndApartmentsField(){
		$formField = new CheckboxField('wa');

		$value = $this->getWohnungenUndApartmentsFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	/**
	 * Grundstuecke Checkbox
	 * g
	 * 
	 * @return CheckboxField
	 */
	public function GrundstueckeField(){
		$formField = new CheckboxField('g');

		$value = $this->getGrundstueckeFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	/**
	 * Gewerbeimmobilien Checkbox
	 * gi
	 * 
	 * @return CheckboxField
	 */
	public function GewerbeimmobilienField(){
		$formField = new CheckboxField('gi');

		$value = $this->getGewerbeimmobilienFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	/**
	 * NeubauErstbezugField Checkbox
	 * ne
	 * 
	 * @return CheckboxField
	 */
	public function NeubauErstbezugField(){
		$formField = new CheckboxField('ne');

		$value = $this->getNeubauErstbezugFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}
	
	
	/**
	 * NeubauErstbezugField Checkbox
	 * ne
	 * 
	 * @return CheckboxField
	 */
	public function InvestmentobjektField(){
		$formField = new CheckboxField('io');

		$value = $this->getInvestmentobjektFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	/**
	 * NeuesteObjekteField Checkbox
	 * ne
	 * 
	 * @return CheckboxField
	 */
	public function NeuesteObjekteField(){
		$formField = new CheckboxField('no');

		$value = $this->getNeuesteObjekteFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}

	/**
	 * Unter250Field Checkbox
	 * ne
	 * 
	 * @return CheckboxField
	 */
	public function Unter250Field(){
		$formField = new CheckboxField('u250');

		$value = $this->getUnter250Filter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	public function MeerblickField(){
		$formField = new CheckboxField('mb');

		$value = $this->getMeerblickFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	public function LuxusField(){
		$formField = new CheckboxField('lx');

		$value = $this->getLuxusFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}


	/**
	 * PriceSort Dropdown
	 * s
	 * 
	 * @return DropdownField
	 */
	public function PriceSortField(){

		$ranges = $this->priceSortParameters;
		
		$rangeLabels = array();
		foreach ($ranges as $range) {
			$rangeLabels[$range] = _t(
				'ImmobilienHolder.PriceSortParameters' . $range,
				"$range"
			);
		}

		$formField = new DropdownField(
			'sdf',
			'Sortieren nach:',
			$rangeLabels
		);
		$formField->setEmptyString(_t('ImmobilienHolder.SORT_BY', 'Sort by:'));

		$value = $this->getSortDirectionFilter();
		if ($value) {
			$formField->setValue($value);
		}

		return $formField;
	}
	
	
	
	/******************************* FILTERING *******************************/




	/******************************* OTHER STUFF *******************************/
	
	
	public function BaseLink(){
		$link = parent::Link($action = null);
		return $link;
	}
	
	//TODO this is more a temporary fastfix of setting the current objektart
	public function OpenImmoObjektartID(){
		if (isset($_GET['OpenImmoObjektart'])) {
			return $_GET['OpenImmoObjektart'];
		}
	}



	//
	// public function Link($action = null){
	// 	$link = parent::Link($action = null);
	//
	// 	$params = $this->getURLParams();
	// 	$action = $params['Action'];
	//
	// 	if ($action) {
	// 		$link = $link . $action . "/";
	// 	}
	//
	// 	return $link;
	// }
	//
	
	public function Objektarten(){
		$objs = OpenImmoObjektart::get()
			->filter('ParentID', 0);
		return $objs;
	}
	
	


	/**
	 * Slideshow images for the current item
	 * ATM this method is only needed as $Pos in SilverStripe
	 * starts with 1, and the Bootstrap slideshow requires 0
	 * @return ArrayList
	 */
	public function CurrentItemSlideshowImages(){
		$item = $this->getCurrentItem();
		$imgs = $item->SortedImages();
		$al = new ArrayList();
		$i = 0;
		foreach ($imgs as $img) {
			$img->CustomPos = $i;
			$al->push($img);
			$i++;
		}
		return $al;
	}


	private $cachedItems = null;
	
	
	/*
	 * Returns the items to list on this page pagintated or Limited
	 */
	public function Items($limit = null, $increasedRadius = 0, $iteration = 1) {
		
		if ($this->cachedItems) {
			return $this->cachedItems;
		}

		//Set custom filter
		//$where = ($this->hasMethod('getItemsWhere')) ? $this->getItemsWhere() : Null;

		//Debug::dump($where);

		//Set custom sort
		
		$sort = ($this->hasMethod('getItemsSort')) ? $this->getItemsSort() : $this->stat('item_sort');
		
		
		//QUERY
		//$items = $this->FetchItems($this->Stat('item_class'), $where, $sort, $limit);
		$o = $this->getObjektartFilter();
		$v = $this->getVermarktungsartFilter();
		$s = $this->getSchlafzimmerFilter();
		$b = $this->getBadezimmerFilter();
		$p = $this->getPriceFilter();
		$u = $this->getUniqueIDFilter();
		$ref = $this->getRefFilter();
		$fl = $this->getFincasUndLandhaeuserFilter();
		$hv = $this->getHaeuserUndVillenFilter();
		$wa = $this->getWohnungenUndApartmentsFilter();
		$g = $this->getGrundstueckeFilter();
		$gi = $this->getGewerbeimmobilienFilter();
		$ne = $this->getNeubauErstbezugFilter();
		$io = $this->getInvestmentobjektFilter();
		$no = $this->getNeuesteObjekteFilter();
		$u250 = $this->getUnter250Filter();
		$mb = $this->getMeerblickFilter();
		$lx = $this->getLuxusFilter();
		$l = $this->getLocationFilter();
		$lr = $this->getLocationRangeFilter();
		
		if ($increasedRadius) {
			$lr = $increasedRadius;
			$this->increasedRadius = $lr;
		}

		$re = $this->getResetFilter();
		$sdf = $this->getSortDirectionFilter();
		$spf = $this->getSortPropertyFilter();
		
		
		//Getting items
		$items = Immobilie::get()
			->filter('ImmoDbStatus','Online');


		//Vermarktungsart
		if ($v) {
			$items = $items->filter('ImmoDbVermarktungsart', $v);
		}

		//Objektart
		if ($o) {
			
			//If objektart is grundstück, we'll add more results
			$grundStueckObjektart = OpenImmoObjektart::get()
				->filter('Identifier', 'openimmo-grundstueck')
				->first();
			
			$doFilterGrundstueck = false;
			if ($grundStueckObjektart && $grundStueckObjektart->exists()) {
				$doFilterGrundstueck = true;
			}
			
			if ($doFilterGrundstueck && ($o == $grundStueckObjektart->ID)) {
				//$items = $items->filterAny(array(
				//	'OpenImmoObjektart.ID' => $o,
				//	'ImmoDbGrundstueck' => 1
				//	)
				//);
				$items = $items->filter(array(
						'OpenImmoObjektart.ID' => $o
					)
				);
			} else {
				//Debug::dump($o);
				$items = $items->filter('OpenImmoObjektart.ID', $o);
				//$filter['filter']['ImmoDbObjektart'] = $o;
			}



		}
		
		//Price ranges
		if ($p) {
			$ranges = $this->priceRanges;
			if (isset($ranges[$p])) {
				$min = (int) str_replace('.', '', $ranges[$p]['min']);
				$max = (int) str_replace('.', '', $ranges[$p]['max']);
				//Debug::dump($min);
				//OpenImmoKaufpreis

				$items = $items->filter(array(
					'OpenImmoKaufpreis:LessThanOrEqual' => $max,
					'OpenImmoKaufpreis:GreaterThanOrEqual' => $min)
				);
			}
		}
		
		
		//Location
		if ($l) {
			$items = $this->rangeFilter($items, $l, $lr);
		}
		
		
		//Schlafzimmer
		if ($s) {
			$items = $items->filter('OpenImmoAnzahlSchlafzimmer', $s);
		}

		//Badezimmer
		if ($b) {
			$items = $items->filter('OpenImmoAnzahlBadezimmer:GreaterThanOrEqual', $b);
		}
		
		//Fincas Und Landhäuser
		if ($fl) {
			
			$items = $items->filter(
				'OpenImmoObjektart.Identifier', array( 
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmoFINCA')
				)
			);
		}

		//Häuser Und Villen
		if ($hv) {
			
			$items = $items->filter(
				'OpenImmoObjektart.Identifier', array( 
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmohaus'), 					ImmoDbRelationHelper::tansliterated_identifier('OpenImmoVILLA') 
				)
			);

			
		}
		
		
		//Wohnungen Und Apartments
		if ($wa) {
			$items = $items->filter(
				'OpenImmoObjektart.Identifier', array(
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmowohnung')
					// ImmoDbRelationHelper::tansliterated_identifier('OpenImmoPENTHOUSE'),
					// ImmoDbRelationHelper::tansliterated_identifier('OpenImmoAPARTMENT')
				) 
			);
		}
		
		
		//Grundstuecke
		if ($g) {
		
			$items = $items->filter(
				'OpenImmoObjektart.Identifier', array( 
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmogrundstueck')
				)
			);
		
		}

		
		//Gewerbeimmobilien
		if ($gi) {
		
			$items = $items->filter(
				'OpenImmoObjektart.Identifier', array( 
					ImmoDbRelationHelper::tansliterated_identifier('CustomGewerbeimmobilie')
				)
			);
		
		}

		//NeubauErstbezug
		if ($ne) {
			$items = $items->filter('ImmoDbNeubauErstbezug', 1);
		}
		
		//Investmentobjekt
		if ($io) {
			$items = $items->filter('CustomBesonderesInvestment', 1);
			
		}
		
		
		//Reset
		if ($re && $re="reset") {
			$items = Immobilie::get()->filter('ImmoDbStatus','Online');
		}
		
		
		//SortDirectionFilter
		if ($sdf && $sdf !="" ) {
			
			$array = array('ASC', 'DESC', 'SZ_ASC', 'SZ_DESC', 'CREATED_DESC');
			
			// if (in_array($sdf, $array)) {
			// 	$sdf = $sdf;
			// } else {
			// 	$sdf = 'DESC';
			//
			// }
			//
			if ($sdf == 'ASC' || $sdf == 'DESC') {
				$sort = "Rating DESC, OpenImmoKaufpreis " . $sdf . ", Created DESC";
			
			} 
			
			else if ( $sdf == 'SZ_ASC' || $sdf == 'SZ_DESC' ) {

				$parts = explode("_", $sdf);

				$sort = "Rating DESC, OpenImmoAnzahlSchlafzimmer " . $parts[1] . ", Created DESC";


			}

			else if  ( $sdf == 'CREATED_ASC' || $sdf == 'CREATED_DESC' ) {

				$parts = explode("_", $sdf);
				$sort = "Rating DESC, Created ".$parts[1] . "";


			}
			
			else {
				$sort = "Rating DESC, OpenImmoKaufpreis ASC, Created DESC";
			}
			
		} else {
			
				$sort = "Rating DESC, OpenImmoKaufpreis ASC, Created DESC";
		}

		//SortPropertyFilter
		if ($spf && $spf!="") {
			if ($spf == "p") {
				$spf = "OpenImmoKaufpreis";
				
			} else {
				$spf = "Created";
			}
			
		} else {
			$spf = "Created";
			
		}
		
		
		//Ref-nr
		if ($u) {
			$items = Immobilie::get()->filter('ImmoDbStatus','Online');
			$items = $items->filter(array(
				'ImmoDbUniqueID:PartialMatch' => $u
			));
		}

		//Ref-nr
		if ($ref) {
			// Resetting all the other crap
			$items = Immobilie::get()->filter('ImmoDbStatus','Online');
			$items = $items->filter(array(
				'ImmoDbUniqueIDLeft:ExactMatch' => $ref
			));
			
			if ($items->count() == 1 ) {
				Controller::curr()->redirect( $items->First()->Link() );
			}
			
		}
		
		

		/* TODO LOOK INTO PRoperyFilter. For now only sorting by price */
		//$sort = "OpenImmoKaufpreis " . $sdf . ", Created DESC";
		
		
		//NeuesteObjekte
		if ($no) {
			$sort = "";
			$items = $items->sort('Created', 'DESC');
		}
		
		
		//Unter 250.000
		if ($u250) {
			$items = $items->filter('OpenImmoKaufpreis:LessThanOrEqual', '250000');
		}
		
		//Meerblick
		if ($mb) {
			$items = $items->filter('OpenImmoAusblickValue:PartialMatch', 'MEER');
		}
		
		$items = $items->sort($sort);
		
		if ($lx) {
			
			/* ********************
			** Wohnung/Apartment **
			**********************/
			

			$items = Immobilie::get()->filter(
				'OpenImmoObjektart.Identifier', array(
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmowohnung')
					// ImmoDbRelationHelper::tansliterated_identifier('OpenImmoPENTHOUSE'),
					// ImmoDbRelationHelper::tansliterated_identifier('OpenImmoAPARTMENT')
				) 
			);
			

			$items = $items->filter(array(
				'ImmoDbStatus' => 'Online',
				'OpenImmoKaufpreis:GreaterThanOrEqual' => '370000',
				'ImmoDbDoppelVerglasung' => 1
			));
			
			$items = $items->filterAny(array(
				'OpenImmoAusblickValue:PartialMatch' => 'MEER',
				'ImmoDbPoolAussen' => 'Gemeinschaftspool',
				'ImmoDbPoolIndoor' => 'Gemeinschaftspool'
			));

			
			// Has heating
			$items = $items->filter(array(
					'OpenImmoHeizungsartValue:PartialMatch' => 'a:',
			));
			
			
			
			/* *********
			** Fincas **
			***********/
			
			$Fincas = Immobilie::get()->filter(
				'OpenImmoObjektart.Identifier', array( 
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmoFINCA')
				)
			);

			$Fincas = $Fincas->filter(array(
					'ImmoDbStatus' => 'Online',
					'OpenImmoKaufpreis:GreaterThanOrEqual' => '1500000',
					'OpenImmoGrundstuecksflaeche:GreaterThanOrEqual' =>'7000',
					'ImmoDbDoppelVerglasung' => 1,
					'OpenImmoHeizungsartValue:PartialMatch' => 'a:'
			));
			
			$Fincas = $Fincas->exclude(array(
					'ImmoDbPoolIndoor' => 'KEINE_ANGABE',
					'ImmoDbPoolAussen' => 'KEINE_ANGABE',
					'ImmoDbKlimaTyp' => 'KEINE_ANGABE',
					'ImmoDbSaniert' => 'KEINE_ANGABE'
					
			));
			
			
			/* **********
			**  Villa  **
			************/
			
			$Villen = Immobilie::get()->filter(
				'OpenImmoObjektart.Identifier', array( 
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmohaus'), 					ImmoDbRelationHelper::tansliterated_identifier('OpenImmoVILLA') 
				)
			);

			$Villen = $Villen->filter(array(
					'ImmoDbStatus' => 'Online',
					'OpenImmoKaufpreis:GreaterThanOrEqual' => '2500000',
					'OpenImmoGrundstuecksflaeche:GreaterThanOrEqual' =>'1000',
					'ImmoDbDoppelVerglasung' => 1,
					'OpenImmoHeizungsartValue:PartialMatch' => 'a:',
					'OpenImmoAusblickValue:PartialMatch' => 'MEER'
			));
			
			$Villen = $Villen->exclude(array(
					'ImmoDbPoolIndoor' => 'KEINE_ANGABE',
					'ImmoDbPoolAussen' => 'KEINE_ANGABE',
					'ImmoDbKlimaTyp' => 'KEINE_ANGABE'
			));
			
			
			
			/* ***************************
			**  Dorfhäuser/Stadthäuser  **
			*****************************/
			
			// (1,'OpenImmogrundstueck',1,1),
// 			(2,'OpenImmoFINCA',1,2),
// 			(3,'OpenImmowohnung',1,3),
// 			(4,'OpenImmoAPARTMENT',1,1),
// 			(5,'OpenImmoPENTHOUSE',1,2),
// 			(6,'OpenImmohaus',1,4),
// 			(7,'OpenImmoVILLA',1,1),
// 			(8,'OpenImmoSTADTHAUS',1,2),
// 			(9,'OpenImmoREIHENHAUS',1,3),
// 			(10,'OpenImmoDOPPELHAUSHAELFTE',1,4),
// 			(11,'OpenImmoCHALET',1,5),
// 			(12,'CustomMyTestObjektart',0,5);

			
			$Häuser = Immobilie::get()->filter(
				'OpenImmoObjektart.Identifier', array( 
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmohaus'),
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmoSTADTHAUS'),
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmoREIHENHAUS'),
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmoCHALET') 
				)
			);

			$Häuser = $Häuser->filter(array(
					'ImmoDbStatus' => 'Online',
					'OpenImmoKaufpreis:GreaterThanOrEqual' => '400000'
			));
			
			$Häuser = $Häuser->filterAny(array(
				'OpenImmoAusblickValue:PartialMatch' => 'MEER',
				'ImmoDbErsteMeereslinie' => true,
				
			));
			
			$Häuser = $Häuser->filterAny(array(
				'ImmoDbPoolAussen' => 'Gemeinschaftspool',
				'ImmoDbPoolIndoor' => 'Gemeinschaftspool'
			));
			
			
			
			/* ***************
			**  Grundstück  **
			*****************/
			
			$Grundstuecke = Immobilie::get()->filter(
				'OpenImmoObjektart.Identifier', array( 
					ImmoDbRelationHelper::tansliterated_identifier('OpenImmogrundstueck')
				)
			);

			$Grundstuecke = $Grundstuecke->filter(array(
					'ImmoDbStatus' => 'Online',
					'ImmoDbGrundstueckGrundstuecksflaeche:GreaterThanOrEqual' => '14000',
					'ImmoDbGrundstueckBebauungMoeglich' => true,
					
			));
			
			$Grundstuecke = $Grundstuecke->filterAny(array(
				'OpenImmoAusblickValue:PartialMatch' => 'MEER',
				'ImmoDbErsteMeereslinie' => true
			));
			
			
			if ($this->Locale == "es_ES") {
				$luxusTerm = "luj";
			} else {
				$luxusTerm = "lux";
			}
			// Last, we add all titles having luxury within
			$items = Immobilie::get()->filter('OpenImmoObjekttitel:PartialMatch', array(
				$luxusTerm
			));
			
			
			$itemsBadezimmerDiff = $items->filterByCallback(function($record) {
				return ($record->BadezimmerDiff()) < 2;
			});
			
			$VillenBadezimmerDiff = $Villen->filterByCallback(function($record) {
				return ($record->BadezimmerDiff()) < 2;
			});





			
			$displayContent = ArrayList::create();
			
			foreach ($items as $item) {
			$displayContent->push($item);
			}
			
			foreach ($itemsBadezimmerDiff as $Foo) {
			$displayContent->push($Foo);
			}
			foreach ($Fincas as $Finca) {
			$displayContent->push($Finca);
			}
			foreach ($Villen as $Villa) {
			$displayContent->push($Villa);
			}
			
			foreach ($VillenBadezimmerDiff as $Thing) {
			$displayContent->push($Thing);
			}

			foreach ($Häuser as $Haus) {
			$displayContent->push($Haus);
			}

			foreach ($Grundstuecke as $Grundstueck) {
			$displayContent->push($Grundstueck);
			}

			// We need to sort an ArrayList, so we need $sort to be an array! Till now it is a string
			$sortArray = explode(", ", $sort);
			$sa = array();
			foreach ($sortArray as $a) {
				$parts = preg_split('/\s+/', $a);
				$sa[$parts[0]] = $parts[1];
			}
				
			// debug::dump($displayContent);
			$items = $displayContent->sort(
				$sa
			);
			$items->removeDuplicates('ImmoDbUniqueID');

		}
		
		//
		
		
		//step up to the next radius if the result returns less than
		//a predefined number of min items
		//
		// if ( Controller::curr()->ClassName == "MigImmobilienFilterPage" ) {
		// 	$minItems = 3;
		// } else {
		// 	$minItems = 1;
		// }
		
		$minItems = 1;
		$iteration = 0;
		
		
		//we only want 4 iterations of this to not stress the server too much
		
		// if (($items->count()) < ($minItems) & ($iteration < 4)) {
		//
		// 	//echo "LR($lr) ";
		//
		// 	$ranges = $this->locationRadiusRanges;
		//
		// 	$goForNext = false;
		// 	foreach ($ranges as $range) {
		// 		if ($goForNext) {
		// 			$this->increasedRadius = $range;
		// 			//Debug::dump($this->increasedRadius);
		//
		// 			return $this->Items($limit, $range, ($iteration +1));
		// 		}
		// 		if ($range == $lr) {
		// 			$goForNext = true;
		// 		}
		// 	}
		//
		// }

        // When sorting by SZ, we dont want to show Grundstuecke!!!
        if ($sdf == 'SZ_ASC' || $sdf == 'SZ_DESC') {

			$o = OpenImmoObjektart::get()
				->filter('Identifier', 'openimmo-grundstueck')
				->first();

             $items = $items->exclude(array(
				    'OpenImmoObjektart.ID' => $o->ID
				)
			);
        }
		// debug::dump($sort); die;
		// if (isset($sort)) {
		// 	$this->cachedItems = $items->sort($sort);
		// } else {
		// 	$this->cachedItems = $items;
		//
		// }
		
		if ($lx) {
			// We already sorted the ArrayList for LUXUS
			$items = $items;
		} else {
			$items = $items->sort($sort);
		}
		$this->cachedItems = $items;
		return $this->cachedItems;

	}
	
	private function rangeFilter($items, $l, $lr) {
		
		$loc = ImmoDbLocation::get()->byID($l);
		
		
		//		$is = $loc->getImmobilienWithDistances($lr);
		if ($lr == 1) {
			$is = $loc->calcImmobilienTiedbyName();
		} else {
			$is = $loc->getImmobilienWithDistances($lr);
		}
		
		// foreach ($l->calcImmobilienTiedbyName() as $i) {
		// 	echo $i->Title . " (#$i->ID) \n";
		// }
		//Distance debugging (only for dev)
		// foreach($is as $i) {
		// 	debug::dump("#$i->ID $i->ImmoDbUniqueIDLeft: $i->Title - distance: $i->Distance - coords: $i->OpenImmoGeokoordinatenBreitengrad, $i->OpenImmoGeokoordinatenLaengengrad\n");
		// }
		
		$iMap = $is->map();
		
		$iIdList = array();
		
		foreach($iMap as $key => $value) {
			$iIdList[] = $key;
		}
		
		
		
		// Important some Locs don't get found, when a city is bigger than 5km. Nearly all cases.
		// So we always check OpenImmoOrt as well!!!!
		// We manually construct Location search over yml definded search aliases
		// Very important. Filtering leads to unexpected results, if search aliases in yml have leading spaces eg. "Andratx, Port Andratx" this is WRONG. Should be "Andratx,Port Andratx"
		// It is important to build the "$aliasLocs" using Title AND Search from the DB. Otherwise we'll receive many empty results
		
		// Another important note. In case Location search doesn't work as expected, double check lat/long in ImmoDbLocation Table!!!
		
		$aliasLocs = explode(",", $loc->Title . $loc->Search);
		// Avoid zero results in idList empty
			
			if ($lr == 1) {
				
				$items = $items->filterAny(array(
					'ID:ExactMatch' => $iIdList,
				)
				);
				
			} else {
				
				$items = $items->filterAny(array(
					'ID:ExactMatch' => $iIdList,
					'OpenImmoImmobilie.OpenImmoOrt:PartialMatch' =>  $aliasLocs
					// 'ImmoDbBaseRelation.Title:PartialMatch' =>  $aliasLocs
				)
				);
				
			}
			
			
			return $items;
	}
	
	/******************************* URL HANDLING *******************************/
	
	/*
	 * Get the current DataObject Item from the URL if one exists
	 */
	public function getCurrentItem($itemID = null) {
		$params = $this->request->allParams();
		$class = $this->stat('item_class');

		if($itemID) {
			return $class::get()->byID($itemID);
		} elseif(isset($params['ID'])) {
			//Sanitize
			$URL = Convert::raw2sql($params['ID']);

			return Immobilie::get()->filter(
				TranslatableDataObject::localized_field('URLSegment')
				, $URL)->first();
		}
	}
}