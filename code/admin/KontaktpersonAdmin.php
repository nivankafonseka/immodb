<?php
class KontaktpersonAdmin extends DataObjectAsPageAdmin {
	private static $managed_models = array(
		'OpenImmoKontaktperson',
	);
	private static $url_segment = 'Kontakte';
	private static $menu_title = 'Eigentümer';

	public $showImportForm = true;

}