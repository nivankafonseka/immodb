<?php

class GridfieldEstatePublishAction implements GridField_ColumnProvider, GridField_ActionProvider
{

	public function augmentColumns($gridField, &$columns)
	{
		if (!in_array('Actions', $columns)) {
			$columns[] = 'Actions';
		}
	}

	public function getColumnAttributes($gridField, $record, $columnName)
	{
		return array('class' => 'col-buttons');
	}


	public function getColumnMetadata($gridField, $columnName)
	{
		if ($columnName == 'Actions') {
			return array('title' => '');
		}
	}

	public function getColumnsHandled($gridField)
	{
		return array('Actions');
	}


		
	public function getColumnContent($gridField, $record, $columnName)
	{
		
		
		// debug::dump( Controller::curr() );
		
		if (!$record->canEdit()) {
			return;
		}

		if ($record->isPublished()) {
			$field = GridField_FormAction::create(
				$gridField,
				'UnPublish' . $record->ID,
				false,
				"unpublish",
				array('RecordID' => $record->ID)
			)
				->addExtraClass('gridfield-button-unpublish')
				->setAttribute('title', _t('SiteTree.BUTTONUNPUBLISH', 'Unpublish'))
				->setAttribute('data-icon', 'accept')
				->setDescription(_t('ImmoDBAdmin.BUTTONUNPUBLISHDESC', 'Unpublish estate'));
		} else {
			$field = GridField_FormAction::create(
				$gridField,
				'Publish' . $record->ID,
				false,
				"publish",
				array('RecordID' => $record->ID)
			)
				->addExtraClass('gridfield-button-publish')
				->setAttribute('title', _t('SiteTree.BUTTONSAVEPUBLISH', 'Publish'))
				->setAttribute('data-icon', 'unpublish')
				->setDescription(
					_t(
						'ImmoDBAdmin.BUTTONUNPUBLISHDESC',
						'Publish estate'
					)
				);
		}
		return $field->Field();
	}


	public function getActions($gridField)
	{
		return array('publish', 'unpublish');
	}


	public function handleAction(GridField $gridField, $actionName, $arguments, $data)
	{
		if ($actionName == 'publish' || $actionName = 'unpublish') {
			$item = $gridField->getList()->byID($arguments['RecordID']);
			if (!$item) {
				return;
			}
			if (!$item->canEdit()) {
				throw new ValidationException(
					_t(
						'ImmoDBAdmin.PublishPermissionFailure',
						'No permission to publish or unpublish estate'
					)
				);
			}
			if ($actionName == 'publish') {
				$item->doPublish();
			}
			if ($actionName == 'unpublish') {
				$item->doUnpublish();
			}
		}

	}
	
	
}




class ImmoDbAdmin extends DataObjectAsPageAdmin {

	private static $managed_models = array(
		'Immobilie',
	);
	private static $url_segment = 'immodb';
	private static $menu_title = 'Immobilien';

	public $showImportForm = false;

	function init(){
		parent::init();
	}


	/**
	 * custom getList
	 * the getList method from {@see ModelAdmin}, amended
	 * @return SS_List
	 */
	public function getList() {
		$context = $this->getSearchContext();
		$params = $this->request->requestVar('q');
		$action = $this->getAction(); 
		
		//owner filtering gives some strage errors,
		//so it's taken out here and added in manually
		$ownerID = null;
		
		if (isset($params['Owner__ID'])) {
			$ownerID = $params['Owner__ID'];
			unset($params['Owner__ID']);
		}
		
		// debug::dump($params);
		
		if( isset( $params['ImmoDbUniqueID'] ) && $params['ImmoDbUniqueID']!="" ) {
			$ImmoDbUniqueID = $params['ImmoDbUniqueID'];
			$params = array();
			$params['ImmoDbUniqueID'] = $ImmoDbUniqueID;
		}
		

		
		$list = $context->getResults($params);


		if( isset( $params['HomepageFeature'] ) ) {
			$HomepageFeature = $params['HomepageFeature'];
			$list = $list->filter('HomepageFeature', true);
			
		}


		
		//ImmoDB Administrators
		if ($ownerID) {
			
			//better buttons clashes with the way we're filtering here
			//so, prev/next buttons will not work properly when the owner filter is set
			//there's no easy way around this ATM, unless removing better buttons
			//THIS HAS PARTIALLY BEEN FIXED BY TURNING OFF PREV/NEXT BUTTONS THROUGH THE CONFIG 
			//if ($action == 'index') {
				$list = $list->filter('OwnerID', $ownerID);
			//}
		}
		
		
		//ImmoDB Users
		//users should only be able to see estates owned by them
		if (!Permission::check("immodb-admins")) {
			$list = $list->filter('OwnerID', Member::currentUserID());
		}
		
		

		
		$this->extend('updateList', $list);
		
		return $list;
	}


	public function getEditForm($id = null, $fields = null) {
		Requirements::css('immodb/css/admin.css');
		// Requirements::block('framework/css/UploadField.css');
		// Requirements::css('immodb/css/UploadField.css');
		
		$form = parent::getEditForm($id, $fields);

		$gridFieldName = 'Immobilie';
		$gridField = $form->Fields()->fieldByName($gridFieldName);
		if ($gridField) {
			$gridField->getConfig()->addComponent(new GridfieldEstatePublishAction());
		}

		$gridField->getConfig()->getComponentByType('GridFieldDetailForm')
			->setItemRequestClass('ImmoDbGridFieldDetailForm_ItemRequest');
		
		return $form;
	}
	
	
}

class ImmoDbGridFieldDetailForm extends GridFieldDetailForm {
}

class ImmoDbGridFieldDetailForm_ItemRequest extends GridFieldDetailForm_ItemRequest
{
	private static $allowed_actions = array(
		'ItemEditForm',
		'doCancel'
	);
	
	

	public function ItemEditForm() {
		$form = parent::ItemEditForm();
		$formActions = $form->Actions();
		
		$CancelAction = FormAction::create('doCancel', _t('BackLink_Button_ss.Back', 'Back'))
			->addExtraClass('ss-ui-action-cancel')
			->setAttribute('data-icon', 'back')
			->setUseButtonTag(true);
		
		$formActions->push($CancelAction);

		return $form;
	}

	
	public function Breadcrumbs($unlinked = false) {
		if(!$this->popupController->hasMethod('Breadcrumbs')) return;

		$items = $this->popupController->Breadcrumbs($unlinked);
		if($this->record && $this->record->ID) {
			$items->push(new ArrayData(array(
				'Title' => $this->record->ImmoDbUniqueID .' / '. $this->record->Title,
				'Link' => $this->Link()
			)));	
		} else {
			$items->push(new ArrayData(array(
				'Title' => sprintf(_t('GridField.NewRecord', 'New %s'), $this->record->i18n_singular_name()),
				'Link' => false
			)));	
		}
		
		return $items;
	}
	// HACK TO MAKE BACK BUTTON WORK AS EXPECTED
	// Persist the filter for model admins, and pages list view #3542
	// https://github.com/silverstripe/silverstripe-framework/pull/3542
	
	public function doCancel($data, $form) {
		$controller = $this->getToplevelController();
		$controller->getRequest()->addHeader('X-Pjax', 'Content'); // Force a content refresh
		return $controller->redirect($this->getBacklink(), 302); //redirect back to admin section
	}
	


}